## Installation

* requires python3.7+
* clone this repo
* cd to the repo/development
* create a virtual environment: `python3 -m venv venv`
* load the virtual environment: `source venv/bin/activate`
* install the dependencies: `pip install -r requirements.txt`
* install the spaCy model: `python -m spacy download en_core_web_md`

## Usage

* download all the documents: `python run.py --download`
* to parse one document: `python run.py --parse <filepath> [--outdir <output dir>]`
* to parse all the documents: `python run.py --parseall [--outdir <output dir>]`
* to use with a GUI: `python run.py --gui [--port: port_no]`
(it will return *.akn zip archives, also saved locally in
`keld/server/converted/`)

**All the converted files will be written in the directory `data/output` unless `--outdir` is specified**

## troubleshooting
If you are experiencing problems with import errors, export the PYTHONPATH
as follows:

```export PYTHONPATH:${PYTHONPATH}:<full/path/to/the/repo/development>```
