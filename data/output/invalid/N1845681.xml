<akomaNtoso xmlns:akn4un="http://un.org/akn4un" xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0">
  <statement name="resolution">
    <meta>
      <identification source="#cirsfidUnibo">
        <FRBRWork>
          <FRBRthis value="/akn/un/statement/deliberation/unga/2018-12-20/73-202/!main"/>
          <FRBRuri value="/akn/un/statement/deliberation/unga/2018-12-20/73-202"/>
          <FRBRdate date="2018-12-20" name="Date"/>
          <FRBRauthor href="#ga" as="#author"/>
          <FRBRcountry value="un"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/un/statement/deliberation/unga/2018-12-20/73-202/eng@/!main"/>
          <FRBRuri value="/akn/un/statement/deliberation/unga/2018-12-20/73-202/eng@"/>
          <FRBRdate date="2018-12-20" name="Date"/>
          <FRBRauthor href="#ga" as="#editor"/>
          <FRBRlanguage language="eng"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/un/statement/deliberation/unga/2018-12-20/73-202/eng@/!main.xml"/>
          <FRBRuri value="/akn/un/statement/deliberation/unga/2018-12-20/73-202/eng@.xml"/>
          <FRBRdate date="2018-12-20" name="Date"/>
          <FRBRauthor href="#cirsfidUnibo" as="#editor"/>
        </FRBRManifestation>
      </identification>
      <publication date="2018-12-20" showAs="" name=""/>
      <references source="#cirsfidUnibo">
        <TLCConcept eId="symbol" href="/akn/ontology/concepts/un/symbol" showAs="Symbol"/>
        <TLCConcept eId="general" href="/akn/ontology/concepts/un/general" showAs="general"/>
        <TLCEvent eId="publicationEvent" href="/akn/ontology/events/un/publicationEvent" showAs="Publication Event"/>
        <TLCEvent eId="adoptionEvent" href="/akn/ontology/events/un/adoptionEvent" showAs="Adoption Event"/>
        <TLCLocation eId="plenaryMeeting62" href="/akn/ontology/locations/un/plenaryMeeting62" showAs="62nd plenary meeting"/>
        <TLCOrganization eId="cirsfidUnibo" href="/akn/ontology/organization/it/cirsfidUnibo" showAs="CIRSFID"/>
        <TLCOrganization eId="un" href="/akn/ontology/organizations/un/un" showAs="United Nations"/>
        <TLCOrganization eId="unga" href="/akn/ontology/organizations/un/unga" showAs="General Assembly"/>
        <TLCOrganization eId="theSixthCommittee" href="/akn/ontology/organizations/un/theSixthCommittee" showAs="the Sixth Committee"/>
        <TLCOrganization eId="theInternationalLawCommission" href="/akn/ontology/organizations/un/theInternationalLawCommission" showAs="the International Law Commission"/>
        <TLCOrganization eId="states" href="/akn/ontology/organizations/un/states" showAs="States"/>
        <TLCRole eId="editor" href="/akn/ontology/roles/un/editor" showAs="Editor of the Document"/>
        <TLCRole eId="author" href="/akn/ontology/roles/un/author" showAs="Author of the Document"/>
        <TLCTerm eId="havingConsidered" href="/akn/ontology/terms/un/havingConsidered" showAs="Having considered"/>
        <TLCTerm eId="takingNote" href="/akn/ontology/terms/un/takingNote" showAs="Taking note"/>
        <TLCTerm eId="emphasizing" href="/akn/ontology/terms/un/emphasizing" showAs="Emphasizing"/>
        <TLCTerm eId="noting" href="/akn/ontology/terms/un/noting" showAs="Noting"/>
        <TLCTerm eId="subsequent" href="/akn/ontology/terms/un/subsequent" showAs="Subsequent"/>
      </references>
    </meta>
    <coverPage>
      <p>
        <docAuthority refersTo="#un">United Nations</docAuthority>
      </p>
      <p>
        <docNumber refersTo="#symbol">A/RES/73/202</docNumber>
      </p>
      <p>
        <docAuthority refersTo="#unga">General Assembly</docAuthority>
      </p>
      <p>
        <docStatus refersTo="#general">Distr.: General</docStatus>
      </p>
      <p>
        <docDate date="2019-01-03" refersTo="#publicationEvent">3 January 2019</docDate>
      </p>
      <p>
        <session>Seventy-third session</session>
      </p>
      <p>
        <ref href="/akn/un/debateReport/agenda/ga/xxx/xxx/#item_82">Agenda item 82</ref>
      </p>
    </coverPage>
    <preface>
      <p>
        <docTitle>Resolution adopted by the <organization refersTo="#unga">General Assembly</organization> on <docDate date="2018-12-20" refersTo="#adoptionEvent">20 December 2018</docDate></docTitle>
      </p>
      <p>
        <docNumber>73/202.</docNumber>
      </p>
      <p>
        <docTitle>Subsequent agreements and subsequent practice in relation to the interpretation of treaties</docTitle>
      </p>
    </preface>
    <preamble>
      <formula eId="formula_preamble_pg1" name="">
        <p>The <organization refersTo="#unga">General Assembly</organization>,</p>
      </formula>
      <container name="havingConsidered" eId="container_preamble_pg1">
        <p><term refersTo="#havingConsidered">Having considered</term> chapter IV of the report of <organization eId="organization_6" refersTo="#theInternationalLawCommission">the International Law Commission</organization> on the work of its seventieth session,<authorialNote marker="1" placement="bottom">
  <p> Official Records of the <organization refersTo="#unga">General Assembly</organization>, Seventy-third Session, Supplement No. 10 (<ref href="">A/73/10</ref>).</p>
</authorialNote>
 which contains the draft conclusions on subsequent agreements and subsequent practice in relation to the interpretation of treaties,</p>
      </container>
      <container name="takingNote" eId="container_preamble_pg2">
        <p><term refersTo="#takingNote">Taking note</term> of the recommendation of <organization eId="organization_7" refersTo="#theInternationalLawCommission">the International Law Commission</organization> contained in paragraph 49 of its report,</p>
      </container>
      <container name="emphasizing" eId="container_preamble_pg3">
        <p><term refersTo="#emphasizing">Emphasizing</term> the continuing importance of the codification and progressive development of international law, as referred to in <ref href="">Article 13, paragraph 1</ref> (a), of the Charter of the <organization refersTo="#un">United Nations</organization>,</p>
      </container>
      <container name="noting" eId="container_preamble_pg4">
        <p><term refersTo="#noting">Noting</term> that the subject of subsequent agreements and subsequent practice in relation to the interpretation of treaties is of major importance in international relations,</p>
      </container>
    </preamble>
    <mainBody>
      <paragraph eId="para_1">
        <num>1.</num>
        <content>
          <p>Welcomes the conclusion of the work of <organization eId="organization_9" refersTo="#theInternationalLawCommission">the International Law Commission</organization> on subsequent agreements and subsequent practice in relation to the interpretation of treaties, and its adoption of the draft conclusions and commentaries thereto;<authorialNote marker="2" placement="bottom">
  <p> Ibid., para. 52.</p>
</authorialNote>
 </p>
        </content>
      </paragraph>
      <paragraph eId="para_2">
        <num>2.</num>
        <content>
          <p>Expresses its appreciation to <organization eId="organization_10" refersTo="#theInternationalLawCommission">the International Law Commission</organization> for its continuing contribution to the codification and progressive development of international law;</p>
        </content>
      </paragraph>
      <paragraph eId="para_3">
        <num>3.</num>
        <content>
          <p>Takes note of the statements in <organization eId="organization_11" refersTo="#theSixthCommittee">the Sixth Committee</organization> on the subject, including those made at the seventy-third session of the <organization refersTo="#unga">General Assembly</organization>,<authorialNote marker="3" placement="bottom">
  <p> See <ref href="">A/C.6/73/SR.20</ref>, <ref href="">A/C.6/73/SR.21</ref>, <ref href="">A/C.6/73/SR.22</ref>, <ref href="">A/C.6/73/SR.23</ref>, <ref href="">A/C.6/73/SR.24</ref>, <ref href="">A/C.6/73/SR.29</ref> and <ref href="">A/C.6/73/SR.30</ref>; see also the statements in <organization eId="organization_14" refersTo="#theSixthCommittee">the Sixth Committee</organization> that are available on the <organization refersTo="#un">United Nations</organization> PaperSmart portal.</p>
</authorialNote>
 after <organization eId="organization_12" refersTo="#theInternationalLawCommission">the International Law Commission</organization> had completed its consideration of this topic in accordance with its statute;</p>
        </content>
      </paragraph>
      <paragraph eId="para_4">
        <num>4.</num>
        <content>
          <p>Also takes note of the conclusions on subsequent agreements and subsequent practice in relation to the interpretation of treaties, the text of which is annexed to the present resolution, with the commentaries thereto, brings them to the attention of <organization refersTo="#states">States</organization> and all who may be called upon to interpret treaties, and encourages their widest possible dissemination.</p>
        </content>
      </paragraph>
    </mainBody>
    <conclusions>
      <p><location refersTo="#plenaryMeeting62">62nd plenary meeting</location> <docDate date="2018-12-20" refersTo="#adoptionEvent">20 December 2018</docDate></p>
      <blockContainer>
        <crossHeading refersTo="#preamble" eId="container_conclusions_pg1"><term refersTo="#subsequent">Subsequent</term> agreements and subsequent practice in relation to the interpretation of treaties</crossHeading>
      </blockContainer>
    </conclusions>
    <attachments>
      <attachment>
        <statement name="annex">
          <meta>
            <identification source="#cirsfidUnibo">
              <FRBRWork>
                <FRBRthis value="/akn/un/statement/deliberation/unga/2018-12-20/xx-xxxx/!main"/>
                <FRBRuri value="/akn/un/statement/deliberation/unga/2018-12-20/xx-xxxx"/>
                <FRBRdate date="2018-12-20" name="Date"/>
                <FRBRauthor href="#ga" as="#author"/>
                <FRBRcountry value="un"/>
              </FRBRWork>
              <FRBRExpression>
                <FRBRthis value="/akn/un/statement/deliberation/unga/2018-12-20/xx-xxxx/eng@/!main"/>
                <FRBRuri value="/akn/un/statement/deliberation/unga/2018-12-20/xx-xxxx/eng@"/>
                <FRBRdate date="2018-12-20" name="Date"/>
                <FRBRauthor href="#ga" as="#editor"/>
                <FRBRlanguage language="eng"/>
              </FRBRExpression>
              <FRBRManifestation>
                <FRBRthis value="/akn/un/statement/deliberation/unga/2018-12-20/xx-xxxx/eng@/!main.xml"/>
                <FRBRuri value="/akn/un/statement/deliberation/unga/2018-12-20/xx-xxxx/eng@.xml"/>
                <FRBRdate date="2018-12-20" name="Date"/>
                <FRBRauthor href="#cirsfidUnibo" as="#editor"/>
              </FRBRManifestation>
            </identification>
            <publication date="2018-12-20" showAs="" name=""/>
            <references source="#cirsfidUnibo">
              <TLCOrganization eId="cirsfidUnibo" href="/akn/ontology/organization/it/cirsfidUnibo" showAs="CIRSFID"/>
              <TLCOrganization eId="un" href="/akn/ontology/organizations/un/un" showAs="United Nations"/>
              <TLCOrganization eId="unga" href="/akn/ontology/organizations/un/unga" showAs="General Assembly"/>
              <TLCRole eId="editor" href="/akn/ontology/roles/un/editor" showAs="Editor of the Document"/>
              <TLCRole eId="author" href="/akn/ontology/roles/un/author" showAs="Author of the Document"/>
            </references>
          </meta>
          <mainBody>
            <paragraph eId="annex_1__para_main_body_pg1" refersTo="operational">
              <content>
                <p><term refersTo="#part">Part</term> One</p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_main_body_pg2" refersTo="operational">
              <content>
                <p>
                  <term refersTo="#introduction">Introduction</term>
                </p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_main_body_pg3" refersTo="operational">
              <content>
                <p><term refersTo="#conclusion">Conclusion</term> 1</p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_main_body_pg4" refersTo="operational">
              <content>
                <p>
                  <term refersTo="#scope">Scope</term>
                </p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_main_body_pg5">
              <content>
                <p>The present conclusions concern the role of subsequent agreements and subsequent practice in the interpretation of treaties.</p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_main_body_pg6" refersTo="operational">
              <content>
                <p><term refersTo="#part">Part</term> Two</p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_main_body_pg7" refersTo="operational">
              <content>
                <p><term refersTo="#basicRules">Basic rules</term> and definitions</p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_main_body_pg8" refersTo="operational">
              <content>
                <p><term refersTo="#conclusion">Conclusion</term> 2</p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_main_body_pg9" refersTo="operational">
              <content>
                <p><term refersTo="#generalRule">General rule</term> and means of treaty interpretation</p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_1">
              <num>1.</num>
              <content>
                <p>Articles 31 and 32 of the Vienna Convention on the Law of Treaties set forth, respectively, the general rule of interpretation and the recourse to supplementary means of interpretation. These rules also apply as customary international law.</p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_2">
              <num>2.</num>
              <content>
                <p>A treaty shall be interpreted in good faith in accordance with the ordinary meaning to be given to its terms in their context and in the light of its object and purpose, as provided in article 31, paragraph 1.</p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_3">
              <num>3.</num>
              <content>
                <p>Article 31, paragraph 3, provides, inter alia, that there shall be taken into account, together with the context, (a) any subsequent agreement between the parties regarding the interpretation of the treaty or the application of its provisions; and (b) any subsequent practice in the application of the treaty which establishes the agreement of the parties regarding its interpretation.</p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_4">
              <num>4.</num>
              <content>
                <p>Recourse may be had to other subsequent practice in the application of the treaty as a supplementary means of interpretation under article 32.</p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_5">
              <num>5.</num>
              <content>
                <p>The interpretation of a treaty consists of a single combined operation, which places appropriate emphasis on the various means of interpretation indicated, respectively, in articles 31 and 32.</p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_main_body_pg15" refersTo="operational">
              <content>
                <p><term refersTo="#conclusion">Conclusion</term> 3</p>
              </content>
            </paragraph>
            <blockContainer>
              <crossHeading refersTo="#preamble" eId="annex_1__container_main_body_pg1"><term refersTo="#subsequent">Subsequent</term> agreements and subsequent practice as authentic means of interpretation</crossHeading>
            </blockContainer>
            <blockContainer>
              <crossHeading refersTo="#preamble" eId="annex_1__container_main_body_pg2"><term refersTo="#subsequent">Subsequent</term> agreements and subsequent practice under article 31, paragraph 3 (a) and (b), being objective evidence of the understanding of the parties as to the meaning of the treaty, are authentic means of interpretation, in the application of the general rule of treaty interpretation reflected in article 31.</crossHeading>
            </blockContainer>
            <paragraph eId="annex_1__para_main_body_pg16" refersTo="operational">
              <content>
                <p><term refersTo="#conclusion">Conclusion</term> 4</p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_main_body_pg17">
              <content>
                <p>Definition of subsequent agreement and subsequent practice</p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_1">
              <num>1.</num>
              <content>
                <p>A subsequent agreement as an authentic means of interpretation under article 31, paragraph 3 (a), is an agreement between the parties, reached after the conclusion of a treaty, regarding the interpretation of the treaty or the application of its provisions.</p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_2">
              <num>2.</num>
              <content>
                <p>A subsequent practice as an authentic means of interpretation under article 31, paragraph 3 (b), consists of conduct in the application of a treaty, after its conclusion, which establishes the agreement of the parties regarding the interpretation of the treaty.</p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_3">
              <num>3.</num>
              <content>
                <p>A subsequent practice as a supplementary means of interpretation under article 32 consists of conduct by one or more parties in the application of the treaty, after its conclusion.</p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_main_body_pg21" refersTo="operational">
              <content>
                <p><term refersTo="#conclusion">Conclusion</term> 5</p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_main_body_pg22" refersTo="operational">
              <content>
                <p><term refersTo="#conduct">Conduct</term> as subsequent practice</p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_1">
              <num>1.</num>
              <content>
                <p>Subsequent practice under articles 31 and 32 may consist of any conduct of a party in the application of a treaty, whether in the exercise of its executive, legislative, judicial or other functions.</p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_2">
              <num>2.</num>
              <content>
                <p>Other conduct, including by non-State actors, does not constitute subsequent practice under articles 31 and 32. Such conduct may, however, be relevant when assessing the subsequent practice of parties to a treaty.</p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_main_body_pg25" refersTo="operational">
              <content>
                <p><term refersTo="#part">Part</term> Three</p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_main_body_pg26" refersTo="operational">
              <content>
                <p>
                  <term refersTo="#generalAspects">General aspects</term>
                </p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_main_body_pg27" refersTo="operational">
              <content>
                <p><term refersTo="#conclusion">Conclusion</term> 6</p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_main_body_pg28">
              <content>
                <p>Identification of subsequent agreements and subsequent practice</p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_1">
              <num>1.</num>
              <content>
                <p>The identification of subsequent agreements and subsequent practice under article 31, paragraph 3, requires, in particular, a determination whether the parties, by an agreement or a practice, have taken a position regarding the interpretation of the treaty. Such a position is not taken if the parties have merely agreed not to apply the treaty temporarily or agreed to establish a practical arrangement (modus vivendi).</p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_2">
              <num>2.</num>
              <content>
                <p>Subsequent agreements and subsequent practice under article 31, paragraph 3, may take a variety of forms.</p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_3">
              <num>3.</num>
              <content>
                <p>The identification of subsequent practice under article 32 requires, in particular, a determination whether conduct by one or more parties is in the application of the treaty.</p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_main_body_pg32" refersTo="operational">
              <content>
                <p><term refersTo="#conclusion">Conclusion</term> 7</p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_main_body_pg33" refersTo="operational">
              <content>
                <p><term refersTo="#possibleEffects">Possible effects</term> of subsequent agreements and subsequent practice in interpretation</p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_1">
              <num>1.</num>
              <content>
                <p>Subsequent agreements and subsequent practice under article 31, paragraph 3, contribute, in their interaction with other means of interpretation, to the clarification of the meaning of a treaty. This may result in narrowing, widening, or otherwise determining the range of possible interpretations, including any scope for the exercise of discretion which the treaty accords to the parties.</p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_2">
              <num>2.</num>
              <content>
                <p>Subsequent practice under article 32 may also contribute to the clarification of the meaning of a treaty.</p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_3">
              <num>3.</num>
              <content>
                <p>It is presumed that the parties to a treaty, by an agreement or a practice in the application of the treaty, intend to interpret the treaty, not to amend or to modify it. The possibility of amending or modifying a treaty by subsequent practice of the parties has not been generally recognized. The present conclusion is without prejudice to the rules on the amendment or modification of treaties under the Vienna Convention on the Law of Treaties and under customary international law.</p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_main_body_pg37" refersTo="operational">
              <content>
                <p><term refersTo="#conclusion">Conclusion</term> 8</p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_main_body_pg38">
              <content>
                <p>Interpretation of treaty terms as capable of evolving over time</p>
              </content>
            </paragraph>
            <blockContainer>
              <crossHeading refersTo="#preamble" eId="annex_1__container_main_body_pg3"><term refersTo="#subsequent">Subsequent</term> agreements and subsequent practice under articles 31 and 32 may assist in determining whether or not the presumed intention of the parties upon the conclusion of the treaty was to give a term used a meaning which is capable of evolving over time.</crossHeading>
            </blockContainer>
            <paragraph eId="annex_1__para_main_body_pg39" refersTo="operational">
              <content>
                <p><term refersTo="#conclusion">Conclusion</term> 9</p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_main_body_pg40" refersTo="operational">
              <content>
                <p><term refersTo="#weight">Weight</term> of subsequent agreements and subsequent practice as a means of interpretation</p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_1">
              <num>1.</num>
              <content>
                <p>The weight of a subsequent agreement or subsequent practice as a means of interpretation under article 31, paragraph 3, depends, inter alia, on its clarity and specificity.</p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_2">
              <num>2.</num>
              <content>
                <p>In addition, the weight of subsequent practice under article 31, paragraph 3 (b), depends, inter alia, on whether and how it is repeated.</p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_3">
              <num>3.</num>
              <content>
                <p>The weight of subsequent practice as a supplementary means of interpretation under article 32 may depend on the criteria referred to in paragraphs 1 and 2.</p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_main_body_pg44" refersTo="operational">
              <content>
                <p><term refersTo="#conclusion">Conclusion</term> 10</p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_main_body_pg45" refersTo="operational">
              <content>
                <p><term refersTo="#agreement">Agreement</term> of the parties regarding the interpretation of a treaty</p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_1">
              <num>1.</num>
              <content>
                <p>An agreement under article 31, paragraph 3 (a) and (b), requires a common understanding regarding the interpretation of a treaty which the parties are aware of and accept. Such an agreement may, but need not, be legally binding for it to be taken into account.</p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_2">
              <num>2.</num>
              <content>
                <p>The number of parties that must actively engage in subsequent practice in order to establish an agreement under article 31, paragraph 3 (b), may vary. Silence on the part of one or more parties may constitute acceptance of the subsequent practice when the circumstances call for some reaction.</p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_main_body_pg48" refersTo="operational">
              <content>
                <p><term refersTo="#part">Part</term> Four</p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_main_body_pg49" refersTo="operational">
              <content>
                <p>
                  <term refersTo="#specificAspects">Specific aspects</term>
                </p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_main_body_pg50" refersTo="operational">
              <content>
                <p><term refersTo="#conclusion">Conclusion</term> 11</p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_main_body_pg51" refersTo="operational">
              <content>
                <p><term refersTo="#decisions">Decisions</term> adopted within the framework of a Conference of States Parties</p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_1">
              <num>1.</num>
              <content>
                <p>A Conference of States Parties, under these conclusions, is a meeting of parties to a treaty for the purpose of reviewing or implementing the treaty, except where they act as members of an organ of an international organization.</p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_2">
              <num>2.</num>
              <content>
                <p>The legal effect of a decision adopted within the framework of a Conference of States Parties depends primarily on the treaty and any applicable rules of procedure. Depending on the circumstances, such a decision may embody, explicitly or implicitly, a subsequent agreement under article 31, paragraph 3 (a), or give rise to subsequent practice under article 31, paragraph 3 (b), or to subsequent practice under article 32. Decisions adopted within the framework of a Conference of States Parties often provide a non-exclusive range of practical options for implementing the treaty.</p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_3">
              <num>3.</num>
              <content>
                <p>A decision adopted within the framework of a Conference of States Parties embodies a subsequent agreement or subsequent practice under article 31, paragraph 3, in so far as it expresses agreement in substance between the parties regarding the interpretation of a treaty, regardless of the form and the procedure by which the decision was adopted, including adoption by consensus.</p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_main_body_pg55" refersTo="operational">
              <content>
                <p><term refersTo="#conclusion">Conclusion</term> 12</p>
              </content>
            </paragraph>
            <blockContainer>
              <crossHeading refersTo="#preamble" eId="annex_1__container_main_body_pg4"><term refersTo="#constituent">Constituent</term> instruments of international organizations</crossHeading>
            </blockContainer>
            <paragraph eId="annex_1__para_1">
              <num>1.</num>
              <content>
                <p>Articles 31 and 32 apply to a treaty which is the constituent instrument of an international organization. Accordingly, subsequent agreements and subsequent practice under article 31, paragraph 3, are, and subsequent practice under article 32 may be, means of interpretation for such treaties.</p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_2">
              <num>2.</num>
              <content>
                <p>Subsequent agreements and subsequent practice of the parties under article 31, paragraph 3, or subsequent practice under article 32, may arise from, or be expressed in, the practice of an international organization in the application of its constituent instrument.</p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_3">
              <num>3.</num>
              <content>
                <p>Practice of an international organization in the application of its constituent instrument may contribute to the interpretation of that instrument when applying articles 31 and 32.</p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_4">
              <num>4.</num>
              <content>
                <p>Paragraphs 1 to 3 apply to the interpretation of any treaty which is the constituent instrument of an international organization without prejudice to any relevant rules of the organization.</p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_main_body_pg60" refersTo="operational">
              <content>
                <p><term refersTo="#conclusion">Conclusion</term> 13</p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_main_body_pg61" refersTo="operational">
              <content>
                <p><term refersTo="#pronouncements">Pronouncements</term> of expert treaty bodies</p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_1">
              <num>1.</num>
              <content>
                <p>For the purposes of these conclusions, an expert treaty body is a body consisting of experts serving in their personal capacity, which is established under a treaty and is not an organ of an international organization.</p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_2">
              <num>2.</num>
              <content>
                <p>The relevance of a pronouncement of an expert treaty body for the interpretation of a treaty is subject to the applicable rules of the treaty.</p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_3">
              <num>3.</num>
              <content>
                <p>A pronouncement of an expert treaty body may give rise to, or refer to, a subsequent agreement or subsequent practice by parties under article 31, paragraph 3, or subsequent practice under article 32. Silence by a party shall not be presumed to constitute subsequent practice under article 31, paragraph 3 (b), accepting an interpretation of a treaty as expressed in a pronouncement of an expert treaty body.</p>
              </content>
            </paragraph>
            <paragraph eId="annex_1__para_4">
              <num>4.</num>
              <content>
                <p>This conclusion is without prejudice to the contribution that pronouncements of expert treaty bodies make to the interpretation of the treaties under their mandates.</p>
              </content>
            </paragraph>
          </mainBody>
        </statement>
      </attachment>
    </attachments>
  </statement>
</akomaNtoso>
