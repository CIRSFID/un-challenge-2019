<akomaNtoso xmlns:akn4un="http://un.org/akn4un" xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0">
  <statement name="resolution">
    <meta>
      <identification source="#cirsfidUnibo">
        <FRBRWork>
          <FRBRthis value="/akn/un/statement/deliberation/unga/2018-12-22/73-281/!main"/>
          <FRBRuri value="/akn/un/statement/deliberation/unga/2018-12-22/73-281"/>
          <FRBRdate date="2018-12-22" name="Date"/>
          <FRBRauthor href="#ga" as="#author"/>
          <FRBRcountry value="un"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/un/statement/deliberation/unga/2018-12-22/73-281/eng@/!main"/>
          <FRBRuri value="/akn/un/statement/deliberation/unga/2018-12-22/73-281/eng@"/>
          <FRBRdate date="2018-12-22" name="Date"/>
          <FRBRauthor href="#ga" as="#editor"/>
          <FRBRlanguage language="eng"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/un/statement/deliberation/unga/2018-12-22/73-281/eng@/!main.xml"/>
          <FRBRuri value="/akn/un/statement/deliberation/unga/2018-12-22/73-281/eng@.xml"/>
          <FRBRdate date="2018-12-22" name="Date"/>
          <FRBRauthor href="#cirsfidUnibo" as="#editor"/>
        </FRBRManifestation>
      </identification>
      <publication date="2018-12-22" showAs="" name=""/>
      <references source="#cirsfidUnibo">
        <TLCConcept eId="symbol" href="/akn/ontology/concepts/un/symbol" showAs="Symbol"/>
        <TLCConcept eId="general" href="/akn/ontology/concepts/un/general" showAs="general"/>
        <TLCEvent eId="publicationEvent" href="/akn/ontology/events/un/publicationEvent" showAs="Publication Event"/>
        <TLCEvent eId="adoptionEvent" href="/akn/ontology/events/un/adoptionEvent" showAs="Adoption Event"/>
        <TLCLocation eId="plenaryMeeting65" href="/akn/ontology/locations/un/plenaryMeeting65" showAs="65th plenary meeting"/>
        <TLCOrganization eId="cirsfidUnibo" href="/akn/ontology/organization/it/cirsfidUnibo" showAs="CIRSFID"/>
        <TLCOrganization eId="un" href="/akn/ontology/organizations/un/un" showAs="United Nations"/>
        <TLCOrganization eId="unga" href="/akn/ontology/organizations/un/unga" showAs="General Assembly"/>
        <TLCOrganization eId="theFifthCommittee" href="/akn/ontology/organizations/un/theFifthCommittee" showAs="the Fifth Committee"/>
        <TLCOrganization eId="secretariat" href="/akn/ontology/organizations/un/secretariat" showAs="Secretariat"/>
        <TLCOrganization eId="theAdvisoryCommitteeOnAdministrativeAndBudgetaryQuestions2" href="/akn/ontology/organizations/un/theAdvisoryCommitteeOnAdministrativeAndBudgetaryQuestions2" showAs="the Advisory Committee on Administrative and Budgetary Questions;2"/>
        <TLCOrganization eId="theDepartmentOfOperationalSupport" href="/akn/ontology/organizations/un/theDepartmentOfOperationalSupport" showAs="the Department of Operational Support"/>
        <TLCOrganization eId="theDepartmentOfManagementStrategyPolicyAndCompliance" href="/akn/ontology/organizations/un/theDepartmentOfManagementStrategyPolicyAndCompliance" showAs="the Department of Management Strategy, Policy and Compliance"/>
        <TLCRole eId="editor" href="/akn/ontology/roles/un/editor" showAs="Editor of the Document"/>
        <TLCRole eId="author" href="/akn/ontology/roles/un/author" showAs="Author of the Document"/>
        <TLCRole eId="secretaryGeneral" href="/akn/ontology/roles/un/secretaryGeneral" showAs="Secretary-General"/>
        <TLCTerm eId="recalling" href="/akn/ontology/terms/un/recalling" showAs="Recalling"/>
        <TLCTerm eId="commending" href="/akn/ontology/terms/un/commending" showAs="Commending"/>
        <TLCTerm eId="recognizing" href="/akn/ontology/terms/un/recognizing" showAs="Recognizing"/>
        <TLCTerm eId="havingConsidered" href="/akn/ontology/terms/un/havingConsidered" showAs="Having considered"/>
      </references>
    </meta>
    <coverPage>
      <p>
        <docAuthority refersTo="#un">United Nations</docAuthority>
      </p>
      <p>
        <docNumber refersTo="#symbol">A/RES/73/281</docNumber>
      </p>
      <p>
        <docAuthority refersTo="#unga">General Assembly</docAuthority>
      </p>
      <p>
        <docStatus refersTo="#general">Distr.: General</docStatus>
      </p>
      <p>
        <docDate date="2019-01-04" refersTo="#publicationEvent">4 January 2019</docDate>
      </p>
      <p>
        <session>Seventy-third session</session>
      </p>
      <p>
        <ref href="/akn/un/debateReport/agenda/ga/xxx/xxx/#item_135">Agenda item 135</ref>
      </p>
    </coverPage>
    <preface>
      <p>
        <docTitle>Resolution adopted by the <organization refersTo="#unga">General Assembly</organization> on <docDate date="2018-12-22" refersTo="#adoptionEvent">22 December 2018</docDate></docTitle>
      </p>
      <p>
        <docNumber>73/281.</docNumber>
      </p>
      <p>
        <docTitle>Shifting the management paradigm in the <organization refersTo="#un">United Nations</organization> </docTitle>
      </p>
    </preface>
    <preamble>
      <formula eId="formula_preamble_pg1" name="">
        <p>The <organization refersTo="#unga">General Assembly</organization>,</p>
      </formula>
      <container name="recalling" eId="container_preamble_pg1">
        <p><term refersTo="#recalling">Recalling</term> its resolutions 72/266 A of <date date="2017-12-24">24 December 2017</date> and 72/266 B of <date date="2018-07-05">5 July 2018</date>, </p>
      </container>
      <container name="commending" eId="container_preamble_pg2">
        <p><term refersTo="#commending">Commending</term> the <role eId="role_1" refersTo="#secretaryGeneral">Secretary-General</role> for his continued efforts to enhance the management of the <organization eId="organization_7" refersTo="#secretariat">Secretariat</organization>,</p>
      </container>
      <container name="recognizing" eId="container_preamble_pg3">
        <p><term refersTo="#recognizing">Recognizing</term> that human resources management is pivotal to the delivery of mandates entrusted by the <organization refersTo="#unga">General Assembly</organization>,</p>
      </container>
      <container name="havingConsidered" eId="container_preamble_pg4">
        <p><term refersTo="#havingConsidered">Having considered</term> the report of the <role eId="role_2" refersTo="#secretaryGeneral">Secretary-General</role> entitled "Shifting the management paradigm in the <organization refersTo="#un">United Nations</organization>: comparative assessment of human resources structures"<authorialNote marker="1" placement="bottom">
  <p> <ref href="">A/73/366</ref>.</p>
</authorialNote>
 and the related report of <organization eId="organization_9" refersTo="#theAdvisoryCommitteeOnAdministrativeAndBudgetaryQuestions2">the Advisory Committee on Administrative and Budgetary Questions,[[2</organization>]] </p>
      </container>
    </preamble>
    <mainBody>
      <paragraph eId="para_1">
        <num>1.</num>
        <content>
          <p>Takes note of the report of the Secretary-General;1 </p>
        </content>
      </paragraph>
      <paragraph eId="para_2">
        <num>2.</num>
        <content>
          <p>Endorses the conclusions and recommendations contained in the report of <organization eId="organization_10" refersTo="#theAdvisoryCommitteeOnAdministrativeAndBudgetaryQuestions2">the Advisory Committee on Administrative and Budgetary Questions;2</organization> </p>
        </content>
      </paragraph>
      <paragraph eId="para_3">
        <num>3.</num>
        <content>
          <p>Approves the proposal contained in the report of the <role eId="role_3" refersTo="#secretaryGeneral">Secretary-General</role>;</p>
        </content>
      </paragraph>
      <paragraph eId="para_4">
        <num>4.</num>
        <content>
          <p>Emphasizes the necessity of ensuring regular and direct engagement between <organization eId="organization_11" refersTo="#theDepartmentOfOperationalSupport">the Department of Operational Support</organization> and <organization eId="organization_12" refersTo="#theDepartmentOfManagementStrategyPolicyAndCompliance">the Department of Management Strategy, Policy and Compliance</organization> to ensure that policies are calibrated and more responsive to the operational needs of all <organization eId="organization_13" refersTo="#secretariat">Secretariat</organization> entities, including in the field;</p>
        </content>
      </paragraph>
      <paragraph eId="para_5">
        <num>5.</num>
        <content>
          <p>Recalls paragraph 28 of its resolution 72/266 B, and requests the <role eId="role_4" refersTo="#secretaryGeneral">Secretary-General</role> to include in his review the human resources functions, including an assessment of progress achieved towards equitable geographical representation, bearing in mind <ref href="">Article 101, paragraph 3</ref>, of the Charter of the <organization refersTo="#un">United Nations</organization>.</p>
        </content>
      </paragraph>
    </mainBody>
    <conclusions>
      <p><location refersTo="#plenaryMeeting65">65th plenary meeting</location> <docDate date="2018-12-22" refersTo="#adoptionEvent">22 December 2018</docDate></p>
    </conclusions>
  </statement>
</akomaNtoso>
