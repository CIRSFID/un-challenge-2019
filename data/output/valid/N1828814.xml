<akomaNtoso xmlns:akn4un="http://un.org/akn4un" xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0">
  <statement name="resolution">
    <meta>
      <identification source="#cirsfidUnibo">
        <FRBRWork>
          <FRBRthis value="/akn/un/statement/deliberation/unga/2018-09-13/72-312/!main"/>
          <FRBRuri value="/akn/un/statement/deliberation/unga/2018-09-13/72-312"/>
          <FRBRdate date="2018-09-13" name="Date"/>
          <FRBRauthor href="#ga" as="#author"/>
          <FRBRcountry value="un"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/un/statement/deliberation/unga/2018-09-13/72-312/eng@/!main"/>
          <FRBRuri value="/akn/un/statement/deliberation/unga/2018-09-13/72-312/eng@"/>
          <FRBRdate date="2018-09-13" name="Date"/>
          <FRBRauthor href="#ga" as="#editor"/>
          <FRBRlanguage language="eng"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/un/statement/deliberation/unga/2018-09-13/72-312/eng@/!main.xml"/>
          <FRBRuri value="/akn/un/statement/deliberation/unga/2018-09-13/72-312/eng@.xml"/>
          <FRBRdate date="2018-09-13" name="Date"/>
          <FRBRauthor href="#cirsfidUnibo" as="#editor"/>
        </FRBRManifestation>
      </identification>
      <publication date="2018-09-13" showAs="" name=""/>
      <references source="#cirsfidUnibo">
        <TLCConcept eId="symbol" href="/akn/ontology/concepts/un/symbol" showAs="Symbol"/>
        <TLCConcept eId="general" href="/akn/ontology/concepts/un/general" showAs="general"/>
        <TLCEvent eId="publicationEvent" href="/akn/ontology/events/un/publicationEvent" showAs="Publication Event"/>
        <TLCEvent eId="adoptionEvent" href="/akn/ontology/events/un/adoptionEvent" showAs="Adoption Event"/>
        <TLCLocation eId="plenaryMeeting115" href="/akn/ontology/locations/un/plenaryMeeting115" showAs="115th plenary meeting"/>
        <TLCOrganization eId="cirsfidUnibo" href="/akn/ontology/organization/it/cirsfidUnibo" showAs="CIRSFID"/>
        <TLCOrganization eId="un" href="/akn/ontology/organizations/un/un" showAs="United Nations"/>
        <TLCOrganization eId="unga" href="/akn/ontology/organizations/un/unga" showAs="General Assembly"/>
        <TLCOrganization eId="aMainCommittee" href="/akn/ontology/organizations/un/aMainCommittee" showAs="a Main Committee"/>
        <TLCOrganization eId="securityCouncil" href="/akn/ontology/organizations/un/securityCouncil" showAs="Security Council"/>
        <TLCRole eId="editor" href="/akn/ontology/roles/un/editor" showAs="Editor of the Document"/>
        <TLCRole eId="author" href="/akn/ontology/roles/un/author" showAs="Author of the Document"/>
        <TLCRole eId="secretaryGeneral" href="/akn/ontology/roles/un/secretaryGeneral" showAs="Secretary-General"/>
        <TLCTerm eId="recalling" href="/akn/ontology/terms/un/recalling" showAs="Recalling"/>
        <TLCTerm eId="recallingAlso" href="/akn/ontology/terms/un/recallingAlso" showAs="Recalling also"/>
      </references>
    </meta>
    <coverPage>
      <p>
        <docAuthority refersTo="#un">United Nations</docAuthority>
      </p>
      <p>
        <docNumber refersTo="#symbol">A/RES/72/312</docNumber>
      </p>
      <p>
        <docAuthority refersTo="#unga">General Assembly</docAuthority>
      </p>
      <p>
        <docStatus refersTo="#general">Distr.: General</docStatus>
      </p>
      <p>
        <docDate date="2018-09-19" refersTo="#publicationEvent">19 September 2018</docDate>
      </p>
      <p>
        <session>Seventy-second session</session>
      </p>
      <p>
        <ref href="/akn/un/debateReport/agenda/ga/xxx/xxx/#item_131">Agenda item 131</ref>
      </p>
    </coverPage>
    <preface>
      <p>
        <docTitle>Resolution adopted by the <organization refersTo="#unga">General Assembly</organization> on <docDate date="2018-09-13" refersTo="#adoptionEvent">13 September 2018</docDate></docTitle>
      </p>
      <p>
        <docNumber>72/312.</docNumber>
      </p>
      <p>
        <docTitle><organization refersTo="#un">United Nations</organization> action on sexual exploitation and abuse</docTitle>
      </p>
    </preface>
    <preamble>
      <formula eId="formula_preamble_pg1" name="">
        <p>The <organization refersTo="#unga">General Assembly</organization>,</p>
      </formula>
      <container name="recalling" eId="container_preamble_pg1">
        <p><term refersTo="#recalling">Recalling</term> the purposes and principles of the Charter of the <organization refersTo="#un">United Nations</organization> and international law, </p>
      </container>
      <container name="recallingAlso" eId="container_preamble_pg2">
        <p><term refersTo="#recallingAlso">Recalling also</term> its resolutions 71/278 of <date date="2017-03-10">10 March 2017</date> on <organization refersTo="#un">United Nations</organization> action on sexual exploitation and abuse, 72/304 of <date date="2018-07-13">13 July 2018</date> on the comprehensive review of the whole question of peacekeeping operations in all their aspects, 71/297 of <date date="2017-06-30">30 June 2017</date> on special measures for protection from sexual exploitation and abuse and 72/112 of <date date="2017-12-07">7 December 2017</date> on criminal accountability of <organization refersTo="#un">United Nations</organization> officials and experts on mission, and taking note of <organization eId="organization_11" refersTo="#securityCouncil">Security Council</organization> resolutions 2242 (2015) of <date date="2015-10-13">13 October 2015</date> and 2272 (2016) of <date date="2016-03-11">11 March 2016</date>, </p>
      </container>
    </preamble>
    <mainBody>
      <paragraph eId="para_1">
        <num>1.</num>
        <content>
          <p>Reaffirms its commitment to the zero-tolerance policy on sexual exploitation and abuse throughout the <organization refersTo="#un">United Nations</organization> system, including the agencies, funds and programmes, and takes note of the report of the Secretary-General;<authorialNote marker="1" placement="bottom">
  <p> <ref href="">A/72/751</ref> and <ref href="">A/72/751/Corr.1</ref>.</p>
</authorialNote>
</p>
        </content>
      </paragraph>
      <paragraph eId="para_2">
        <num>2.</num>
        <content>
          <p>Decides to include in the provisional agenda of its seventy-third session the item entitled "Sexual exploitation and abuse: implementing a zero-tolerance policy", and requests the <role eId="role_1" refersTo="#secretaryGeneral">Secretary-General</role> to continue to report, pursuant to resolution 57/306 of <date date="2003-04-15">15 April 2003</date>, on special measures for protection from sexual exploitation and abuse, including on progress made in implementing a zero-tolerance policy within the <organization refersTo="#un">United Nations</organization> system, for consideration by the <organization refersTo="#unga">General Assembly</organization>, in line with existing mandates and procedures.</p>
        </content>
      </paragraph>
    </mainBody>
    <conclusions>
      <p><location refersTo="#plenaryMeeting115">115th plenary meeting</location> <docDate date="2018-09-13" refersTo="#adoptionEvent">13 September 2018</docDate></p>
    </conclusions>
  </statement>
</akomaNtoso>
