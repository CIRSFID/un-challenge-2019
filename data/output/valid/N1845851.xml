<akomaNtoso xmlns:akn4un="http://un.org/akn4un" xmlns="http://docs.oasis-open.org/legaldocml/ns/akn/3.0">
  <statement name="resolution">
    <meta>
      <identification source="#cirsfidUnibo">
        <FRBRWork>
          <FRBRthis value="/akn/un/statement/deliberation/unga/2018-12-20/73-208/!main"/>
          <FRBRuri value="/akn/un/statement/deliberation/unga/2018-12-20/73-208"/>
          <FRBRdate date="2018-12-20" name="Date"/>
          <FRBRauthor href="#ga" as="#author"/>
          <FRBRcountry value="un"/>
        </FRBRWork>
        <FRBRExpression>
          <FRBRthis value="/akn/un/statement/deliberation/unga/2018-12-20/73-208/eng@/!main"/>
          <FRBRuri value="/akn/un/statement/deliberation/unga/2018-12-20/73-208/eng@"/>
          <FRBRdate date="2018-12-20" name="Date"/>
          <FRBRauthor href="#ga" as="#editor"/>
          <FRBRlanguage language="eng"/>
        </FRBRExpression>
        <FRBRManifestation>
          <FRBRthis value="/akn/un/statement/deliberation/unga/2018-12-20/73-208/eng@/!main.xml"/>
          <FRBRuri value="/akn/un/statement/deliberation/unga/2018-12-20/73-208/eng@.xml"/>
          <FRBRdate date="2018-12-20" name="Date"/>
          <FRBRauthor href="#cirsfidUnibo" as="#editor"/>
        </FRBRManifestation>
      </identification>
      <publication date="2018-12-20" showAs="" name=""/>
      <references source="#cirsfidUnibo">
        <TLCConcept eId="symbol" href="/akn/ontology/concepts/un/symbol" showAs="Symbol"/>
        <TLCConcept eId="general" href="/akn/ontology/concepts/un/general" showAs="general"/>
        <TLCEvent eId="publicationEvent" href="/akn/ontology/events/un/publicationEvent" showAs="Publication Event"/>
        <TLCEvent eId="adoptionEvent" href="/akn/ontology/events/un/adoptionEvent" showAs="Adoption Event"/>
        <TLCLocation eId="plenaryMeeting62" href="/akn/ontology/locations/un/plenaryMeeting62" showAs="62nd plenary meeting"/>
        <TLCOrganization eId="cirsfidUnibo" href="/akn/ontology/organization/it/cirsfidUnibo" showAs="CIRSFID"/>
        <TLCOrganization eId="un" href="/akn/ontology/organizations/un/un" showAs="United Nations"/>
        <TLCOrganization eId="unga" href="/akn/ontology/organizations/un/unga" showAs="General Assembly"/>
        <TLCOrganization eId="theSixthCommittee" href="/akn/ontology/organizations/un/theSixthCommittee" showAs="the Sixth Committee"/>
        <TLCOrganization eId="states" href="/akn/ontology/organizations/un/states" showAs="States"/>
        <TLCOrganization eId="assembly" href="/akn/ontology/organizations/un/assembly" showAs="Assembly"/>
        <TLCRole eId="editor" href="/akn/ontology/roles/un/editor" showAs="Editor of the Document"/>
        <TLCRole eId="author" href="/akn/ontology/roles/un/author" showAs="Author of the Document"/>
        <TLCRole eId="secretaryGeneral" href="/akn/ontology/roles/un/secretaryGeneral" showAs="Secretary-General"/>
        <TLCTerm eId="reaffirming" href="/akn/ontology/terms/un/reaffirming" showAs="Reaffirming"/>
        <TLCTerm eId="recalling" href="/akn/ontology/terms/un/recalling" showAs="Recalling"/>
        <TLCTerm eId="takingIntoAccount" href="/akn/ontology/terms/un/takingIntoAccount" showAs="Taking into account"/>
        <TLCTerm eId="noting" href="/akn/ontology/terms/un/noting" showAs="Noting"/>
        <TLCTerm eId="reiterating" href="/akn/ontology/terms/un/reiterating" showAs="Reiterating"/>
      </references>
    </meta>
    <coverPage>
      <p>
        <docAuthority refersTo="#un">United Nations</docAuthority>
      </p>
      <p>
        <docNumber refersTo="#symbol">A/RES/73/208</docNumber>
      </p>
      <p>
        <docAuthority refersTo="#unga">General Assembly</docAuthority>
      </p>
      <p>
        <docStatus refersTo="#general">Distr.: General</docStatus>
      </p>
      <p>
        <docDate date="2019-01-14" refersTo="#publicationEvent">14 January 2019</docDate>
      </p>
      <p>
        <session>Seventy-third session</session>
      </p>
      <p>
        <ref href="/akn/un/debateReport/agenda/ga/xxx/xxx/#item_87">Agenda item 87</ref>
      </p>
    </coverPage>
    <preface>
      <p>
        <docTitle>Resolution adopted by the <organization refersTo="#unga">General Assembly</organization> on <docDate date="2018-12-20" refersTo="#adoptionEvent">20 December 2018</docDate></docTitle>
      </p>
      <p>
        <docNumber>73/208.</docNumber>
      </p>
      <p>
        <docTitle>The scope and application of the principle of universal jurisdiction </docTitle>
      </p>
    </preface>
    <preamble>
      <formula eId="formula_preamble_pg1" name="">
        <p>The <organization refersTo="#unga">General Assembly</organization>,</p>
      </formula>
      <container name="reaffirming" eId="container_preamble_pg1">
        <p><term refersTo="#reaffirming">Reaffirming</term> its commitment to the purposes and principles of the Charter of the <organization refersTo="#un">United Nations</organization>, to international law and to an international order based on the rule of law, which is essential for peaceful coexistence and cooperation among <organization refersTo="#states">States</organization>, </p>
      </container>
      <container name="recalling" eId="container_preamble_pg2">
        <p><term refersTo="#recalling">Recalling</term> its resolutions 64/117 of <date date="2009-12-16">16 December 2009</date>, 65/33 of <date date="2010-12-06">6 December 2010</date>, 66/103 of <date date="2011-12-09">9 December 2011</date>, 67/98 of <date date="2012-12-14">14 December 2012</date>, 68/117 of <date date="2013-12-16">16 December 2013</date>, 69/124 of <date date="2014-12-10">10 December 2014</date>, 70/119 of <date date="2015-12-14">14 December 2015</date>, 71/149 of <date date="2016-12-13">13 December 2016</date> and 72/120 of <date date="2017-12-07">7 December 2017</date>, </p>
      </container>
      <container name="takingIntoAccount" eId="container_preamble_pg3">
        <p><term refersTo="#takingIntoAccount">Taking into account</term> the comments and observations of Governments and observers and the discussions held in <organization eId="organization_6" refersTo="#theSixthCommittee">the Sixth Committee</organization> at the sixty-fourth to seventy-third sessions of the <organization refersTo="#unga">General Assembly</organization> on the scope and application of universal jurisdiction,<authorialNote marker="1" placement="bottom">
  <p> See <ref href="">A/C.6/64/SR.12</ref>, <ref href="">A/C.6/64/SR.13</ref>, <ref href="">A/C.6/64/SR.25</ref> and <ref href="">A/C.6/64/SR.1</ref>–28/Corrigendum; <ref href="">A/C.6/65/SR.10</ref>, <ref href="">A/C.6/65/SR.11</ref>, <ref href="">A/C.6/65/SR.12</ref>, <ref href="">A/C.6/65/SR.27</ref> and <ref href="">A/C.6/65/SR.28</ref>; <ref href="">A/C.6/66/SR.12</ref>, <ref href="">A/C.6/66/SR.13</ref>, <ref href="">A/C.6/66/SR.17</ref> and <ref href="">A/C.6/66/SR.29</ref>; <ref href="">A/C.6/67/SR.12</ref>, <ref href="">A/C.6/67/SR.13</ref>, <ref href="">A/C.6/67/SR.24</ref> and <ref href="">A/C.6/67/SR.25</ref>; <ref href="">A/C.6/68/SR.12</ref>, <ref href="">A/C.6/68/SR.13</ref>, <ref href="">A/C.6/68/SR.14</ref> and <ref href="">A/C.6/68/SR.23</ref>; <ref href="">A/C.6/69/SR.11</ref>, <ref href="">A/C.6/69/SR.12</ref> and <ref href="">A/C.6/69/SR.28</ref>; <ref href="">A/C.6/70/SR.12</ref>, <ref href="">A/C.6/70/SR.13</ref> and <ref href="">A/C.6/70/SR.27</ref>; <ref href="">A/C.6/71/SR.13</ref>, <ref href="">A/C.6/71/SR.14</ref>, <ref href="">A/C.6/71/SR.15</ref> and <ref href="">A/C.6/71/SR.31</ref>; <ref href="">A/C.6/72/SR.13</ref>, <ref href="">A/C.6/72/SR.14</ref> and <ref href="">A/C.6/72/SR.28</ref>; and <ref href="">A/C.6/73/SR.10</ref>, <ref href="">A/C.6/73/SR.11</ref>, <ref href="">A/C.6/73/SR.12</ref> and <ref href="">A/C.6/73/SR.33</ref>.</p>
</authorialNote>
 </p>
      </container>
      <container name="noting" eId="container_preamble_pg4">
        <p><term refersTo="#noting">Noting</term> the constructive dialogue in <organization eId="organization_8" refersTo="#theSixthCommittee">the Sixth Committee</organization>, including in the context of its working group, recognizing the diversity of views expressed by <organization refersTo="#states">States</organization>, including concerns expressed in relation to the abuse or misuse of the principle of universal jurisdiction, and acknowledging, in order to make progress, the need for continuing discussions on the scope and application of the principle of universal jurisdiction in <organization eId="organization_9" refersTo="#theSixthCommittee">the Sixth Committee</organization>, </p>
      </container>
      <container name="reiterating" eId="container_preamble_pg5">
        <p><term refersTo="#reiterating">Reiterating</term> its commitment to fighting impunity, and noting the views expressed by <organization refersTo="#states">States</organization> that the legitimacy and credibility of the use of universal jurisdiction are best ensured by its responsible and judicious application consistent with international law, </p>
      </container>
    </preamble>
    <mainBody>
      <paragraph eId="para_1">
        <num>1.</num>
        <content>
          <p>Takes note with appreciation of the report of the <role eId="role_1" refersTo="#secretaryGeneral">Secretary-General</role> prepared on the basis of comments and observations of Governments and relevant observers;<authorialNote marker="2" placement="bottom">
  <p> <ref href="">A/73/123</ref> and <ref href="">A/73/123</ref>/Add.1; see also <ref href="">A/65/181</ref>, <ref href="">A/66/93</ref>, <ref href="">A/66/93</ref>/Add.1, <ref href="">A/67/116</ref>, <ref href="">A/68/113</ref>, <ref href="">A/69/174</ref>, <ref href="">A/70/125</ref>, <ref href="">A/71/111</ref> and <ref href="">A/72/112</ref>.</p>
</authorialNote>
 </p>
        </content>
      </paragraph>
      <paragraph eId="para_2">
        <num>2.</num>
        <content>
          <p>Decides that <organization eId="organization_10" refersTo="#theSixthCommittee">the Sixth Committee</organization> shall continue its consideration of the scope and application of universal jurisdiction, without prejudice to the consideration of this topic and related issues in other forums of the <organization refersTo="#un">United Nations</organization>, and for this purpose decides to establish, at its seventy-fourth session, a working group of <organization eId="organization_12" refersTo="#theSixthCommittee">the Sixth Committee</organization> to continue to undertake a thorough discussion of the scope and application of universal jurisdiction; </p>
        </content>
      </paragraph>
      <paragraph eId="para_3">
        <num>3.</num>
        <content>
          <p>Invites Member <organization refersTo="#states">States</organization> and relevant observers to the <organization refersTo="#unga">General Assembly</organization>, as appropriate, to submit, before <date date="2019-04-26">26 April 2019</date>, information and observations on the scope and application of universal jurisdiction, including, where appropriate, information on the relevant applicable international treaties and on their national legal rules and judicial practice, and requests the <role eId="role_3" refersTo="#secretaryGeneral">Secretary-General</role> to prepare and submit to the <organization eId="organization_14" refersTo="#assembly">Assembly</organization> at its seventy-fourth session a report based on such information and observations; </p>
        </content>
      </paragraph>
      <paragraph eId="para_4">
        <num>4.</num>
        <content>
          <p>Decides that the working group shall be open to all Member <organization refersTo="#states">States</organization> and that relevant observers will be invited to participate in the work of the working group; </p>
        </content>
      </paragraph>
      <paragraph eId="para_5">
        <num>5.</num>
        <content>
          <p>Also decides to include in the provisional agenda of its seventy-fourth session the item entitled "The scope and application of the principle of universal jurisdiction".</p>
        </content>
      </paragraph>
    </mainBody>
    <conclusions>
      <p><location refersTo="#plenaryMeeting62">62nd plenary meeting</location> <docDate date="2018-12-20" refersTo="#adoptionEvent">20 December 2018</docDate></p>
    </conclusions>
  </statement>
</akomaNtoso>
