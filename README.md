# UNGA Resolution Challenge 2019

## SANKOFA - AKN4UN

[Installation and Usage](/settings/README.md)

[Documents](/documents)

[Ontology](/ontology)

[Source code](/development)

[Data](/data)
