import os
import unittest

from keld.doc_loader import load_document
from keld.inline_parser import parse_inlines

TEST_DOC_DIR = os.path.join(os.pardir, "test_docs")


class TestEngine(unittest.TestCase):

    def test_read_doc(self):
        file_list = [fname for fname in os.listdir(TEST_DOC_DIR)
                     if fname.lower().endswith((".doc", ".docx")) and fname[0].isalnum()]
        for filename in file_list:
            filepath = os.path.join(TEST_DOC_DIR, filename)
            load_document(filepath)

    def test_inline_parser(self):
        text = "Resolution adopted by the General Assembly on 7 December 2017"
        self.assertTrue(len(parse_inlines(text)), 1)


if __name__ == "__main__":
    unittest.main(verbosity=2)
