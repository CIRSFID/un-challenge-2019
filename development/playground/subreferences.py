from keld.inline_parser import parse_inlines
from keld.doc_handler.serializers import markup


def test_1():
    for text in [
        "this is a range of dates: from 10 october 1990 to 20 october 1991",
        "this is a range of dates: from october 1990 to 20 october 1991",
        "this is a range of dates: from october 1990 to october 1991",
        "this is a range of dates: from 1990 to october 1991",
        "this is a range of dates: from 10 to 20 october 1991",
        "this is a range of dates: from 1990 to 20 october 1991",
        "this is a range of dates: from 1990 to 1991"
    ]:
        refs = parse_inlines(text)
        print(markup(text, refs))


if __name__ == "__main__":
    test_1()
