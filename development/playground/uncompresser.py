import os

from keld import TEST_DOCS_DIR, load_document


def uncompress(docname):
    print("\n", docname, "\n")
    docpath = os.path.join(TEST_DOCS_DIR, docname)
    doc = load_document(docpath)


if __name__ == "__main__":
    uncompress("N1743046.DOC")
