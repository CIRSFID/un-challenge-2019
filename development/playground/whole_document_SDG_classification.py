from keld import load_document
from keld.ner_sdg import get_sdg
from pprint import pprint

import os

from keld import TEST_DOCS_DIR
from keld.body_parser import parse
from keld.doc_handler import setup_logger
from keld.serializer import Serializer
from tqdm import tqdm

here = os.path.abspath(os.path.dirname(os.path.abspath(__file__)))

logger = setup_logger("tmp", os.path.join(here, "tmp.log"))
tqdm.monitor_interval = 0

if __name__ == "__main__":
    serializer = Serializer()
    pbar = tqdm(os.listdir(TEST_DOCS_DIR))
    ndoc = 5
    os.makedirs("out", exist_ok=True)
    for i, docname in enumerate(pbar):
        if not docname.endswith((".doc", ".DOC")):
            continue
        pbar.set_description(docname)
        docpath = os.path.join(TEST_DOCS_DIR, docname)
        try:
            doc = load_document(docpath)
            doc = doc[1][1]
            print('Doc:', doc)
            print('')
            pprint(get_sdg(doc))
        except KeyboardInterrupt:
            raise
        except Exception as e:
            raise
            logger.exception(f"Fatal error for {docname}")
            tqdm.write(f">>>> Fatal error for {docname}: {e}")
        if i == ndoc:
            break
    pbar.close()
