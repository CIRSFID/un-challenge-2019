import json
import os
import sys

from keld.commons import TEST_DOCS_DIR
from keld.aknschema import validate
from keld.body_parser import parse
from keld.commons.logging import setup_logger
# from keld.parse import parse
from tqdm import tqdm

here = os.path.abspath(os.path.dirname(os.path.abspath(__file__)))

tqdm.monitor_interval = 0

logger = None


def parse_one(docname):
    docpath = os.path.join(TEST_DOCS_DIR, docname)

    try:
        doc = parse(docpath)
        doc.hierarchize()
        doc.build_eids()
        doc.nlp()
        akn_string = doc.serialize()
        valid, validation = validate(akn_string)
        tqdm.write(f"{docname} Valid: {valid}")
        if valid:
            out_dir = os.path.join("out", "valid")
        else:
            out_dir = os.path.join("out", "invalid")
        os.makedirs(out_dir, exist_ok=True)
        with open(os.path.join(out_dir, f"{docname.replace('.DOC', '.xml')}"), "w") as f:
            f.write(akn_string)
        with open(os.path.join(out_dir, f"{docname.replace('.DOC', '.validation.json')}"), "w") as f:
            json.dump(validation, f, indent=2)
    except KeyboardInterrupt:
        raise
    except Exception as e:
        # raise
        logger.exception(f"Fatal error for {docname}")
        tqdm.write(f">>>> Fatal error for {docname}: {e}")


def parse_all(batch_no=None):
    docs_to_parse = os.listdir(TEST_DOCS_DIR)
    batches = [
        docs_to_parse[:150],
        docs_to_parse[150:300],
        docs_to_parse[300:450],
        docs_to_parse[450:],
    ]
    if batch_no:
        batch = batches[batch_no]
    else:

        batch = docs_to_parse
    pbar = tqdm(batch)
    ndoc = 5
    os.makedirs("out", exist_ok=True)
    for i, docname in enumerate(pbar):
        # if i == ndoc:
        # break
        if not docname.endswith((".doc", ".DOC")):
            continue
        pbar.set_description(docname)
    pbar.close()


if __name__ == "__main__":

    if len(sys.argv) == 2:
        arg = sys.argv[1]
        if arg.isdigit():
            _batch_no = int(arg)
            logger = setup_logger(f"tmp{arg}", os.path.join(here, f"tmp{arg}.log"))
            parse_all(_batch_no)
        else:
            parse_one(arg)
    else:
        logger = setup_logger("tmp", os.path.join(here, "tmp.log"))
        parse_all()
