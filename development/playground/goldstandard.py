import json
import os

from keld.ner_sdg import get_sdg
from tqdm import tqdm


def create_gold_standard():
    filepath = os.path.join("all_text.txt")
    gold_standard = []
    with open(filepath, "r") as f:
        all_text = f.read()

    for paragraph in tqdm(all_text.split("\n"), "analyzing"):
        sdgs = get_sdg(paragraph)
        for sdg, tgts in sdgs:
            sdg['index'] = int(sdg['index'])
            sdg['similarity'] = float(sdg['similarity'])
            for tgt in tgts:
                tgt['index'] = int(tgt['index'])
                tgt['similarity'] = float(tgt['similarity'])
        gold_standard.append(
            {
                "paragraph": paragraph,
                "sdg": sdgs
            }
        )
    with open("gold_standard.json", "w") as f:
        json.dump(gold_standard, f, ensure_ascii=False, indent=2)


if __name__ == "__main__":
    create_gold_standard()
