import argparse

from keld import download_all, parse, parse_all
from keld.server.server import app

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Demo')
    parser.add_argument('--parse', help='Parse the provided document')
    parser.add_argument('--outdir', help="Set the output directory")
    parser.add_argument('--parseall', action="store_true", help='Parse all the documents')
    parser.add_argument('--download', action="store_true", help='Downloads all the documents')
    parser.add_argument('--gui', action="store_true", help='Starts the server with a GUI')
    parser.add_argument('--port', help='Specify the port on which to run the server')

    args = parser.parse_args()

    if args.download:
        download_all()

    if args.parse:
        parse(args.parse, args.outdir)

    if args.parseall:
        parse_all(args.outdir)

    if args.gui:
        if args.port:
            port = int(args.port)
        else:
            port = 3000
        app.run(debug=True, host='127.0.0.1', port=port)
