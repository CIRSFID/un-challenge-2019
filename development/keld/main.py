import argparse
import json
import os
import re
import zipfile

from keld.aknschema import validate
from keld.body_parser import parse as parse_doc
from keld.commons import INPUT_DIR, OUTPUT_DIR
from keld.commons.logging import setup_logger
from keld.doc_scraper import download_everything
from tqdm import tqdm

here = os.path.abspath(os.path.dirname(os.path.abspath(__file__)))

tqdm.monitor_interval = 0
logger = setup_logger(f"keld", "keld.log")


def parse(filepath, output_dir=None, write_akn=False):
    output_dir = output_dir or OUTPUT_DIR
    os.makedirs(output_dir, exist_ok=True)
    doc = parse_doc(filepath)
    doc.hierarchize()
    doc.build_eids()
    doc.nlp()
    akn_string = doc.serialize()
    valid, validation = validate(akn_string)
    filename = os.path.basename(filepath)
    akn_filename = os.path.join(output_dir, re.sub(r"\.\w+$", ".akn", filename))
    xml_filename = re.sub(r"\.\w+$", ".xml", filename)
    val_filename = re.sub(r"\.\w+$", ".validation.json", filename)
    tqdm.write(f"{filename} Valid: {valid}")
    if valid:
        out_dir = os.path.join(OUTPUT_DIR, "valid")
    else:
        out_dir = os.path.join(OUTPUT_DIR, "invalid")
    os.makedirs(out_dir, exist_ok=True)
    with open(os.path.join(out_dir, xml_filename), "w") as f:
        f.write(akn_string)
    with open(os.path.join(out_dir, val_filename), "w") as f:
        json.dump(validation, f, indent=2)
    if write_akn:
        zf = zipfile.ZipFile(akn_filename, mode='w', compression=zipfile.ZIP_DEFLATED)
        try:
            zf.writestr(xml_filename, akn_string)
            zf.writestr(val_filename, json.dumps(validation, ensure_ascii=False, indent=2))
        finally:
            zf.close()
    return valid, akn_filename


def parse_all(batch_no=None, output_dir=None):
    docs_to_parse = os.listdir(INPUT_DIR)
    batches = [
        docs_to_parse[:150],
        docs_to_parse[150:300],
        docs_to_parse[300:450],
        docs_to_parse[450:],
    ]
    if batch_no:
        batch = batches[batch_no]
    else:
        batch = docs_to_parse
    pbar = tqdm(batch)
    os.makedirs(output_dir or "out", exist_ok=True)
    for i, docname in enumerate(pbar):
        if not docname.lower().endswith((".doc", ".docx")):
            continue
        docpath = os.path.join(INPUT_DIR, docname)
        try:
            parse(docpath)
        except KeyboardInterrupt:
            raise
        except Exception as e:
            # raise
            logger.exception(f"Fatal error for {docname}")
            tqdm.write(f">>>> Fatal error for {docname}: {e}")
        pbar.set_description(docname)
    pbar.close()


def download_all():
    download_everything()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Demo')
    parser.add_argument('--parse', help='Parse the provided document')
    parser.add_argument('--parseall', action="store_true", help='Parse all the documents')
    parser.add_argument('--download', action="store_true", help='Downloads all the documents')
    args = parser.parse_args()

    if args.dowload:
        download_all()

    if args.parse:
        parse(args.parse)

    if args.parseall:
        parse_all()
