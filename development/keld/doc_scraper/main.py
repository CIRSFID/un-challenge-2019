import json
import os
import re

import requests
from bs4 import BeautifulSoup
from keld.commons import INPUT_DIR
from tqdm import tqdm

DOC_HOMEPAGE = "https://www.un.org/en/sections/documents/general-assembly-resolutions/"
DOC_DIR = INPUT_DIR
os.makedirs(DOC_DIR, exist_ok=True)

tqdm.monitor_interval = 0

if os.path.exists("sessions_to_download.json"):
    sessions_to_download = json.load(open("sessions_to_download.json"))
else:
    sessions_to_download = {}

if os.path.exists("resolutions_to_download.json"):
    resolutions_to_download = json.load(open("resolutions_to_download.json"))
else:
    resolutions_to_download = {}


def write_progress():
    with open("sessions_to_download.json", "w") as f:
        json.dump(sessions_to_download, f, indent=True, ensure_ascii=False)
    with open("resolutions_to_download.json", "w") as f:
        json.dump(resolutions_to_download, f, indent=True, ensure_ascii=False)


def _get_sessions_to_download():
    print("Getting sessions to download...", end="", flush=True)
    r = requests.get(DOC_HOMEPAGE)
    home_soup = BeautifulSoup(r.text, "html.parser")
    for restable in home_soup.find_all("div", attrs={"class": "resolutionstable"}):
        for a in restable.find_all("a"):
            sessions_to_download[a.text] = [a.get("href"), False]
    write_progress()
    print("Ok.")


def _get_resolutions_to_download():
    print("Getting resolutions to download for")
    pbar = tqdm(sessions_to_download.items())
    for k, v in pbar:
        pbar.set_description(k)
        url, downloaded = v
        if downloaded:
            continue
        r = requests.get(url)
        page_soup = BeautifulSoup(r.text, "html.parser")
        table = page_soup.find("table", attrs={"class": "tablefont"})
        if table is not None:
            for tr in table.find_all("tr"):
                tds = tr.find_all("td")
                if len(tds):
                    a = tds[0].find("a")
                    if a and a.get("href"):
                        href = a.get("href")
                        resolutions_to_download[href] = resolutions_to_download.get(href, False)
        sessions_to_download[k][1] = True
        write_progress()
    pbar.close()


def _download_all_resolutions():
    for k in tqdm([k for k, v in resolutions_to_download.items() if not v], "Downloading resolutions"):
        r = requests.get(k)
        if r.status_code == 200:
            res_soup = BeautifulSoup(r.text, "html.parser")
            top_frame = res_soup.find("frame", attrs={"name": "topFrame"})
            top_frame_src = top_frame.get("src")
            i = k.find("view_doc")
            top_frame_url = k[:i] + top_frame_src
            tf = requests.get(top_frame_url)
            top_frame_soup = BeautifulSoup(tf.text, "html.parser")
            word_div = top_frame_soup.find("div", attrs={"id": "word"})
            if word_div:
                a = word_div.find("a")
                url = a.get("href")
                r = requests.get(url)
                if r.headers.get('content-type') == 'application/msword':
                    if "Content-Disposition" in r.headers:
                        fname = re.findall("filename=(.+)", r.headers["Content-Disposition"])[0]
                    else:
                        fname = r.url.split("/")[-1]
                    with open(os.path.join(INPUT_DIR, fname), "wb") as f:
                        f.write(r.content)
                    resolutions_to_download[k] = True
                    write_progress()
                else:
                    tqdm.write(f"No document for {k}")
        else:
            tqdm.write(f"Error for {k} ({r.status_code})")


def download_everything():
    _get_sessions_to_download()
    _get_resolutions_to_download()
    _download_all_resolutions()


if __name__ == "__main__":
    download_everything()
