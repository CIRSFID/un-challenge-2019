from keld.doc_handler.doc_container import (
    DocContainer, DocNumber, DocTitle, Session, AgendaItem, Formula, Paragraph, Point, Container
)
from keld.commons.helpers import normalize_name

TYPE_MAP = {
    "doc_num": DocNumber,
    "para_num": Paragraph,
    "point_num": Point,
    "session_number": Session,
    "agenda_item": AgendaItem,
    "document_heading": DocTitle,
}


def qualify_paragraph(text):
    words = text.split()
    qualification = None
    start = end = 0
    for w in words[:3]:
        if w.endswith("s"):
            qualification = "operational"
            start = text.find(w)
            end = start + len(w)
            break
        elif w.endswith("ing"):
            qualification = "preambular"
            start = text.find(w)
            end = start + len(w)
            break

    return qualification, (start, end), normalize_name(text[start:end])
