import os
import re

from keld.commons.helpers import to_camel_case
from keld.doc_handler.doc_container import (
    DocContainer, DocNumber, DocTitle, Session, AgendaItem, Formula,
    Paragraph, Point, Container, P, Section, Heading, AuthorialNote
)
from keld.doc_handler.helpers import normalize_name, AUTHORITY_MAP
from keld.inline_parser import parse_inlines
from replus import Engine
from unidecode import unidecode
from keld.qualifier import qualifier
from keld.doc_loader import load_document
# from .helpers import qualify_paragraph

here = os.path.dirname(os.path.abspath(__file__))

models = os.path.join(here, "models")

engine = Engine(models, *["i"])


def parse(docpath):
    doc_strings = load_document(docpath)
    document = DocContainer()
    for type_, text in doc_strings:
        if type_ == "coverpage":
            parse_coverpage(text, document)
        if type_ == "main":
            parse_main(text, document)
        if type_ == "footnotes":
            parse_footnotes(text, document)
    return document


def parse_main(main_text, document):
    previous_type = None
    previous_value = None
    attachments = document.attachments
    target = document.cover_page
    last_element = None
    for _p in main_text.split("\n"):
        p = unidecode(_p)  # TODO: this is REAL BAD
        if p.strip() == "":
            continue
        m = engine.search(p)
        if m is not None:
            qualification, (qstart, qend), qterm = qualifier.qualify_paragraph(p)[0]  # qualify_paragraph(p[m.end:].lstrip())  #
            if m.type == "numbers":
                first_group = m.groups()[0].key
                if first_group == "doc_num":
                    document.number = re.sub(r"\.$", r"", m.value.strip())
                    # target = document.preface
                    target.append(DocNumber(text=m.value, parent=document))
                    target.append(DocTitle(p[m.end:].lstrip(), parent=document))
                elif first_group == "para_num":
                    target = document.main_body
                    num = m.value
                    e = Paragraph(p[m.end:].lstrip(), num=num, parent=document)
                    if qualification == "operational":
                        attributes = {
                            "refersTo": f"#{qterm}",
                            "$reference": {
                                "tag": "TLCTerm",
                                "eId": "operational",
                                "href": f"/akn/ontology/terms/un/{qterm}",
                                "showAs": "Operational"
                            }
                        }
                        e.inlines.append(("term", (qstart, qend), attributes))
                        e.attributes["refersTo"] = "operational"
                    elif qualification == "preambular":
                        show_as = p[qstart:qend]
                        attributes = {
                            "refersTo": f"#{qterm}",
                            "$reference": {
                                "tag": "TLCTerm",
                                "eId": qterm,
                                "href": f"/akn/ontology/terms/un/{qterm}",
                                "showAs": show_as
                            }
                        }
                        e.inlines.append(("term", (qstart, qend), attributes))
                        e.attributes["refersTo"] = "#preamble"
                    target.append(e)
                    last_element = e
                elif first_group == "point_num":
                    num = m.value
                    if last_element is not None and last_element.tag in ["paragraph", "container"]:
                        e = Point(text=p[m.end:].lstrip(), num=num, parent=last_element)
                        last_element.append(e)
                    else:
                        target = document.main_body
                        e = Paragraph(text=p[m.end:].lstrip(), num=num, parent=document)
                        target.append(e)
                elif first_group == "act_num":
                    pass  # Todo
                else:
                    raise ValueError(f"Unkown number: {m.value}")

            elif m.type == "annex":
                annex_doc = DocContainer(
                    doctype="annex",
                    authority=document.authority,
                    date=document.date,
                )
                attachments.append(annex_doc)
                annex_doc.eid = f"annex_{len(attachments)}"
                document = annex_doc
            else:
                if m.type != "roman":
                    if m.type == "session_number":
                        target.append(Session(p, parent=document))
                    elif m.type == "agenda_item":
                        target.append(AgendaItem(p, num=m.group("num_item").value))
                    elif m.type == "document_heading":
                        target = document.preface
                        document.doctype = to_camel_case(m.group("act").value)
                        document.authority = normalize_name(m.group("authority").value)
                        e = DocTitle(p, parent=document)
                        for r in parse_inlines(p, "date", "authority"):
                            if r[0] == "date":
                                document.date = r[2]["date"]
                                tag, offset, attributes = r
                                tag = "docDate"
                                attributes["refersTo"] = "#adoptionEvent"
                                attributes["$reference"] = dict(
                                    tag=f"TLCEvent",
                                    eId="adoptionEvent",
                                    href=f"/akn/ontology/events/un/adoptionEvent",
                                    showAs="Adoption Event"
                                )
                                e.inlines.append((tag, offset, attributes))
                            elif r[0] == "authority":
                                tag, offset, attributes = r
                                tag = "docAuthority"
                                _start, _end = r[1]
                                _name = normalize_name(p[_start:_end])
                                authority_id = AUTHORITY_MAP.get(_name, _name)
                                attributes["refersTo"] = f"#{authority_id}"
                                attributes["$reference"] = dict(
                                    tag=f"TLCOrganization",
                                    eId=authority_id,
                                    href=f"/akn/ontology/organizations/un/{authority_id}",
                                    showAs=f"{p[_start:_end]}"
                                )
                                e.inlines.append((tag, offset, attributes))
                        target.append(e)
                    elif m.type == "subheading":
                        target.append(P(text=p, parent=document))
                    elif m.type == "formula":
                        target = document.preamble
                        formula = Formula(text=p, parent=document)
                        target.append(formula)
                    elif m.type == "conclusions":
                        target = document.conclusions
                        e = P(p, parent=document)
                        conclusions_inlines = []
                        for r in parse_inlines(p, "date"):
                            if r[0] == "date":
                                tag, offset, attributes = r
                                tag = "docDate"
                                attributes["refersTo"] = "#adoptionEvent"
                                attributes["$reference"] = dict(
                                    tag=f"TLCEvent",
                                    eId="adoptionEvent",
                                    href=f"/akn/ontology/events/un/adoptionEvent",
                                    showAs="Adoption Event"
                                )
                                conclusions_inlines.append((tag, offset, attributes))
                        e.inlines.extend(conclusions_inlines)
                        target.append(e)
                previous_type = m.type
        else:
            if previous_type == "roman":
                section = Section(num=previous_value, parent=document)
                heading = Heading(p, parent=section)
                section.append(heading)
                target = document.main_body
                target.append(section)
            else:
                qualification, (start, end), term = qualifier.qualify_paragraph(p)[0]  # qualify_paragraph(p)  #
                if qualification == "preambular":
                    show_as = p[start:end]
                    attributes = {
                        "refersTo": f"#{term}",
                        "$reference": {
                            "tag": "TLCTerm",
                            "eId": term,
                            "href": f"/akn/ontology/terms/un/{term}",
                            "showAs": show_as
                        }
                    }
                    e = Container(p, parent=document)
                    e.inlines.append(("term", (start, end), attributes))
                    e.attributes["name"] = term
                elif qualification == "operational":
                    attributes = {
                        "refersTo": f"#{term}",
                        "$reference": {
                            "tag": "TLCTerm",
                            "eId": "operational",
                            "href": f"/akn/ontology/terms/un/{term}",
                            "showAs": "Operational"
                        }
                    }
                    target = document.main_body
                    e = Paragraph(p, parent=document, refersTo=qualification)
                    e.inlines.append(("term", (start, end), attributes))
                else:
                    target = document.main_body
                    e = Paragraph(p, parent=document)
                last_element = e
                target.append(e)
            previous_type = None
        previous_value = p


def parse_coverpage(text, document):
    target = document.cover_page
    for line in text.split("\n"):
        if line.strip() != "":
            e = P(line, parent=document)
            for r in parse_inlines(line):
                if r[0] == "date":
                    tag, offset, attributes = r
                    tag = "docDate"
                    attributes["refersTo"] = "#publicationEvent"
                    attributes["$reference"] = dict(
                        tag=f"TLCEvent",
                        eId="publicationEvent",
                        href=f"/akn/ontology/events/un/publicationEvent",
                        showAs="Publication Event"
                    )
                    e.inlines.append((tag, offset, attributes))
                elif r[0] == "organization":
                    tag, offset, attributes = r
                    tag = "docAuthority"
                    _start, _end = r[1]
                    _name = normalize_name(line[_start:_end])
                    authority_id = AUTHORITY_MAP.get(_name, _name)
                    attributes["refersTo"] = f"#{authority_id}"
                    attributes["$reference"] = dict(
                        tag=f"TLCOrganization",
                        eId=authority_id,
                        href=f"/akn/ontology/organizations/un/{authority_id}",
                        showAs=f"{line[_start:_end]}"
                    )
                    e.inlines.append((tag, offset, attributes))
                elif r[0] == "ref":
                    tag, offset, attributes = r
                    _start, _end = r[1]
                    if line[_start:_end].startswith("A/RES"):
                        tag = "docNumber"
                        attributes["refersTo"] = "#symbol"
                        attributes.pop("href", None)
                        attributes["$reference"] = dict(
                            tag=f"TLCConcept",
                            eId="symbol",
                            href=f"/akn/ontology/concepts/un/symbol",
                            showAs=f"Symbol"
                        )
                    e.inlines.append((tag, offset, attributes))
            target.append(e)


def parse_footnotes(footnotes, document):
    for note_id, text in footnotes:
        document.authorial_notes.append(AuthorialNote(note_id, text=text, parent=document, placement="bottom"))
