import numpy as np  # for fast array ops

from . import common as lib
from .spacy_model_user import SpacyModelUser


class ConceptRecognizer(SpacyModelUser):
    doc_list = None
    query_cache = {}

    def __init__(self, target_id, with_centered_similarity=True, with_semantic_shifting=True, with_topic_scaling=True,
                 with_document_log_length_scaling=True, with_stemmed_tfidf=True, cache_queries=True, log=False):
        super().__init__()
        self.log = log
        self.target_id = target_id
        self.cache_queries = cache_queries
        self.with_stemmed_tfidf = with_stemmed_tfidf
        self.with_semantic_shifting = with_semantic_shifting
        self.with_topic_scaling = with_topic_scaling
        self.with_document_log_length_scaling = with_document_log_length_scaling
        self.with_centered_similarity = with_centered_similarity
        documents, self.target_size = self.get_documents()
        tfidf = self.prepare_tfidf(documents)
        self.spacy_lemmatized_documents, (self.dictionary, self.tfidf_model, self.tfidf_corpus_similarities) = tfidf

    def prepare_tfidf(self, documents):
        self.use_docvec = self.with_semantic_shifting or self.with_topic_scaling
        if self.log:
            print('with_stemmed_tfidf', self.with_stemmed_tfidf)
            print('with_semantic_shifting', self.with_semantic_shifting)
            print('with_topic_scaling', self.with_topic_scaling)
            print('with_document_log_length_scaling', self.with_document_log_length_scaling)
        # Process the whole corpus: removing upper-cases, performing tokenization, etc..
        documents = [self.format_query(d) for d in documents]
        # Get lemmatized documents
        lemmatized_documents = [self.lemmatize_spacy_document(doc) for doc in documents]
        if self.log:
            print('lemmatized_documents', lemmatized_documents)
        spacy_lemmatized_documents = [self.nlp(lib.get_document_from_words_vector(doc)) for doc in lemmatized_documents]
        if self.with_stemmed_tfidf:
            stemmed_documents = [lib.get_stemmed_document_from_words_vector(doc) for doc in lemmatized_documents]
            if self.log:
                print('stemmed_documents', stemmed_documents)
        # Build tf-idf model and similarities
        dictionary, tfidf_model, tfidf_corpus_similarities = lib.build_tfidf(
            words_vector=stemmed_documents if self.with_stemmed_tfidf else lemmatized_documents,
            very_big_corpus=False
        )
        if self.log:
            print("Number of words in dictionary:", len(dictionary))
        return spacy_lemmatized_documents, (dictionary, tfidf_model, tfidf_corpus_similarities)

    def get_documents(self):
        # Build the joint documents set and print some statistics
        documents = [indicator[1] for indicator in self.doc_list[self.target_id]]
        if self.log:
            print("Number of documents:", len(documents))
        return documents, len(documents)

    def lemmatize_spacy_document(self, doc):
        return [
            token.lemma_
            for token in doc
            if not (token.is_stop or token.is_punct)
        ]

    def format_query(self, query, manipulator=lambda x: x.lower()):
        if self.cache_queries:
            if query in self.query_cache:
                if self.log:
                    print('Concept_Recognizer.format_query: using query in cache')
                return self.query_cache[query]
            # Save the original query before changing it, we will use it as key for the caching system
            original_query = query
        # Format/normalize the query
        query = manipulator(query)
        result = self.nlp(query)
        if self.cache_queries:
            self.query_cache[original_query] = result
        return result

    def get_query_similarity(self, query):
        # Get the filtered query (Document object built using lemmas)
        query = self.format_query(query)
        # Get the lemmatized query
        lemmatized_query = self.lemmatize_spacy_document(query)
        # Get the stemmed query for tf-idf
        if self.with_stemmed_tfidf:
            stemmed_query = lib.get_stemmed_document_from_words_vector(lemmatized_query)
        if self.log:
            print('lemmatized_query', lemmatized_query)
            if self.with_stemmed_tfidf:
                print('stemmed_query', stemmed_query)
        # Get tf-idf and docvec similarities
        tfidf_similarity = lib.get_query_tfidf_similarity(
            stemmed_query if self.with_stemmed_tfidf else lemmatized_query, self.dictionary, self.tfidf_model,
            self.tfidf_corpus_similarities)
        if self.use_docvec:
            # Get the filtered query (Document object built using lemmas)
            spacy_lemmatized_query = self.nlp(lib.get_document_from_words_vector(lemmatized_query))
            docvec_similarity = lib.get_query_docvec_similarity(spacy_lemmatized_query, self.spacy_lemmatized_documents)
        else:
            docvec_similarity = None
        # Group by sub-corpus the similarities
        tfidf_similarity = np.array([tfidf_similarity[i * self.target_size:(i + 1) * self.target_size] for i in
                                     range(len(tfidf_similarity) // self.target_size)])
        if self.use_docvec:
            docvec_similarity = np.array([docvec_similarity[i * self.target_size:(i + 1) * self.target_size] for i in
                                          range(len(docvec_similarity) // self.target_size)])
            # Get the topic similarity for every sub-corpus, by averaging the docvec similarities of every sub-corpus
            topic_similarity = lib.get_topic_similarity(docvec_similarity)
            topic_similarity = np.expand_dims(topic_similarity, -1)  # expand_dims because we have sub-corpus
        else:
            topic_similarity = None
        # Get the weighted similarity
        weighted_similarity = lib.get_weighted_similarity(
            tfidf_similarity=tfidf_similarity,
            docvec_similarity=docvec_similarity,
            topic_similarity=topic_similarity,
            query_length=len(lemmatized_query),
            with_semantic_shifting=self.with_semantic_shifting,
            with_topic_scaling=self.with_topic_scaling,
            with_document_log_length_scaling=self.with_document_log_length_scaling
        )
        # Sum the weighted similarity across sub-corpus
        weighted_similarity = np.sum(weighted_similarity, 0)
        # Center the weighted_similarity vector
        if self.with_centered_similarity:
            weighted_similarity = lib.get_centered_similarity(weighted_similarity)
        return tfidf_similarity, docvec_similarity, weighted_similarity, topic_similarity

    def get_similarity_ranking(self, similarities):
        return np.argsort(similarities)[::-1] % self.target_size

    def get_index_of_most_similar_documents(self, similarities, size=None, threshold=0):
        doc_list = self.doc_list[self.target_id]
        final_ranking = self.get_similarity_ranking(similarities)
        index_list = []
        found_all_indices = False
        i = 0
        while not found_all_indices:
            if similarities[final_ranking[i]] >= threshold:
                best = final_ranking[i]
                index_list.append({'id': doc_list[best][0], 'index': best, 'similarity': similarities[best]})
                i += 1
                found_all_indices = i >= len(final_ranking) or (size is not None and len(index_list) >= size)
            else:
                found_all_indices = True
        return index_list
