from .entity_recognizer import NamedEntityRecognizer, ROLE
from .sdg_recognizer import SDGRecognizer
from .sdg_target_recognizer import SDGTargetRecognizer
from .sdg_indicator_recognizer import SDGIndicatorRecognizer

import warnings
warnings.filterwarnings("ignore")

role_model = NamedEntityRecognizer(
    target_id=ROLE,
    with_semantic_shifting=True,
    with_topic_scaling=False,
    with_document_log_length_scaling=False,
    with_stemmed_tfidf=False,
    cache_queries=False,
    log=False
)

sdg_model = SDGRecognizer(
    with_semantic_shifting=True,
    with_topic_scaling=True,
    with_document_log_length_scaling=True,
    with_stemmed_tfidf=True,
    cache_queries=True,
    log=False
)
# Targets
tgt_model_list = [
    SDGTargetRecognizer(
        target_id=i,
        with_semantic_shifting=True,
        with_topic_scaling=True,
        with_document_log_length_scaling=True,
        with_stemmed_tfidf=True,
        cache_queries=True,
        log=False
    )
    for i in range(sdg_model.target_size)
]
# Indicators
ind_model_list = [
    SDGIndicatorRecognizer(
        target_id=i, 
        with_semantic_shifting=True, 
        with_topic_scaling=True, 
        with_document_log_length_scaling=True, 
        with_stemmed_tfidf=True, 
        with_centered_similarity=True,
        cache_queries=True,
        log=False
    ) 
    for i in range(sdg_model.target_size)
]


def get_entities(text):
    roles = role_model.get_entities(
        text,
        stop_pos=[
            'PART',  # Particle <http://universaldependencies.org/u/pos/PART.html>
            'PRON',  # Pronoun <http://universaldependencies.org/u/pos/PRON.html>
            'DET',  # Determiner <http://universaldependencies.org/u/pos/DET.html>
            'SCONJ',  # Subordinating Conjunction <http://universaldependencies.org/u/pos/SCONJ.html>
            'ADJ',  # Adjective <http://universaldependencies.org/u/pos/ADJ.html>
        ],
        filter_pos=[
            # 'VERB', # Verb <http://universaldependencies.org/u/pos/VERB.html>
            'AUX',  # Auxiliary Verb <http://universaldependencies.org/u/pos/AUX_.html>
            'PUNCT',  # Punctuation <http://universaldependencies.org/u/pos/PUNCT.html>
            'ADP',  # Adposition <http://universaldependencies.org/u/pos/ADP.html>
            'CONJ',  # Coordinating Conjunction <http://www.chompchomp.com/terms/coordinatingconjunction.htm>
        ],
        token_threshold=0.5,
        entity_threshold=1.25,
        with_composite_query_tokenization=True
    )
    result = [
        ('ROLE', role['segment'])
        for role in roles
    ]

    ORG = ['ORG', 'NORP']
    EVENT = ['EVENT']
    PERSON = ['PERSON']
    PLACES = ['FACILITY', 'GPE', 'LOC']
    DATE = ['DATE']
    LAW = ['LAW']
    # List of entity labels: https://blog.ouseful.info/2017/09/page/3/

    ALL = ORG + PERSON + PLACES + LAW + EVENT

    processed_document = role_model.nlp(text)
    result.extend(
        (ent.label_, (ent.start_char, ent.end_char))
        for ent in processed_document.ents
        if ent.label_ in ALL
    )
    result.extend(
        ('DEADLINE', (processed_document[max(0, ent.start-1)].idx, ent.end_char))
        for ent in processed_document.ents
        if ent.label_ in DATE and processed_document[max(0, ent.start-1)].lemma_.lower() in ['by','until']
    )
    return result


def get_sdg(text):
    _, _, weighted_similarity, _ = sdg_model.get_query_similarity(text)
    sdg_list = sdg_model.get_index_of_most_similar_documents(weighted_similarity, threshold=0.85)
    result = []
    for sdg in sdg_list:
        i = sdg['index']
        tgt_model = tgt_model_list[i]
        ind_model = ind_model_list[i]
        # Compute target similarities
        _, _, weighted_similarity, _ = tgt_model.get_query_similarity(text)
        target_list = tgt_model.get_index_of_most_similar_documents(weighted_similarity, threshold=0.75)
        # Compute indicator similarities
        _, _, weighted_similarity, _ = ind_model.get_query_similarity(text)
        indicator_list = ind_model.get_index_of_most_similar_documents(weighted_similarity, threshold=0.75)
        result.append((sdg, target_list, indicator_list))
    return result
