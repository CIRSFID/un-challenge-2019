import json
import os
from .concept_recognizer import ConceptRecognizer

ROLE = 0
ORG = 1
PERSON = 2

here = os.path.dirname(os.path.abspath(__file__))
with open(os.path.join(here, "data", 'UN-FAO-items-list.json')) as f:
    data = json.load(f)


class NamedEntityRecognizer(ConceptRecognizer):
    roles = [(role["id"], role["description"]) for role in data["roles"]]
    orgs = [(org["id"], org["description"]) for org in data["organizations"]]
    people = [(person["id"], person["description"]) for person in data["persons"]]
    doc_list = [roles, orgs, people]

    def format_query(self, query):
        def manipulator(query):
            query = query.lower()
            query = query.replace('-', ' ')
            return query

        return super().format_query(query, manipulator)

    def lemmatize_spacy_document(self, doc):
        return [
            token.lemma_
            for token in doc
            # Remove stop tokens: <https://stackoverflow.com/questions/40288323/what-do-spacys-part-of-speech-and-dependency-tags-mean>
            # if not (token.is_punct or token.pos_ in ['PART','DET','ADP','CONJ','SCONJ'])
        ]

    def composite_query_tokenization(self, query):
        # We want to get composite tokens from the query. A composite token is a group of consecutive tokens starting with upper-case or joint by '-', '/'.
        # We also want to keep track of tokens indices inside the query.
        composite_tokens = []
        # Initialize temporary structures for handling composite tokens
        last_composite_token = []
        last_was_composition_punctuation = False
        # For each token in query
        for token in self.nlp(query):
            # Check whether token is a composite-punctuation token
            if len(token.text) == 1 and token.text[0] in ['-', '/']:
                last_composite_token.append(token)
                last_was_composition_punctuation = True
            else:
                # Check whether token starts with upper-case
                if token.text[0].isupper() or last_was_composition_punctuation:
                    last_composite_token.append(token)
                else:
                    if len(last_composite_token) > 0:
                        composite_tokens.append(last_composite_token)
                        last_composite_token = []
                    # https://stackoverflow.com/questions/40288323/what-do-spacys-part-of-speech-and-dependency-tags-mean
                    composite_tokens.append([token])
                last_was_composition_punctuation = False
        # Add the remaining composite token, if any left
        if len(last_composite_token) > 0:
            composite_tokens.append(last_composite_token)
        return composite_tokens

    def get_entities(self, query, filter_pos=[], stop_pos=[], token_threshold=0, entity_threshold=0,
                     with_composite_query_tokenization=True):
        if with_composite_query_tokenization:
            # We want to get composite tokens from the query. A composite token is a group of consecutive tokens having an upper-case first character.
            composite_tokens = self.composite_query_tokenization(query)
        else:
            composite_tokens = [[token] for token in self.format_query(query)]
        # Remove stop tokens: <https://stackoverflow.com/questions/40288323/what-do-spacys-part-of-speech-and-dependency-tags-mean>
        composite_tokens = [
            [
                token
                for token in composite_token
                if not token.pos_ in stop_pos
            ]
            for composite_token in composite_tokens
        ]
        # We want composite lemmas
        composite_lemmatized_query = [
            [
                token.lemma_
                for token in composite_token
                if not token.pos_ in filter_pos
            ]
            for composite_token in composite_tokens
            if len(composite_token) > 0
        ]
        # We also want to keep track of tokens indices inside the query, in order to perform tagging on the query (later).
        token_indices = [
            (composite_token[0].idx, composite_token[-1].idx + len(composite_token[-1].text))
            for composite_token in composite_tokens
            if len(composite_token) > 0
        ]
        # Print debug information
        if self.log:
            print('composite_lemmatized_query', composite_lemmatized_query)
            print('token_indices', token_indices)
        # Build a sub-query for each composite-token and compute its similarity across the corpus.
        entities = []
        previous_ents_count = 0
        for id, composite_token in enumerate(composite_lemmatized_query):
            # Ignore useless tokens
            if len(composite_token) == 0:
                continue
            # Compute the weighted similarity
            _, _, weighted_similarity, _ = super().get_query_similarity(' '.join(composite_token))
            # Given the threashold, get the index of the most similar documents
            index_dict = self.get_index_of_most_similar_documents(weighted_similarity, threshold=token_threshold)
            if len(index_dict) == 0:
                previous_ents_count = 0
                continue
            previous_ents_count += 1

            similarity_dict = {idx['index']: idx['similarity'] for idx in index_dict}
            entities.append({'id': id, 'composite_token': composite_token, 'similarity_dict': similarity_dict})

        if len(entities) == 0:
            return entities

        first_ent = entities[0]
        composed_entities = [
            {
                'ids': [first_ent['id']],
                'similarity_dict': first_ent['similarity_dict'],
                'start': token_indices[first_ent['id']][0],
                'end': token_indices[first_ent['id']][1]
            }
        ]
        for i in range(1, len(entities)):
            ent = entities[i]
            ent_id = ent['id']
            last_composed_ent = composed_entities[-1]
            last_ent_id = last_composed_ent['ids'][-1]
            if last_ent_id + 1 == ent_id:
                last_composed_ent['ids'].append(ent_id)
                last_composed_ent['end'] = token_indices[ent_id][1]
                last_ent_similarity_dict = last_composed_ent['similarity_dict']
                for k, v in ent['similarity_dict'].items():
                    if k not in last_ent_similarity_dict:
                        last_ent_similarity_dict[k] = 0
                    last_ent_similarity_dict[k] += v
            else:
                composed_entities.append({
                    'ids': [ent_id],
                    'similarity_dict': ent['similarity_dict'],
                    'start': token_indices[ent['id']][0],
                    'end': token_indices[ent['id']][1]
                })

        composed_entities_match = []
        for ce in composed_entities:
            # ===================================================================
            # similarity_ranking = sorted(ce['similarity_dict'].items(), key=lambda x: x[1], reverse=True)
            # match_ranking = list(map(lambda idx_similarity: (self.doc_list[self.target_id][idx_similarity[0]-1], idx_similarity[1]), similarity_ranking))
            # ===================================================================
            best_match_id, best_similarity = max(ce['similarity_dict'].items(), key=lambda x: x[1])
            best_match = self.doc_list[self.target_id][best_match_id]
            if best_similarity < entity_threshold:
                continue
            composed_entities_match.append({
                'lemmas': [composite_lemmatized_query[id] for id in ce['ids']],
                'segment': (ce['start'], ce['end']),
                'best_match': best_match,
                'best_similarity': best_similarity
            })
        return composed_entities_match
