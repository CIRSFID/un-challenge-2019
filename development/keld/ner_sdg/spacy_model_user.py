import spacy  # for natural language processing

# python3 -m spacy download en_core_web_md

SPACY_MODEL = 'en_core_web_md'


class SpacyModelUser():
    nlp = None

    @staticmethod
    def load_nlp_model():
        print('Loading spacy model <{}>...'.format(SPACY_MODEL))
        # go here <https://spacy.io/usage/processing-pipelines> for more information about Language Processing Pipeline (tokenizer, tagger, parser, etc..)
        nlp = spacy.load(SPACY_MODEL)
        return nlp

    def __init__(self):
        if SpacyModelUser.nlp is None:
            SpacyModelUser.nlp = SpacyModelUser.load_nlp_model()
