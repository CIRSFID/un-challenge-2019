import os

DATA_DIR = os.path.abspath(
    os.path.join(
        os.path.dirname(os.path.realpath(__file__)),
        os.pardir,
        os.pardir,
        os.pardir,
        "data",
    )
)

INPUT_DIR = os.path.abspath(os.path.join(DATA_DIR, "input"))
OUTPUT_DIR = os.path.abspath(os.path.join(DATA_DIR, "output"))
