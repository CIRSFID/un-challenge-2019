import logging
import os


def setup_logger(name, log_file_path, level=logging.DEBUG):
    log_dir, _ = os.path.split(log_file_path)
    if log_dir != "":
        os.makedirs(log_dir, exist_ok=True)

    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    handler = logging.FileHandler(log_file_path)
    handler.setFormatter(formatter)

    logger = logging.getLogger(name)
    logger.setLevel(level)
    logger.addHandler(handler)

    return logger
