import re

from lxml import etree

AUTHORITY_MAP = {
    "generalAssembly": "unga",
    "theGeneralAssembly": "unga",
    "unitedNations": "un",
    "theUnitedNations": "un"
}


def to_camel_case(text):
    text = text.lower()
    components = re.split(r"\W", text)
    return components[0] + ''.join(x.title() for x in components[1:])


def etree_to_string(xml):
    return etree.tostring(xml, encoding="utf-8", method="xml", pretty_print=True).decode("utf-8")


def normalize_name(text):
    atcc = to_camel_case(text)
    return AUTHORITY_MAP.get(atcc, atcc)

