HIERARCHY = [
    "section",
    "paragraph",
    "container",
    "point",
    "docNumber",
    "docTitle",
    "heading",
    "session"
    "agendaItem",
    "formula",
    "p",
]

SIBLINGS = [
    ("paragraph", "container", "formula"),
    ("docAuthority", "operational"),
    ("docNumber",
     "docTitle",
     "heading",
     "session"
     "agendaItem",
     "formula",)
]
