import re
from collections import Counter

from keld.inline_parser import parse_inlines
from keld.ner_sdg import get_sdg

from .helpers import ner_entities
from .hierarchy import HIERARCHY, SIBLINGS
from .serializers import etree_element, markup, note_pattern

EID_MAP = {
    "section": "sec",
    "paragraph": "para",
}


class _Base:

    def __init__(self, parent=None, _type=None, eid=None):
        self.__children = []
        self.__idx = 0
        self.parent = parent
        self.type = _type
        self.inlines = []
        self._eid = eid

    def append(self, child):
        if child.parent is not None:
            child.parent.remove(child)
        child.parent = self
        self.__children.append(child)

    def extend(self, children: list):
        for c in children:
            self.__children.append(c)

    def remove(self, element):
        try:
            self.__children.remove(element)
        except ValueError:
            pass
        if self.__class__.__name__ == "DocContainer":
            for attr in self.components:
                try:
                    self.components[attr].remove(element)
                except ValueError:
                    pass

    @property
    def eid(self):
        if self.parent is not None:
            parent_eid = self.parent.eid
            if parent_eid is not None and self._eid is not None:
                return f"{parent_eid}__{self._eid}"
        # assert self._eid is not None, "here!"
        return self._eid

    @eid.setter
    def eid(self, eid):
        self._eid = eid

    @property
    def children(self):
        return self.__children

    @children.setter
    def children(self, new_children):
        self.__children = new_children[:]

    def __getitem__(self, i):
        return self.__children[i]

    def __iter__(self):
        return self

    def __next__(self):
        self.__idx += 1
        try:
            return self.__children[self.__idx - 1]
        except IndexError:
            self.__idx = 0
            raise StopIteration

    def _hierarchize(self, attr):
        new_children = []
        for element in getattr(self, attr)[:]:
            element.parent = self
            if len(new_children) == 0:
                new_children.append(element)
            else:
                for new_element in reversed(new_children):
                    if new_element.is_parent_of(element):
                        new_element.append(element)
                        new_element.hierarchize()
                        break
                else:
                    new_children.append(element)
        setattr(self, attr, new_children)

    def hierarchize(self):
        if self.__class__.__name__ == "DocContainer":
            for attr in ["cover_page", "preface", "preamble", "main_body", "conclusions", "attachments"]:
                self._hierarchize(attr)
        else:
            self._hierarchize("children")

    def _build_eids(self, attr):
        eid_counter = Counter()
        for element in getattr(self, attr):
            if not hasattr(element, "num"):
                element.build_eids()
                continue
            eid_counter[element.tag] += 1
            eid_base = EID_MAP.get(element.tag, element.tag)
            eid_num = element.num or f"{attr}_pg{eid_counter[element.tag]}"
            element.eid = re.sub(r"\W", "", f"{eid_base}_{eid_num}")
            element.build_eids()

    def build_eids(self):
        if self.__class__.__name__ == "DocContainer":
            for attr in ["cover_page", "preface", "preamble", "main_body", "conclusions", "attachments"]:
                self._build_eids(attr)
        else:
            self._build_eids("children")

    def is_parent_of(self, element):
        for same_hierarchy in SIBLINGS:
            if self.type in same_hierarchy and element.type in same_hierarchy:
                return False
        try:
            return HIERARCHY.index(self.type) < HIERARCHY.index(element.type)
        except ValueError:
            return False

    def qualify(self):
        for c in self.children:
            c.qualify()

    def nlp(self):
        def overlaps(_e):
            _, (_es, _ee), _ = _e
            for inline in self.inlines:
                _, (_is, _ie), _ = inline
                if not ((_ee <= _is) or (_ie <= _es)):
                    return True
            return False

        if hasattr(self, "text"):
            parent = self.parent
            document = self
            while parent is not None:
                document = parent
                parent = document.parent

            for r in parse_inlines(self.text):
                if not overlaps(r):
                    self.inlines.append(r)
            entities = ner_entities(self.text, document)
            for e in entities:
                if not overlaps(e):
                    self.inlines.append(e)
            self.inlines.sort(key=lambda x: x[1][0])
            for e in self.inlines:
                attributes = e[2].pop("$reference", {})
                if attributes:
                    reference = Reference(**attributes)
                    document.references[attributes["eId"]] = reference
            self.get_sdg(document)
        for c in self.children:
            c.nlp()

    def get_sdg(self, document):
        for goal_list, target_list, indicator_list in get_sdg(self.text):
            sdg_id = goal_list['id']
            sdg_confidence = goal_list['similarity']
            eid = f"keyword_{len(document.classification) + 1}"
            reference_id = f"concept_sdg_{sdg_id}"
            kw_attributes = dict(
                eId=eid,
                dictionary="SDGIO",
                value=f"goal_{sdg_id}",
                href=f"/akn/ontology/concepts/un/sdg_{sdg_id}",
                showAs=f"SDG {sdg_id}",
                refersTo=f"#{reference_id}"
            )
            document.classification[eid] = Reference("keyword", **kw_attributes)
            ref_attributes = kw_attributes.copy()
            ref_attributes.pop("dictionary")
            ref_attributes.pop("value")
            ref_attributes.pop("refersTo")
            ref_attributes["eId"] = reference_id
            document.references[reference_id] = Reference("TLCConcept", **ref_attributes)
            sdg_source = Reference("source", namespace="akn4un", href=f"#{self.eid}")
            classification = Reference(
                "sdgGoal",
                namespace="akn4un",
                href=f"#{eid}",
                # value=kw_attributes["value"],
                confidence=sdg_confidence,
                name="SDGIO"
            )
            sdg_source.children.append(classification)
            for tgt in target_list:
                tgt_id = tgt['id']
                tgt_confidence = tgt['similarity']
                tgt_kw_attributes = {k: f"{v}_{tgt_id}" for k, v in kw_attributes.items() if k != "dictionary"}
                tgt_kw_attributes["dictionary"] = "SDGIO"
                tgt_ref_attributes = {k: f"{v}_{tgt_id}" for k, v in ref_attributes.items()}
                document.classification[tgt_kw_attributes["eId"]] = Reference("keyword", **tgt_kw_attributes)
                document.references[tgt_ref_attributes["eId"]] = Reference("TLCConcept", **tgt_ref_attributes)
                tgt_classification = Reference(
                    "sdgTarget",
                    namespace="akn4un",
                    href=f"#{eid}",
                    # value=tgt_kw_attributes["value"],
                    confidence=tgt_confidence,
                    name="SDGIO"
                )
                sdg_source.children.append(tgt_classification)

            document.sdg_sources.append(sdg_source)


class Base(_Base):
    def __init__(self, tag, num=None, text="", parent=None, content=True, **attributes):
        super().__init__(parent=parent, _type=tag)
        self.tag = tag
        self.num = num
        self.text = text
        self.content = content
        self.attributes = attributes

    def serialize(self, from_=None):
        self.inlines.sort(key=lambda x: x[1][0])
        if self.tag == "point" and self.parent.tag == "container":
            my_node = etree_element("item")
            self.content = False
        elif self.tag == "heading":
            return markup(self.text, self.inlines, root="heading")
        else:
            my_node = etree_element(self.tag)
        if self.eid:
            my_node.set("eId", f"{self.eid}")
        if self.num:
            num = etree_element("num")
            num.text = self.num
            my_node.append(num)
        if self.content and len(self.children) == 0:
            content = etree_element("content")
            my_node.append(content)
        else:
            content = my_node
        if len(self.children) == 0:
            if self.text != "":
                p = markup(self.text, self.inlines)
                content.append(p)
        else:
            if self.text != "":
                if self.text.strip().endswith(":"):
                    intro = etree_element("intro")
                    intro.append(markup(self.text, self.inlines))
                else:
                    alinea = etree_element("alinea")
                    _content = etree_element("content")
                    _content.append(markup(self.text, self.inlines))
                    intro = alinea
                _l = etree_element("list")
                _l.append(intro)
                content.append(_l)
                content = _l
            for c in self.children:
                content.append(c.serialize(from_=self.tag))

        for k, v in self.attributes.items():
            if v is None:
                v = ""
            my_node.set(k, f"{v}")

        return my_node


class RBase(_Base):

    def __init__(self, tag, text="", parent=None, **attributes):
        super().__init__(parent=parent, _type=tag)
        self.tag = tag
        self.text = text
        self.attributes = attributes

    def serialize(self, from_=None):
        self.inlines.sort(key=lambda x: x[1][0])
        my_node = etree_element("p")
        tag = markup(self.text, self.inlines, root=self.tag)
        my_node.append(tag)
        for k, v in self.attributes.items():
            if v is None:
                v = ""
            tag.set(k, f"{v}")
        return my_node


class Reference:

    def __init__(self, tag, namespace=None, **attributes):
        self.tag = tag
        self.attributes = attributes
        self.namespace = namespace
        self.children = []

    def serialize(self, from_=None):
        my_node = etree_element(self.tag, namespace=self.namespace)
        for k, v in self.attributes.items():
            if v is None:
                v = ""
            v = re.sub(note_pattern, "", f"{v}")
            my_node.set(k, f"{v}")
        for c in self.children:
            my_node.append(c.serialize())
        return my_node
