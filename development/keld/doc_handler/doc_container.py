from collections import Counter

from lxml import etree

from .base import _Base, Base, RBase, Reference
from .constants import DocTypes
from .serializers import create_meta, akn_string
from .serializers import etree_element, NS_MAP, markup


class DocContainer(_Base):

    def __init__(self, doctype=None, authority=None, number=None, date=None, docname=None, publication_date=None):
        super().__init__(_type="document")
        self.doctype = doctype or DocTypes.RESOLUTION
        self.docname = docname or "deliberation"
        self.authority = authority or "xxxx"
        self.number = number or "xx-xxxx"
        self.date = date or "xxxx-xx-xx"
        # self.publication_date = publication_date or "xxxx-xx-xx"
        self.cover_page = []
        self.preface = []
        self.preamble = []
        self.main_body = []
        self.conclusions = []
        self.attachments = []
        self.authorial_notes = []
        self.ref_counter = Counter()
        self.classification = {}
        self.references = dict(
            cirsfidUnibo=Reference(
                "TLCOrganization", eId="cirsfidUnibo",
                href="/akn/ontology/organization/it/cirsfidUnibo", showAs="CIRSFID"
            ),
            editor=Reference(
                "TLCRole", eId="editor",
                href="/akn/ontology/roles/un/editor", showAs="Editor of the Document"
            ),
            author=Reference(
                "TLCRole", eId="author",
                href="/akn/ontology/roles/un/author", showAs="Author of the Document"
            ),
            un=Reference(
                "TLCOrganization", eId="un",
                href="/akn/ontology/organizations/un/un", showAs="United Nations"
            ),
            unga=Reference(
                "TLCOrganization", eId="unga",
                href="/akn/ontology/organizations/un/unga", showAs="General Assembly"
            ),
        )
        self.sdg_sources = []
        self.components = {
            "coverPage": self.cover_page,
            "preface": self.preface,
            "preamble": self.preamble,
            "mainBody": self.main_body,
            "conclusions": self.conclusions,
            "attachments": self.attachments
        }

    @property
    def base_uri(self):
        return f"/akn/un/statement/{self.docname}/{self.authority}/{self.date}/{self.number.replace('/', '-')}"

    def nlp(self):
        for attr in ["children", "cover_page", "preface", "preamble", "main_body", "conclusions", "authorial_notes"]:
            for c in getattr(self, attr):
                c.nlp()

    def serialize(self, from_=None):
        akn = etree.Element("akomaNtoso", nsmap=NS_MAP)
        root = etree_element("statement", name=self.doctype)
        meta = create_meta(self)
        akn.append(root)
        root.append(meta)
        for k, v in self.components.items():
            if len(v):
                if k == "attachments":
                    attachments = etree_element("attachments")
                    for c in v:
                        component = etree_element("attachment")
                        component.append(c.serialize(from_=k))
                        attachments.append(component)
                    root.append(attachments)
                    continue
                else:
                    component = etree_element(k)
                for c in v:
                    component.append(c.serialize(from_=k))
                root.append(component)
        if self.doctype == "annex":
            return root
        return akn_string(akn, self)


class Section(Base):
    def __init__(self, num=None, parent=None):
        super().__init__("section", num=num, content=False, parent=parent)


class DocNumber(RBase):
    def __init__(self, text="", parent=None, **attributes):
        super().__init__("docNumber", text=text, parent=parent, **attributes)


class DocAuthority(RBase):
    def __init__(self, text="", parent=None, **attributes):
        super().__init__("docAuthority", text=text, parent=parent, **attributes)


class DocTitle(RBase):
    def __init__(self, text="", parent=None):
        super().__init__("docTitle", text=text, parent=parent)


class Heading(Base):
    def __init__(self, text="", parent=None):
        super().__init__("heading", text=text, content=False, parent=parent)


class Session(RBase):
    def __init__(self, text="", parent=None):
        super().__init__("session", text=text, parent=parent)


class AgendaItem(RBase):
    def __init__(self, text="", num="xx", parent=None):
        super().__init__("ref", text=text, href=f"/akn/un/debateReport/agenda/ga/xxx/xxx/#item_{num}", parent=parent)


class Formula(Base):
    def __init__(self, text="", name="", parent=None):
        super().__init__("formula", text=text, content=False, name=name, parent=parent)


class Paragraph(Base):
    def __init__(self, text="", num=None, parent=None, **attributes):
        super().__init__("paragraph", text=text, num=num, parent=parent, **attributes)


class Point(Base):
    def __init__(self, text="", num="", parent=None, **attributes):
        super().__init__("point", text=text, num=num, parent=parent, **attributes)


class Container(Base):
    def __init__(self, text="", name="", parent=None, **attributes):
        super().__init__("container", text=text, content=False, parent=parent, name=name, **attributes)

    def serialize(self, from_=None):
        self.inlines.sort(key=lambda x: x[1][0])
        is_cross_heading = from_ != "preamble"
        if is_cross_heading:
            my_node = markup(self.text, self.inlines, "crossHeading")
            my_node.set("refersTo", "#preamble")
            if isinstance(self.parent, DocContainer):
                my_actual_node = etree_element("blockContainer")
                my_actual_node.append(my_node)
            else:
                my_actual_node = my_node
            for c in self.children:
                my_actual_node.append(c.serialize())  # this will eventually be invalid, but we wont lose data
        else:
            my_node = etree_element(self.tag)
            if self.children:
                _l = etree_element("blockList")
                intro = etree_element("listIntroduction")
                intro.append(markup(self.text, self.inlines))
                _l.append(intro)
                for c in self.children:
                    _l.append(c.serialize())
                my_node.append(_l)
            else:
                my_node.append(markup(self.text, self.inlines))
            for k, v in self.attributes.items():
                if v is None:
                    v = ""
                my_node.set(k, f"{v}")
            my_actual_node = my_node

        my_node.set("eId", f"{self.eid}")
        return my_actual_node


class AuthorialNote(Base):
    def __init__(self, marker, text="", parent=None, **attributes):
        super().__init__("authorialNote", text=text, content=False, parent=parent, marker=marker, **attributes)

    def serialize(self, from_=None):
        self.inlines.sort(key=lambda x: x[1][0])
        my_node = etree.Element(self.tag)
        content = my_node
        if len(self.children) == 0:
            if self.text != "":
                p = markup(self.text, self.inlines)
                content.append(p)
        else:
            if self.text != "":
                if self.text.strip().endswith(":"):
                    intro = etree_element("intro")
                    intro.append(markup(self.text, self.inlines))
                else:
                    alinea = etree_element("intro")
                    _content = etree_element("content")
                    _content.append(markup(self.text, self.inlines))
                    intro = alinea
                _l = etree_element("list")
                _l.append(intro)
                content.append(_l)
            else:
                _l = content
            for c in self.children:
                _l.append(c)

        for k, v in self.attributes.items():
            if v is None:
                v = ""
            my_node.set(k, f"{v}")

        return my_node


class P(Base):
    def __init__(self, text="", parent=None):
        super().__init__("p", text=text, parent=parent, content=False)

    def serialize(self, from_=None):
        return markup(self.text, self.inlines)
