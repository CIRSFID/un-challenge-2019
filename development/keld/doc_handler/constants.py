class Types:
    PARAGRAPH = "paragraph"
    HEADING = "heading"
    POINT = "point"


class DocTypes:
    RESOLUTION = "resolution"
    STATEMENT = "statement"


class Qualifications:
    PREAMBLE = "preamble"
    OPERATIONAL = "operational"
