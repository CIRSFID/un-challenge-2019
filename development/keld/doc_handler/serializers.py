import re

from keld.commons.helpers import etree_to_string
from lxml import etree

AKN_NAMESPACE = "http://docs.oasis-open.org/legaldocml/ns/akn/3.0"
AKN4UN = "http://un.org/akn4un"

NS_MAP = {
    None: AKN_NAMESPACE,
    "akn4un": AKN4UN
}

note_pattern = re.compile(r"\[\[(?P<id>\d+)\]\]")


def etree_element(tag, namespace=None, **attributes):
    prefix = "{" + NS_MAP[namespace] + "}"
    e = etree.Element(prefix + tag)
    for k, v in attributes.items():
        if v is None:
            v = ""
        e.set(k, str(v))
    return e


def create_meta(doc):
    def _work():
        frbr_work = etree_element("FRBRWork")
        frbr_this = etree_element("FRBRthis", value=doc.base_uri + "/!main")
        frbr_uri = etree_element("FRBRuri", value=doc.base_uri)
        frbr_date = etree_element("FRBRdate")
        frbr_date.set("date", doc.date)
        frbr_date.set("name", "Date")
        frbr_author = etree_element("FRBRauthor")
        frbr_author.set("href", "#ga")
        frbr_author.set("as", "#author")
        frbr_country = etree_element("FRBRcountry", value="un")
        frbr_work.append(frbr_this)
        frbr_work.append(frbr_uri)
        frbr_work.append(frbr_date)
        frbr_work.append(frbr_author)
        frbr_work.append(frbr_country)
        return frbr_work

    def _expression():
        frbr_expression = etree_element("FRBRExpression")
        frbr_this = etree_element("FRBRthis", value=doc.base_uri + "/eng@/!main")
        frbr_uri = etree_element("FRBRuri", value=doc.base_uri + "/eng@")
        frbr_date = etree_element("FRBRdate")
        frbr_date.set("date", doc.date)
        frbr_date.set("name", "Date")
        frbr_author = etree_element("FRBRauthor")
        frbr_author.set("href", "#ga")
        frbr_author.set("as", "#editor")
        frbr_language = etree_element("FRBRlanguage", language="eng")
        frbr_expression.append(frbr_this)
        frbr_expression.append(frbr_uri)
        frbr_expression.append(frbr_date)
        frbr_expression.append(frbr_author)
        frbr_expression.append(frbr_language)
        return frbr_expression

    def _manifestation():
        frbr_manifestation = etree_element("FRBRManifestation")
        frbr_this = etree_element("FRBRthis", value=doc.base_uri + "/eng@/!main.xml")
        frbr_uri = etree_element("FRBRuri", value=doc.base_uri + "/eng@.xml")
        frbr_date = etree_element("FRBRdate")
        frbr_date.set("date", doc.date)
        frbr_date.set("name", "Date")
        frbr_author = etree_element("FRBRauthor")
        frbr_author.set("href", "#cirsfidUnibo")
        frbr_author.set("as", "#editor")
        frbr_manifestation.append(frbr_this)
        frbr_manifestation.append(frbr_uri)
        frbr_manifestation.append(frbr_date)
        frbr_manifestation.append(frbr_author)
        return frbr_manifestation

    def _references():
        references = etree_element("references", source="#cirsfidUnibo")
        for r in sorted(doc.references.values(), key=lambda x: x.tag):
            references.append(r.serialize())
        return references

    def _classification():
        _classification = etree_element("classification", source="#cirsfidUnibo")
        for r in sorted(doc.classification.values(), key=lambda x: x.tag):
            _classification.append(r.serialize())
        return _classification

    def _sdg_sources():
        _proprietary = etree_element("proprietary", source="#cirsfidUnibo")
        for r in doc.sdg_sources:
            _proprietary.append(r.serialize())
        return _proprietary

    meta = etree_element("meta")
    identification = etree_element("identification", source="#cirsfidUnibo")
    meta.append(identification)
    identification.append(_work())
    identification.append(_expression())
    identification.append(_manifestation())
    publication = etree_element("publication")
    publication.set("date", doc.date)
    publication.set("showAs", "")
    publication.set("name", "")
    meta.append(publication)
    if doc.classification:
        classification = _classification()
        meta.append(classification)
    meta.append(_references())
    if doc.sdg_sources:
        proprietary = _sdg_sources()
        meta.append(proprietary)

    return meta


def markup(text, inlines, root="p"):
    def build_tag(_text, tag, _attributes):
        attributes_string = (
                " " + " ".join(f"""{k}="{v}" """.rstrip() for k, v in attributes.items() if not k.startswith("$"))
        ).rstrip()

        return f"""<{tag}{attributes_string}>{_text}</{tag}>"""

    new_string = ""
    lastindex = 0
    for inline in inlines:
        label, (start, end), attributes = inline
        new_string += text[lastindex:start]
        subreferences = attributes.get("$subreferences")
        if subreferences:
            text_in_tag = markup(text[start:end], subreferences, root=None)
        else:
            text_in_tag = text[start:end]
        new_string += build_tag(text_in_tag, tag=label, _attributes=attributes)
        lastindex = end
    new_string += text[lastindex:]
    if root is None:
        return new_string
    node = f"<{root}>{new_string}</{root}>"
    try:
        return etree.fromstring(node)
    except:
        print(node)
        raise


def akn_string(akn, document):
    # JUST SO YOU KNOW, I'M ASHAMED OF THIS... but also kinda proud
    authorial_notes = {}

    def get_note(match):
        _id = match.group("id")
        return authorial_notes[_id]

    for authorial_note in document.authorial_notes:
        note_id = authorial_note.attributes["marker"]
        note_as_string = etree_to_string(authorial_note.serialize())
        authorial_notes[note_id] = note_as_string
    return re.sub(note_pattern, get_note, etree_to_string(akn))
