from keld.ner_sdg import get_entities
from keld.commons.helpers import normalize_name, AUTHORITY_MAP


NER2AKN = {
    "org": "organization",
    "norp": "organization",
    "law": "ref",
    "gpe": "location",
    "facility": "location",
    "loc": "location",
    "deadline": "event"
}


DISCARD = ["person", "date"]


def ner_entities(text, document):
    new_entities = []
    entities = get_entities(text)
    for e in entities:
        label, (start, end) = e
        if label in DISCARD:
            continue
        label = label.lower()
        value = text[start:end]
        new_label = NER2AKN.get(label, label)
        attributes = get_attributes(new_label, value, document)
        new_entities.append((new_label, (start, end), attributes))
    return new_entities


def get_attributes(label, value, document):
    attributes = {}
    document.ref_counter[label] += 1
    attributes["eId"] = f"{label}_{document.ref_counter[label]}"
    name = normalize_name(value)
    name = AUTHORITY_MAP.get(name, name)
    attributes["refersTo"] = f"#{name}"
    attributes["$reference"] = dict(
        tag=f"TLC{label.title()}",
        eId=name,
        href=f"/akn/ontology/{label}s/un/{name}",
        showAs=value
    )
    if label == "ref":
        attributes["href"] = ""
        attributes["$reference"]["tag"] = "TLCReference"
        attributes["$reference"]["name"] = ""
    return attributes

