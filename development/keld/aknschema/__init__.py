import requests
import json
import traceback
from lxml import etree
import os

VALIDATION_ENDPOINT = "http://sinatra.cirsfid.unibo.it/node/akn-validation/Validate/"

here = os.path.dirname(os.path.abspath(__file__))
SCHEMA_PATH = os.path.join(here, "akomantoso30.xml")
akn_schema= etree.parse(SCHEMA_PATH)
schema = etree.XMLSchema(akn_schema)


def remote_validate(akn_string):
    r = requests.post(
        VALIDATION_ENDPOINT,
        json={"source": akn_string},
        timeout=30
    )
    try:
        if r.status_code == 200:
            validation = json.loads(r.text)
            return validation["success"], validation
        else:
            return False, dict(success="unkown", errors=[dict(message=f"Could not validate from remote ({r.status_code}): {r.text}")])
    except:
        return False, dict(success="unkown", errors=[dict(message=traceback.format_exc())])


def local_validate(akn):
    valid = schema.validate(akn)
    if not valid:
        validation = dict(success=False, errors=[str(err) for err in schema.error_log])
        return False, validation
    return True, {"success": True}


def validate(akn_string, remote=False):
    if remote:
        return remote_validate(akn_string)
    else:
        return local_validate(etree.fromstring(akn_string))
