import zipfile

from bs4 import BeautifulSoup

from .doc_to_text import get_text


def read_doc(filepath):
    doc_parts = {}
    zip_doc = zipfile.ZipFile(filepath)
    for docname in zip_doc.namelist():
        if docname.endswith(".xml"):
            doc_part = zip_doc.open(docname)
            xml_doc = BeautifulSoup(doc_part, "lxml")
            doc_parts[docname] = xml_doc
            # with open(docname.split("/")[-1], "w") as f:
            #     f.write(str(xml_doc))
        elif docname.endswith((".gif", ".png", ".jpg", ".jpeg")):
            img = zip_doc.open(docname).read()
            doc_parts["docname"] = img
    return doc_parts


def load_document(docpath):
    doc_parts = read_doc(docpath)
    return get_text(doc_parts)
