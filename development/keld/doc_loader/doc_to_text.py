import re


def get_text(doc_parts):
    return [
        ("coverpage", _get_coverpage(doc_parts)),
        ("main", _get_main_body(doc_parts)),
        ("footnotes", _get_footnotes(doc_parts))
    ]


def clear_special(text):
    new_text = re.sub(r"\xc2\xa0", r" ", text)
    return new_text


def _get_coverpage(doc_parts):
    main = doc_parts["word/header3.xml"]
    body = main.find("body")
    return _get_text(body)


def _get_main_body(doc_parts):
    main = doc_parts["word/document.xml"]
    body = main.find("body").find("w:body")
    return _get_text(body)


def _get_footnotes(doc_parts):
    main = doc_parts["word/footnotes.xml"]
    body = main.find("body")
    return _get_footnotes_text(body)


def _get_text(body):
    text_parts = []
    for e in body.find_all("w:p"):
        text = ""
        for wr in e.find_all("w:r"):
            wt = wr.find("w:t")
            if wt is not None:
                _text = f"{wt.get_text()}"
                text += _text
            footnote = wr.find("w:footnotereference")
            if footnote is not None:
                fn_id = footnote.get("w:id")
                text += f"[[{fn_id}]]"
        text_parts.append(text)
    return clear_special("\n".join(text_parts))


def _get_footnotes_text(body):
    all_footnotes = []
    for footnote_el in body.find_all("w:footnote"):
        text_parts = []
        footnote_id = footnote_el.get("w:id")
        if int(footnote_id) < 1:
            continue
        for e in footnote_el.find_all("w:p"):
            text = ""
            for wr in e.find_all("w:r"):
                wt = wr.find("w:t")
                if wt is not None:
                    _text = f"{wt.get_text()}"
                    text += _text
            text_parts.append(text)
        all_footnotes.append((footnote_id, clear_special("\n".join(text_parts))))
    return all_footnotes
