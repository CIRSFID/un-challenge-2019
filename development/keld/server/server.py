import os

from keld.main import parse
from flask import Flask, request, render_template, send_from_directory, redirect, make_response
from werkzeug.utils import secure_filename

PORT = 3000

THIS_FILES_DIR = os.path.dirname(os.path.realpath(__file__))
template_dir = os.path.abspath(os.path.join(THIS_FILES_DIR, "server_resources", "templates"))
UPLOAD_FOLDER = os.path.abspath(os.path.join(THIS_FILES_DIR, "uploads"))
CONVERTED_FOLDER = os.path.abspath(os.path.join(THIS_FILES_DIR, "converted"))
os.makedirs(UPLOAD_FOLDER, exist_ok=True)

app = Flask(__name__, template_folder=template_dir)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['CONVERTED_FOLDER'] = CONVERTED_FOLDER
ALLOWED_EXTENSIONS = ["DOC", "doc", "docx"]


# just to serve static files, so we don't need to setup a server like nginx
@app.route('/src/<path:path>')
def send_static(path):
    return send_from_directory("service_resources/src", path)


@app.route('/')
def home():
    return render_template("index.html")


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def download(filename):
    converted = os.path.join(app.config['CONVERTED_FOLDER'])
    response = make_response(send_from_directory(directory=converted, filename=filename))
    response.headers['Content-Disposition'] = f'attachment; filename={filename}'
    return response


@app.route('/convert/', methods=['POST'])
def convert():
    if request.method == 'POST':
        if 'file' not in request.files:
            print("No filepath")
            return redirect("/")
        file = request.files['file']
        if file.filename == '':
            print('No selected file')
            return redirect("/")
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            filepath = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            file.save(filepath)
            valid, respath = parse(filepath, output_dir=CONVERTED_FOLDER, write_akn=True)
            print(filename, "VALID:", valid)
            out_file = os.path.basename(respath)
            return download(out_file)


if __name__ == "__main__":
    app.run(debug=True, host='127.0.0.1', port=PORT)
