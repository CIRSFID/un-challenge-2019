import re

from keld.ner_sdg.spacy_model_user import SpacyModelUser


class ParagraphQualifier(SpacyModelUser):

    @staticmethod
    def normalize(string):
        """
        This function normalize the input string.
        """
        return string.lower().replace('’', '\'').replace("“", "\"").replace("”", "\"")

    def can_be_verb(self, token, original_label="VB", singular=False):
        """
        This function take a token and its original label
        and try to see if adding a subject (He or They) changes the label.
        If yes, a new label is returned, along with the subject (He or They)
        If not, the original label is preserved and the subject returned will be None
        """
        if singular:
            subject = "He"
        else:
            subject = "They"

        new_token = subject + " " + token

        new_label = "None"
        if len(self.get_postag(new_token)) >= 2:
            new_label = self.get_postag(new_token)[1].tag_  # (checking the 2nd token)
        
        if new_label.startswith("V"):
            return [subject, new_label]
        elif singular == False:  # If we already tried plural
            return self.can_be_verb(token, original_label, True)  # Recursively checking the singular
        else:
            return [None, original_label]  # If neither singular nor plural subjects work, return the original label 


    def get_postag(self, p):
        """
        This function get the pos_tag tokenizing the text
        """
        return self.nlp(p)

    def deleting_until_the_comma(self, p, excluding = list(['none'])):
        """
        This function is for excluding introcutory statements.
        Shortly, it loops the token of the give text searching for two patterns ("coma+verb" and "coma+pronoun").
        This two patterns represents the way in which usually a sentence start after an introductory statement.
        E.g. In "In this regard, we note that ... " a comma and the pronoum "we" will be detected
    
        After the detection of the patterns, we split at the comma (to do so we go back one token, and then we split)
    
        """
        p = ' '.join(p.split())                         # remove multiple spaces
        tokenized_sentence = list(self.get_postag(p))   # tokenizing the sentence
        token_to_find_1 = ''
        token_to_find_2 = ''
        pattern = 0

        for i in range(1, len(tokenized_sentence) - 1):
            previous = tokenized_sentence[i - 1]
            current = tokenized_sentence[i]
            next = tokenized_sentence[i + 1]
            # searching for a the patterns
            # Searching for pattern 1
            if current.tag_ == "," and next.tag_ == "PRP":
                token_to_find_1 = previous.text  # going 1 token back
                token_to_find_2 = current.text
                pattern = 1
                break

            # Searching for pattern 2
            elif current.tag_ == "," and next.tag_.startswith('V') and next.tag_ not in excluding:
                token_to_find_1 = previous.text  # going 1 token back
                token_to_find_2 = current.text
                pattern = 2
                break

        # If one of the two pattern (", + PRP" or ", + VERB") exists and we have the two consecutive elements of the pattern not empty
        if token_to_find_1 != '' and token_to_find_2 != '':

            # Having the two elements we have to use them in combination and split the sentence using this combination.
            # (although unlikely, we have to consider the possibility that there is a space after the comma)
            combined_tokens = token_to_find_1 + token_to_find_2
            combined_tokens_wspace = token_to_find_1 + " " + token_to_find_2

            # (without a space before the coma)
            if len(p.split(combined_tokens)) > 1:  ### Hardly we will have more than 2

                the_significative_slice = p.split(combined_tokens)[1]
                the_offset_to_add = re.search(combined_tokens, p.lower()).span()[1]             # searching for the offset to return
                
                if p.lower()[the_offset_to_add:the_offset_to_add+1].isspace():
                    the_offset_to_add = the_offset_to_add+1 
                
                return [the_significative_slice, the_offset_to_add] 

            # (with a space before the coma [rare but possible])
            elif len(p.split(combined_tokens_wspace)) > 1:  ### Hardly we will have more than 2

                the_significative_slice = p.split(combined_tokens_wspace)[1]
                the_offset_to_add = re.search(combined_tokens_wspace, p.lower()).span()[1]      # searching for the offset to return
                
                if p.lower()[the_offset_to_add:the_offset_to_add+1].isspace():
                    the_offset_to_add = the_offset_to_add+1 
                
                return [the_significative_slice,the_offset_to_add]   

            # Exception found, returning the original p
            else:
                # --#print("\n[This should not happen] ERROR: A pattern was found but it was not possible to split the sentence using the pattern")
                return ''

        else:
            # --#print("\n[No pattern found]")
            return ''

    def get_first_tokens(self, p, lenght=7):
        """
        This function get the first TOKENS and their POS
        Returning a dictionary with the following structure
        {
            label_1 : VBZ,
            token_1 : aknowledges,
            ...
        }
    
        """

        first_tokens = dict()

        processed_p = self.get_postag(p)
        for i, t in enumerate(processed_p):
            if i < lenght:
                label_name = "label_" + str(i + 1)
                token_name = "token_" + str(i + 1)
                first_tokens[label_name] = t.tag_
                first_tokens[token_name] = t.text

        return first_tokens


    def qualify(self, p):
        """
        This function qualify a given string as preambular or operational
        """
        if p == '':
            return [None, None, None, (0, 0)]

        p = p.strip()
        qualification = None
        term = None
        
        TOKEN_SLOT_SIZE = 2

        start_plus = 0      # in recursivity, this will add the accumulated offsets
        end_plus = 0        # in recursivity, this will add the accumulated offsets
    
        list_of_prep = [
            'at',
            'by',
            'into',
            'on',
            'further',
            'after',  # !!!
            'before',  # !!!
            'up',
            'upon',
            'for',
            #'to',   # if you want appealsTo instead of just appeal
            'apart',
            'away',
            'off',
            'forward',
            'through',
        ]

        introductory_words = [
            'a',
            'an',
            'the',
            'there',
            'there',
            'as',
            'by',
            'in',
            'for',
            'given',
            'since',
            'because',
            'after',
            'before',
            'while',
            'with',
            'wherever',
            'where',
            'whenever',
            'when',
        ]

        list_of_adjectives = [
            'deep',
            'grave',
            'profound',
            'serious',
            'firm',
            'special',  #
            'huge',
        ]

        list_of_prep_for_adj = [
            "of",
            "at",
            "that",
        ]

        list_of_nouns = [
            'satisfaction',
            'appreciation',
            'commitment',
            'sympathy',
            'gratitude',
            'encouragement',
            'conviction',
            'hope',
            'approval',
            'interest',
            # 'emphasis',       #   with special emphasis
            # 'focus',          #   with special focus
            'support',
            'concern',
            'disappointment',
            'regret',
            'attention',        # special attention
            'request',          ##  its request
        ]

        # =================
        #   .Finding the subject (checking the first token)
        # ================= 
        case = "na"
        first_tokens = self.get_first_tokens(
            p)  # in this dict I will store dynamically the first tokens of the given text

        label_1 = first_tokens.get('label_1', '')  # (checking the 1st token)
        token_1 = first_tokens.get('token_1', '')  # (checking the 1st token)

        term = token_1.lower()
        terms_join = term  # this is for searching the offset

        # --#print(p,"\n")

        # Checking if adding the subject give us a better tokenization (now the ckeck is on the second token)
        # if label_1 == "VBG":   # very few times it does not work ("bearing" is recognized as a noun!) 
        if label_1 == "VBG" or token_1.endswith('ing'):
            qualification = "preambular"
            case = "__ING"

            # [[ Considering paragraphs of at least 4 tokens ]]

            if len(first_tokens) >= 4 * TOKEN_SLOT_SIZE:
    
                # e.g. "Continuing to underline"
                if first_tokens.get('label_2', '') == "TO" and first_tokens.get('label_3', '').lower().startswith('v'):
                    term = term + first_tokens.get('token_2', '').capitalize() + first_tokens.get('token_3',
                                                                                                  '').capitalize()
                    terms_join = " ".join(
                        [terms_join, first_tokens.get('token_2', '').capitalize(),
                         first_tokens.get('token_3', '').capitalize()])
                    case = case + "_to_vb"

                # Starting with auxiliar verb "having"
                if first_tokens.get('token_1', '').lower() == "having":

                    case = case + "_having"

                    # having also
                    if first_tokens.get('token_2', '').lower() == "also":
                        term = term + first_tokens.get('token_2', '').capitalize()
                        terms_join = " ".join([terms_join, first_tokens.get('token_2', '').capitalize()])
                        case = case + "_also"

                        # having also considered
                        if first_tokens.get('label_3', '') == "VBD" or first_tokens.get('label_3',
                                                                                        '') == "VBN":  ## JJ too?
                            term = term + first_tokens.get('token_3', '').capitalize()
                            terms_join = " ".join([terms_join, first_tokens.get('token_3', '').capitalize()])
                            case = case + "_vbdVbn"

                    # having considered
                    elif first_tokens.get('label_2', '') == "VBD" or first_tokens.get('label_2',
                                                                                      '') == "VBN":  ## JJ too?

                        term = term + first_tokens.get('token_2', '').capitalize()
                        terms_join = " ".join([terms_join, first_tokens.get('token_2', '').capitalize()])
                        case = case + "_vbdVbn"

                    # having further considered
                    elif first_tokens.get('label_3', '') == "VBD" or first_tokens.get('label_3',
                                                                                      '') == "VBN":  ## JJ too?

                        term = term + first_tokens.get('token_2', '').capitalize() + first_tokens.get('token_3',
                                                                                                      '').capitalize()
                        terms_join = " ".join(
                            [terms_join, first_tokens.get('token_2', '').capitalize(),
                             first_tokens.get('token_3', '').capitalize()])
                        case = case + "_*_vbdVbn"

                        # having further called upon [RARE but possible]
                        if first_tokens.get('token_4', '').lower() in list_of_prep:
                            term = term + first_tokens.get('token_4', '').capitalize()
                            terms_join = " ".join([terms_join, first_tokens.get('token_4', '').capitalize()])
                            case = case + "_prep"

                    # EXCEPTION FOUND
                    else:
                        case = case + "_no-case-1"


                # Starting with auxiliar verb "being"
                elif first_tokens.get('token_1', '').lower() == "being":

                    case = case + "_being"

                    # being also
                    if first_tokens.get('token_2', '').lower() == "also":
                        term = term + first_tokens.get('token_2', '').capitalize()
                        terms_join = " ".join([terms_join, first_tokens.get('token_2', '').capitalize()])
                        case = case + "_also"

                        # being also concerned
                        if first_tokens.get('label_3', '') == "VBD" or first_tokens.get('label_3',
                                                                                        '') == "VBN":  ## JJ too?
                            term = term + first_tokens.get('token_3', '').capitalize()
                            terms_join = " ".join([terms_join, first_tokens.get('token_3', '').capitalize()])
                            case = case + "_vbdVbn"

                            # being also concerned at
                            if first_tokens.get('token_4', '').lower() in list_of_prep:
                                term = term + first_tokens.get('token_4', '').capitalize()
                                terms_join = " ".join([terms_join, first_tokens.get('token_4', '').capitalize()])
                                case = case + "_prep"

                    # being concerned
                    elif first_tokens.get('label_2', '') == "VBD" or first_tokens.get('label_2',
                                                                                      '') == "VBN":  ## JJ too?

                        term = term + first_tokens.get('token_2', '').capitalize()
                        terms_join = " ".join([terms_join, first_tokens.get('token_2', '').capitalize()])
                        case = case + "_vbdVbn"

                        # being concerned at
                        if first_tokens.get('token_3', '').lower() in list_of_prep:
                            term = term + first_tokens.get('token_3', '').capitalize()
                            terms_join = " ".join([terms_join, first_tokens.get('token_3', '').capitalize()])
                            case = case + "_prep"

                        # being further concerned
                        elif first_tokens.get('label_3', '') == "VBD" or first_tokens.get('label_3',
                                                                                          '') == "VBN":  ## JJ too?

                            term = term + first_tokens.get('token_2', '').capitalize() + first_tokens.get('token_3',
                                                                                                          '').capitalize()
                            terms_join = " ".join(
                                [terms_join, first_tokens.get('token_2', '').capitalize(),
                                 first_tokens.get('token_3', '').capitalize()])
                            case = case + "_*_vbdVbn"

                            # being further concerned at [RARE but possible]
                            if first_tokens.get('token_4', '').lower() in list_of_prep:
                                term = term + first_tokens.get('token_4', '').capitalize()
                                terms_join = " ".join([terms_join, first_tokens.get('token_4', '').capitalize()])
                                case = case + "_prep"

                    # EXCEPTION FOUND
                    else:
                        case = case + "_no-case-1"

                # Starting not with "having" nor "being"
                else:
                    case = case + "_noAux"

                    # e.g. "Believing also"
                    if first_tokens.get('token_2', '').lower() == "also":
                        term = term + first_tokens.get('token_2', '').capitalize()
                        terms_join = " ".join([terms_join, first_tokens.get('token_2', '').capitalize()])
                        case = case + "_also"

                        # Taking also note [ad-hoc: 'note']
                        if first_tokens.get('token_3', '').lower() == "note":
                            term = term + first_tokens.get('token_3', '').capitalize()
                            terms_join = " ".join([terms_join, first_tokens.get('token_3', '').capitalize()])
                            case = case + "_note[ad-hoc]"

                        # Bearing also in mind [ad-hoc: 'in mind']
                        elif first_tokens.get('token_3', '').lower() == "in" and first_tokens.get('token_4',
                                                                                                  '').lower() == "mind":
                            term = term + first_tokens.get('token_3', '').capitalize() + first_tokens.get('token_4',
                                                                                                          '').capitalize()
                            terms_join = " ".join(
                                [terms_join, first_tokens.get('token_3', '').capitalize(),
                                 first_tokens.get('token_4', '').capitalize()])
                            case = case + "_inmind[ad-hoc]"

                        # Calling also upon --- taking also into [CHECK]
                        elif first_tokens.get('token_3', '').lower() in list_of_prep:
                            term = term + first_tokens.get('token_3', '').capitalize()
                            terms_join = " ".join([terms_join, first_tokens.get('token_3', '').capitalize()])
                            case = case + "_prep"

                            # Taking also into consideration
                            if first_tokens.get('token_3', '').lower() == 'into' and first_tokens.get('label_4',
                                                                                                      '').lower().startswith(
                                    'nn'):
                                term = term + first_tokens.get('token_4', '').capitalize()
                                terms_join = " ".join([terms_join, first_tokens.get('token_4', '').capitalize()])
                                case = case + "_nn(s)"

                    # Taking note [ad-hoc: 'note']
                    elif first_tokens.get('token_2', '').lower() == "note":
                        term = term + first_tokens.get('token_2', '').capitalize()
                        terms_join = " ".join([terms_join, first_tokens.get('token_2', '').capitalize()])
                        case = case + "_note[ad-hoc]"

                        # Taking note also [ad-hoc: 'note']
                        # if first_tokens.get('token_3', '').lower() == "also":
                        #   term = term+first_tokens.get('token_3', '').capitalize()
                        #   terms_join = " ".join([terms_join,first_tokens.get('token_3', '').capitalize()])
                        #   case = case+"_also"

                    # -ing in mind [ad-hoc: 'in mind']
                    elif first_tokens.get('token_2', '').lower() == "in" and first_tokens.get('token_3',
                                                                                              '').lower() == "mind":
                        term = term + first_tokens.get('token_2', '').capitalize() + first_tokens.get('token_3',
                                                                                                      '').capitalize()
                        terms_join = " ".join(
                            [terms_join, first_tokens.get('token_2', '').capitalize(),
                             first_tokens.get('token_3', '').capitalize()])
                        case = case + "_inmind[ad-hoc]"

                    # -ing tribute [ad-hoc: 'tribute']
                    elif first_tokens.get('token_2', '').lower() == "tribute":
                        term = term + first_tokens.get('token_2', '').capitalize()
                        terms_join = " ".join([terms_join, first_tokens.get('token_2', '').capitalize()])
                        case = case + "_tribute[ad-hoc]"

                    # Calling upon  [CHECK]
                    elif first_tokens.get('token_2', '').lower() in list_of_prep:
                        term = term + first_tokens.get('token_2', '').capitalize()
                        terms_join = " ".join([terms_join, first_tokens.get('token_2', '').capitalize()])
                        case = case + "_prep"

                        # Taking also into consideration
                        if first_tokens.get('token_2', '').lower() == 'into' and first_tokens.get('label_3',
                                                                                                  '').lower().startswith(
                                'nn'):
                            term = term + first_tokens.get('token_3', '').capitalize()
                            terms_join = " ".join([terms_join, first_tokens.get('token_3', '').capitalize()])
                            case = case + "_nn(s)"

            # [[ Considering paragraphs smaller than 4 tokens ]]
            else:
                pass # we take just the first token, and we will check for a "with (+ JJ) + NN" pattern further on

        # Starting with a pronoun (very rarely, some paragraph start with pronouns like 'we')
        elif label_1.startswith(
                "PRP"):  # token_1.lower() == "we":     # per prendere "further + V.*" che inizia con un RB !
            qualification = "operational"
            case = "__PRP"
    
            if len(first_tokens) >= 2*TOKEN_SLOT_SIZE and len(first_tokens) >= 3*TOKEN_SLOT_SIZE:
    
                if first_tokens.get('token_2', '').lower() == "also" and first_tokens.get('label_3', '').lower().startswith("v"):

                    term = first_tokens.get('token_2', '').lower() + first_tokens.get('token_3', '').capitalize()
                    terms_join = " ".join(
                        [first_tokens.get('token_2', '').lower(), first_tokens.get('token_3', '').capitalize()])
                    case = case + "_also_verb"

                elif first_tokens.get('token_2', '').lower() == "therefore" and first_tokens.get('label_3',
                                                                                                 '').lower().startswith(
                        "v"):
                    term = first_tokens.get('token_2', '').lower() + first_tokens.get('token_3', '').capitalize()
                    terms_join = " ".join(
                        [first_tokens.get('token_2', '').lower(), first_tokens.get('token_3', '').capitalize()])
                    case = case + "_therefore_verb"

                elif first_tokens.get('label_2', '').lower().startswith("v"):
                    term = first_tokens.get('token_2', '').lower()
                    terms_join = " ".join([first_tokens.get('token_2', '').lower()])
                    case = case + "_verb"

                elif first_tokens.get('label_2', '') == "MD":
                    term = first_tokens.get('token_2', '').lower()
                    terms_join = " ".join([first_tokens.get('token_2', '').lower()])
                    case = case + "_modal"  # [CHECK]
                    qualification = "operational"
                    if len(first_tokens) >= 3*TOKEN_SLOT_SIZE:
                        if first_tokens.get('label_3', '').lower().startswith('v'):
                            term = first_tokens.get('token_3', '').lower()
                            terms_join = " ".join([first_tokens.get('token_3', '').lower()])
                            case = case + "_verb"  # [CHECK]

                elif first_tokens.get('label_2', '') == "," or first_tokens.get('token_2', '') == ",":
                    #--#print("\nRecalculation C")
                    comma_splitted_sentence = self.deleting_until_the_comma(p, list(["VBD","VBN","VBG"]))       # going to the ", verb" or ", pronoun" part
                    if comma_splitted_sentence != '':
                        the_significative_slice = comma_splitted_sentence[0].strip()    
                        the_offset_to_add = comma_splitted_sentence[1]          # getting the offset
                        recursion = self.qualify(str(the_significative_slice))       # Recursively re-analyze
                        
                        try:
                            qualification = recursion[0]
                            term = recursion[1]
                            terms_join = str(the_significative_slice)[recursion[3][0]:recursion[3][1]]
                        except:
                            #--#print("unsuccessful recursion A",case)
                            term = None
                    else:
                        return [None, None, None, (0, 0)]
                
                else:
                    return [None, None, None, (0, 0)]

            else:
                return [None, None, None, (0, 0)]       # If there is just a pronoun there is no classification

        # Ad-HOC: take(s) note
        elif (token_1.lower() == "take" or token_1.lower() == "takes") and (
                first_tokens.get('token_2', '').lower() == "note" or first_tokens.get('token_2',
                                                                                      '').lower() == "notes"):
            qualification = "operational"
            term = term + first_tokens.get('token_2', '').capitalize()
            terms_join = " ".join([terms_join, first_tokens.get('token_2', '').capitalize()])
            case = "__takesnotes"

        # Ad-HOC: bear(s) in mind
        elif (token_1.lower() == "bear" or token_1.lower() == "bears") and (
                first_tokens.get('token_2', '').lower() == "in" or first_tokens.get('token_2', '').lower() == "in"):
            qualification = "operational"
            term = term + first_tokens.get('token_2', '').capitalize() + first_tokens.get('token_3', '').capitalize()
            terms_join = " ".join([terms_join, first_tokens.get('token_2', '').capitalize(),
                                   first_tokens.get('token_3', '').capitalize()])
            case = "__bearsinmind"

        # Ad-HOC: pay(s) tribute
        elif token_1.lower().startswith('pay') and first_tokens.get('token_2', '').lower().startswith('tribute'):
            qualification = "operational"
            term = term + first_tokens.get('token_2', '').capitalize()
            terms_join = " ".join([terms_join, first_tokens.get('token_2', '').capitalize()])
            case = "__paytribute"

        ###|### 1st token_1 is already a 3rd person Verb (VBZ)
        elif label_1 == "VBZ":
            qualification = "operational"
            case = "__VBZ"
            # subject = "He"

            # Calls upon
            if first_tokens.get('token_2', '').lower() in list_of_prep:
                term = term + first_tokens.get('token_2', '').capitalize()
                terms_join = " ".join([terms_join, first_tokens.get('token_2', '').capitalize()])
                case = case + "_prep"

        ###|### 1st token_1 is already a verb: we have to discover the subject
        elif label_1 == "VBP":  ### VBP (1st-2nd singul.)   are
            qualification = "operational"
            case = "__VBP"
            # subject = "They"

        elif label_1 == "VB":  # or token_1.lower() == "also":  ### VBP (1st-2nd singul.)   are # VB emphasize
            case = "__VBP"
            search_subj_lab = self.can_be_verb(token_1, label_1)  # Searching for a feasible sibject
            if search_subj_lab[0] is not None:
                qualification = "operational"
                case = case + "_" + search_subj_lab[0] + "-PHRASE [subject guessed from VB]"
                # subject = search_subj_lab[0]
            else:
                # --#print("no-qualification (1)",case)
                term = None

        ###|### 1st token_1 is also [RB]
        elif token_1.lower() == "also":  # per prendere "further + V.*" che inizia con un RB !
            case = "__also"

            # --#print("Recalculating (excluding initial token_1",token_1," whose label_1 is ",label_1,")")
            new_sent = re.sub(r'^' + token_1, '', p)
            try:
                recursion = self.qualify(new_sent)  # Recursively re-analyze
                qualification = recursion[0]
                first_char_term = recursion[1][0].upper()
                term = term + first_char_term + recursion[1][1:]
                terms_join = " ".join([terms_join, first_char_term + recursion[1][1:]])
            except:
                term = None

        ###|### 1st and 2nd token are "once" "again"
        elif token_1.lower() == "once" and first_tokens.get("token_2", "").lower() == "again":  # per prendere "further + V.*" che inizia con un RB !
            case = "__onceagain"

            # --#print("Recalculating (excluding initial tokens ", token_1, " and ", first_tokens.get('token_2', ''), " (whose labels are ",label_1, " ", first_tokens.get('label_2', ''), ")")
            new_sent = re.sub(r'^' + token_1, '', p)
            new_sent = re.sub(r'^' + first_tokens.get('token_2', ''), '', new_sent)
            try:
                recursion = self.qualify(new_sent)  # Recursively re-analyze
                qualification = recursion[0]
                first_char_term = recursion[1][0].upper()
                term = term + first_char_term + recursion[1][1:]
                terms_join = " ".join([terms_join, first_char_term + recursion[1][1:]])
            except:
                term = None

        ###|### 1st token_1 is an adverb: skipping and trying to check the 2nd token_1
        elif label_1.startswith("RB") or token_1.endswith(
                'ly'):  # a volte un RB viene malinterpretato in un NN (e.g. in "deeply concerned also"), consider creating a can_be_adverb function which start a fake sentence with a "they are" to get the right role of any NN ending in "ly"
            case = "__RB"

            # deeply allarmed (RB + JJ)
            if first_tokens.get('label_2', '').lower() == 'jj' or first_tokens.get('label_2', '').lower() == 'vbn' or \
                    first_tokens.get("label_2", "").lower() == 'vbd':
                qualification = "preambular"
                case = case + "__JJVbnVbd"
                term = term + first_tokens.get('token_2', '').capitalize()
                terms_join = " ".join([terms_join, first_tokens.get('token_2', '').capitalize()])

            # deeply deploring (RB + VBG)
            elif first_tokens.get('label_2', '').lower() == 'vbg' or first_tokens.get('token_2', '').endswith('ing'):
                qualification = "preambular"
                case = case + "__VBG"
                term = term + first_tokens.get('token_2', '').capitalize()
                terms_join = " ".join([terms_join, first_tokens.get('token_2', '').capitalize()])

            # deeply deplores (RB + Verb)
            elif first_tokens.get('label_2', '').lower().startswith('v'):
                qualification = "operational"
                case = case + "__VB"
                term = term + first_tokens.get('token_2', '').capitalize()
                terms_join = " ".join([terms_join, first_tokens.get('token_2', '').capitalize()])

            # deeply call upon
            elif first_tokens.get('label_2', '').lower().startswith('v') and first_tokens.get('token_3',
                                                                                              '').lower() in list_of_prep:
                qualification = "operational"
                case = case + "_verbPrep"
                term = term + first_tokens.get('token_2', '').capitalize() + first_tokens.get('token_3',
                                                                                              '').capitalize()
                terms_join = " ".join(
                    [terms_join, first_tokens.get('token_2', '').capitalize(),
                     first_tokens.get('token_3', '').capitalize()])

            # deeply (NN)   # when a verb is confused with a NN
            elif first_tokens.get('label_2', '').lower() == 'nn':
                case = "__NN"

                search_subj_lab = self.can_be_verb(first_tokens.get('token_2', ''), first_tokens.get("label_2", ""))
                if search_subj_lab[0] is not None:
                    qualification = "operational"
                    case = case + "_" + search_subj_lab[0] + "-PHRASE [subject guessed from RB_NN]"
                    # subject = search_subj_lab[0]
                else:
                    # --#print("no-qualification (2a)",case)
                    # --#print('1st label: ', first_tokens.get('label_1', ''), '2nd label: ', first_tokens.get('label_2', ''))
                    term = None

            # deeply deploring (RB + NNS)   # when a verb is confused with a NNS
            elif first_tokens.get('label_2', '').lower() == 'nns':
                case = "__NNS"

                # ad hoc: -ly appeal(s)
                if first_tokens.get('token_2', '').lower() == 'appeals':
                    qualification = "operational"
                    case = case + "_appeals"
                    term = term + first_tokens.get('token_2', '').capitalize()
                    terms_join = " ".join([terms_join, first_tokens.get('token_2', '').capitalize()])

                if first_tokens.get('label_2', '').lower().startswith('v') and first_tokens.get('token_3',
                                                                                                '').lower() in list_of_prep:
                    qualification = "operational"
                    case = case + "_verbPrep"
                    term = term + first_tokens.get('token_2', '').capitalize() + first_tokens.get('token_3',
                                                                                                  '').capitalize()
                    terms_join = " ".join(
                        [terms_join, first_tokens.get('token_2', '').capitalize(),
                         first_tokens.get('token_3', '').capitalize()])

                search_subj_lab = self.can_be_verb(first_tokens.get('token_2', ''), first_tokens.get('label_2', ''),
                                                   True)  # Searching for a feasible sibject (True forces to search a singular subject)
                if search_subj_lab[0] is not None:
                    qualification = "operational"
                    case = case + "_" + search_subj_lab[0] + "-PHRASE [subject guessed from RB_NNS]"
                    # subject = search_subj_lab[0]
                else:
                    # --#print("no-qualification (2b)",case)
                    # --#print('1st label: ', first_tokens.get('label_1', ''), '2nd label: ', first_tokens.get('label_2', ''))
                    term = None

            else:
                # --#print("no-qualification (2c)",case)
                term = None
                # --#print('1st label: ', first_tokens.get('label_1', ''), '2nd label: ', first_tokens.get('label_2', ''))

        ###|### 1st token_1 is a noun: but possibly wrongly
        elif label_1 == "NNS":
            case = "__NNS"
            search_subj_lab = self.can_be_verb(token_1, label_1,
                                               True)  # Searching for a feasible sibject (True forces to search a singular subject)
            if search_subj_lab[0] is not None:
                qualification = "operational"
                case = case + "_" + search_subj_lab[0] + "-PHRASE [subject guessed from NNS]"
                # subject = search_subj_lab[0]

                if first_tokens.get('token_2', '').lower() in list_of_prep:
                    term = term + first_tokens.get('token_2', '').capitalize()
                    terms_join = " ".join([terms_join, first_tokens.get('token_2', '').capitalize()])
                    case = case + "_prep"

            else:
                # --#print("no-qualification (3)",case)
                term = None

        elif label_1 == "NN":
            case = "__NN"
            search_subj_lab = self.can_be_verb(token_1, label_1)  # Searching for a feasible sibject

            # Ad-HOC: as - If it is a not-detected introductory word (e.g. 'as', 'while')
            if first_tokens.get('token_1', '').lower() in introductory_words:
                # --#print("\nRecalculation A")
                comma_splitted_sentence = self.deleting_until_the_comma(p)       # going to the ", verb" or ", pronoun" part
                if comma_splitted_sentence != '':

                    try:
                        the_significative_slice = comma_splitted_sentence[0].strip() 
                        the_offset_to_add = comma_splitted_sentence[1]          # getting the offset
                        recursion = self.qualify(str(the_significative_slice))  # Recursively re-analyze
                        qualification = recursion[0]
                        term = recursion[1]
                        terms_join = str(the_significative_slice)[recursion[3][0]:recursion[3][1]]
                    except:
                        term = None

            # if it is actually a verb
            elif search_subj_lab is not None:
                if search_subj_lab[0] is not None:
                    qualification = "operational"
                    case = case + "_" + search_subj_lab[0] + "-PHRASE [subject guessed from NN]"
                    # subject = search_subj_lab[0]

                    # if it is a phrasal verb
                    if first_tokens.get('token_2', '').lower() in list_of_prep:
                        term = term + first_tokens.get('token_2', '').capitalize()
                        terms_join = " ".join([terms_join, first_tokens.get('token_2', '').capitalize()])
                        case = case + "_prep"

                else:
                    # --#print("no-qualification (3)",case)
                    term = None
            else:
                # --#print("no-qualification (3b)",case)
                term = None


        ###|### 1st token_1 is an adjective ## NB: JJ and NN are sometimes confused
        elif label_1 == "JJ":
            qualification = "preambular"
            case = "__JJ"

            # sometimes a JJ can be confused with a RB (e.g. further), so, check the existence of a verb as a second token
            search_subj_lab = self.can_be_verb(first_tokens.get('token_2', ''), first_tokens.get('label_2', ''))  # Searching for a feasible sibject
            if first_tokens.get('label_2', '').lower().startswith('v') or search_subj_lab[0] is not None:
                term = term + first_tokens.get('token_2', '').capitalize()
                terms_join = " ".join([terms_join, first_tokens.get('token_2', '').capitalize()])
                case = case + "_verb"
                qualification = "operational"
                
                # Calls upon
                if first_tokens.get('token_3', '').lower() in list_of_prep:
                    term = term + first_tokens.get('token_3', '').capitalize()
                    terms_join = " ".join([terms_join, first_tokens.get('token_3', '').capitalize()])
                    case = case + "_prep" 
                # appreciation
                elif first_tokens.get('token_4', '').lower() in list_of_nouns:
                    term = term + first_tokens.get('token_3', '').capitalize() + first_tokens.get('token_4', '').capitalize()
                    terms_join = " ".join([terms_join, first_tokens.get('token_3', '').capitalize(), first_tokens.get('token_4', '').capitalize()])
                    case = case + "_noun" 

            # if it is followed by an importat preposition
            elif first_tokens.get('token_2', '').lower() in list_of_prep or first_tokens.get('token_2', '').lower() in list_of_prep_for_adj:
                term = term + first_tokens.get('token_2', '').capitalize()
                terms_join = " ".join([terms_join, first_tokens.get('token_2', '').capitalize()])
                case = case + "_prep"

            elif first_tokens.get('token_2', '').lower().startswith("rb") or first_tokens.get('token_2', '').lower() == 'also':
                term = term + first_tokens.get('token_2', '').capitalize()
                terms_join = " ".join([terms_join, first_tokens.get('token_2', '').capitalize()])
                case = case + "_also"


        ###|### 1st token_1 is a past participle
        elif label_1 == "VBN" or label_1 == "VBD":
            qualification = "preambular"
            case = "__VBNVBD"

        ###|### 1st token_1 is a coniugation (e.g. "and")
        elif label_1 == "CC":
            case = "__CC"

            # Trying removing this first token_1
            # print(p)
            # --#print("Recalculating (excluding initial token_1 '",token_1,"' whose label_1 is '",label_1,"')\n")
            new_sent = re.sub(r'^' + token_1, '', p)
            try:
                recursion = self.qualify(new_sent)  # Recursively re-analyze
                qualification = recursion[0]
                term = recursion[1]
                terms_join = recursion[2]
            except:
                term = None

        ###|### 1st token_1 is IN (Preposition or Subordinated Conjunction) e.g. "as"
        elif label_1 == "IN" or token_1 in introductory_words:
            case = "__IN"

            # --#print("\nRecalculation B")
            comma_splitted_sentence = self.deleting_until_the_comma(p)       # going to the ", verb" or ", pronoun" part
            if comma_splitted_sentence != '':

                try:
                    the_significative_slice = comma_splitted_sentence[0].strip()
                    the_offset_to_add = comma_splitted_sentence[1]          # getting the offset
                    recursion = self.qualify(str(the_significative_slice))  # Recursively re-analyze
                    qualification = recursion[0]
                    term = recursion[1]
                    terms_join = str(the_significative_slice)[recursion[3][0]:recursion[3][1]]
                except:
                    term = None

        ###|### Keeping trace of any other case, let's learn from experience!
        else:
            qualification = None
            term = None
            case = "__OTHER"
            # --#print("no-qualification (6) - ",case)
            term = None
            # --#print("\tThe first token_1 is: ",token_1," and its label_1 is ",label_1)
            # --#print(p)

        # NOTE: all the above catched portions can be followed by an epxression "with" + JJ/VBN/VBD + NN/NNS ------- e.g with deep concern(s)
        ########
        ########
        ######## Catching with+(adj)+noun
        ########
        ########

        if qualification is not None:


            # Cecking in positions 1-2
            if len(first_tokens) == 2 * TOKEN_SLOT_SIZE:
                
                # concern
                if first_tokens.get('token_2','').lower() in list_of_nouns:
                    print(" + JJ/VBN/VBD + NN/NNS_0")
                    term = term + first_tokens.get('token_2', '').capitalize()
                    terms_join = " ".join(
                        [terms_join, first_tokens.get('token_2', '').capitalize()])
                    case = case + "_nns"
            
            # Cecking in positions 2-3
            if len(first_tokens) == 3 * TOKEN_SLOT_SIZE:
    
                # with concern
                if first_tokens.get('token_2', '').lower() == 'with' and first_tokens.get('token_3',
                                                                                          '').lower() in list_of_nouns:
                    # --#print("__with + JJ/VBN/VBD + NN/NNS")
                    term = term + first_tokens.get('token_2', '').capitalize() + first_tokens.get('token_3',
                                                                                                  '').capitalize()
                    terms_join = " ".join(
                        [terms_join, first_tokens.get('token_2', '').capitalize(),
                         first_tokens.get('token_3', '').capitalize()])
                    case = case + "_withNNs"

                # deep concern
                if (first_tokens.get('label_2', '').lower() in ['jj', 'vbn', 'vbd'] or first_tokens.get("token_2", "").lower() in list_of_adjectives) and first_tokens.get('token_3',
                                                                                   '').lower() in list_of_nouns:
                    # --#print("__with + JJ/VBN/VBD + NN/NNS")
                    term = term + first_tokens.get('token_2', '').capitalize() + first_tokens.get('token_3',
                                                                                                  '').capitalize()
                    terms_join = " ".join(
                        [terms_join, first_tokens.get('token_2', '').capitalize(),
                         first_tokens.get('token_3', '').capitalize()])
                    case = case + "_JJVBNVBDnns"

            # Cecking in positions 2-3-4
            if len(first_tokens) == 4 * TOKEN_SLOT_SIZE:
    
                # with deep concern
                if first_tokens.get('token_2', '').lower() == 'with' and (
                        first_tokens.get('label_3', '').lower() in ['jj', 'vbn', 'vbd'] or first_tokens.get("token_3", "").lower() in list_of_adjectives) and first_tokens.get('token_4',
                                                                                   '').lower() in list_of_nouns:
                    # --#print("__with + JJ/VBN/VBD + NN/NNS")
                    term = term + first_tokens.get('token_2', '').capitalize() + first_tokens.get('token_3',
                                                                                                  '').capitalize() + \
                           first_tokens.get('token_4', '').capitalize()
                    terms_join = " ".join(
                        [terms_join, first_tokens.get('token_2', '').capitalize(),
                         first_tokens.get('token_3', '').capitalize(),
                         first_tokens.get('token_4', '').capitalize()])
                    case = case + "_withJJVBNVBDnns"

                # with concern
                if first_tokens.get('token_3', '').lower() == 'with' and first_tokens.get('token_4',
                                                                                          '').lower() in list_of_nouns:
                    # --#print("__with + JJ/VBN/VBD + NN/NNS")
                    term = term + first_tokens.get('token_3', '').capitalize() + first_tokens.get('token_4',
                                                                                                  '').capitalize()
                    terms_join = " ".join(
                        [terms_join, first_tokens.get('token_3', '').capitalize(),
                         first_tokens.get('token_4', '').capitalize()])
                    case = case + "_withNNs"

                # deep concern
                if (first_tokens.get('label_3', '').lower() in ['jj', 'vbn', 'vbd'] or first_tokens.get("token_3", "").lower() in list_of_adjectives) and first_tokens.get('token_4',
                                                                                   '').lower() in list_of_nouns:
                    # --#print("__with + JJ/VBN/VBD + NN/NNS")
                    term = term + first_tokens.get('token_3', '').capitalize() + first_tokens.get('token_4',
                                                                                                  '').capitalize()
                    terms_join = " ".join(
                        [terms_join, first_tokens.get('token_3', '').capitalize(),
                         first_tokens.get('token_4', '').capitalize()])
                    case = case + "_JJVBNVBDnns"

            # Cecking in positions 3-4-5
            if len(first_tokens) == 5 * TOKEN_SLOT_SIZE:
    
                # with deep concern
                if first_tokens.get('token_3', '').lower() == 'with' and (
                        first_tokens.get('label_4', '').lower() in ['jj', 'vbn', 'vbd'] or first_tokens.get("token_4", "").lower() in list_of_adjectives) and first_tokens.get('token_5',
                                                                                   '').lower() in list_of_nouns:
                    # --#print("__with + JJ/VBN/VBD + NN/NNS")
                    term = term + first_tokens.get('token_3', '').capitalize() + first_tokens.get('token_4',
                                                                                                  '').capitalize() + \
                           first_tokens.get('token_5', '').capitalize()
                    terms_join = " ".join(
                        [terms_join, first_tokens.get('token_3', '').capitalize(),
                         first_tokens.get('token_4', '').capitalize(),
                         first_tokens.get('token_5', '').capitalize()])
                    case = case + "_withJJVBNVBDnns"

                # with concern
                if first_tokens.get('token_4', '').lower() == 'with' and first_tokens.get('token_5',
                                                                                          '').lower() in list_of_nouns:
                    # --#print("__with + JJ/VBN/VBD + NN/NNS")
                    term = term + first_tokens.get('token_4', '').capitalize() + first_tokens.get('token_5',
                                                                                                  '').capitalize()
                    terms_join = " ".join(
                        [terms_join, first_tokens.get('token_4', '').capitalize(),
                         first_tokens.get('token_5', '').capitalize()])
                    case = case + "_withNNs"

                # deep concern
                if (first_tokens.get('label_4', '').lower() in ['jj', 'vbn', 'vbd'] or first_tokens.get("token_4", "").lower() in list_of_adjectives) and first_tokens.get('token_5',
                                                                                   '').lower() in list_of_nouns:
                    # --#print("__with + JJ/VBN/VBD + NN/NNS")
                    term = term + first_tokens.get('token_4', '').capitalize() + first_tokens.get('token_5',
                                                                                                  '').capitalize()
                    terms_join = " ".join(
                        [terms_join, first_tokens.get('token_4', '').capitalize(),
                         first_tokens.get('token_5', '').capitalize()])
                    case = case + "_JJVBNVBDnns"

            # Cecking in positions 4-5-6 [rare]
            if len(first_tokens) == 6 * TOKEN_SLOT_SIZE:
    
                # with deep concern
                if first_tokens.get('token_4', '').lower() == 'with' and (
                        first_tokens.get('label_5', '').lower() in ['jj', 'vbn', 'vbd'] or first_tokens.get("token_5", "").lower() in list_of_adjectives) and first_tokens.get('token_6',
                                                                                   '').lower() in list_of_nouns:
                    # --#print("__with + JJ/VBN/VBD + NN/NNS")
                    term = term + first_tokens.get('token_4', '').capitalize() + first_tokens.get('token_5',
                                                                                                  '').capitalize() + \
                           first_tokens.get('token_6', '').capitalize()
                    terms_join = " ".join(
                        [terms_join, first_tokens.get('token_4', '').capitalize(),
                         first_tokens.get('token_5', '').capitalize(),
                         first_tokens.get('token_6', '').capitalize()])
                    case = case + "_withJJVBNVBDnns"

                # with concern
                if first_tokens.get('token_5', '').lower() == 'with' and first_tokens.get('token_6',
                                                                                          '').lower() in list_of_nouns:
                    # --#print("__with + JJ/VBN/VBD + NN/NNS")
                    term = term + first_tokens.get('token_5', '').capitalize() + first_tokens.get('token_6',
                                                                                                  '').capitalize()
                    terms_join = " ".join(
                        [terms_join, first_tokens.get('token_5', '').capitalize(),
                         first_tokens.get('token_6', '').capitalize()])
                    case = case + "_withNNs"

                # deep concern
                if (first_tokens.get('label_5', '').lower() in ['jj', 'vbn', 'vbd'] or first_tokens.get("token_5", "").lower() in list_of_adjectives) and first_tokens.get('token_6',
                                                                                   '').lower() in list_of_nouns:
                    # --#print("__with + JJ/VBN/VBD + NN/NNS")
                    term = term + first_tokens.get('token_5', '').capitalize() + first_tokens.get('token_6',
                                                                                                  '').capitalize()
                    terms_join = " ".join(
                        [terms_join, first_tokens.get('token_5', '').capitalize(),
                         first_tokens.get('token_6', '').capitalize()])
                    case = case + "_JJVBNVBDnns"

            # Cecking in positions 5-6-7 [rare]
            if len(first_tokens) == 7 * TOKEN_SLOT_SIZE:

                # with deep concern
                if first_tokens.get('token_5', '').lower() == 'with' and (
                        first_tokens.get('label_6', '').lower() in ['jj', 'vbn', 'vbd'] or first_tokens.get("token_6", "").lower() in list_of_adjectives) and first_tokens.get('token_7',
                                                                                   '').lower() in list_of_nouns:
                    # --#print("__with + JJ/VBN/VBD + NN/NNS")
                    term = term + first_tokens.get('token_5', '').capitalize() + first_tokens.get('token_6',
                                                                                                  '').capitalize() + \
                           first_tokens.get('token_7', '').capitalize()
                    terms_join = " ".join(
                        [terms_join, first_tokens.get('token_5', '').capitalize(),
                         first_tokens.get('token_6', '').capitalize(),
                         first_tokens.get('token_7', '').capitalize()])
                    case = case + "_withJJVBNVBDnns"

                # with concern
                if first_tokens.get('token_6', '').lower() == 'with' and first_tokens.get('token_7',
                                                                                          '').lower() in list_of_nouns:
                    # --#print("__with + JJ/VBN/VBD + NN/NNS")
                    term = term + first_tokens.get('token_6', '').capitalize() + first_tokens.get('token_7',
                                                                                                  '').capitalize()
                    terms_join = " ".join(
                        [terms_join, first_tokens.get('token_6', '').capitalize(),
                         first_tokens.get('token_7', '').capitalize()])
                    case = case + "_withNNs"

                # deep concern
                if (first_tokens.get('label_5', '').lower() in ['jj', 'vbn', 'vbd'] or first_tokens.get("token_6", "").lower() in list_of_adjectives) and first_tokens.get('token_7',
                                                                                   '').lower() in list_of_nouns:
                    # --#print("__with + JJ/VBN/VBD + NN/NNS")
                    term = term + first_tokens.get('token_6', '').capitalize() + first_tokens.get('token_7',
                                                                                                  '').capitalize()
                    terms_join = " ".join(
                        [terms_join, first_tokens.get('token_6', '').capitalize(),
                         first_tokens.get('token_7', '').capitalize()])
                    case = case + "_JJVBNVBDnns"

            # --#print(qualification, case)
            # --#print(term)
            # print(terms_join)  # the splitted term

            # serarching the joined terms within the whole paragraph, to get the offsets of achieved final term
            if re.search(terms_join.lower(), p.lower()) is not None:
                # getting start and end of the analized tokens and adding any gathered offsets
                this_start = re.search(terms_join.lower(), p.lower()).span()[0]
                this_end = re.search(terms_join.lower(), p.lower()).span()[1]
                offsets = (this_start+start_plus, this_end+end_plus)
                # --#print("start:", this_start+start_plus, "end:", this_end+end_plus)
                return [qualification, term, terms_join, offsets]
            else:
                # --#print('___offset not matched!')
                return [qualification, None, None, (0, 0)]

        else:
            return [qualification, None, None, (0, 0)]

    def qualify_paragraph(self, p):
        """
        return a list of dictionaries (in case we want to add more outputs)
        
        e.g.
    
        list[
    
            {
              
              'preambular'      : (5,12)
              'term'              : 'callsUpon'
              ...                : ...
            
            }
        
        ]
    
        """

        # =================
        #   .The output data
        # =================
        list_to_return = list()

        # =================
        #   .Normalizing the text
        # =================
        p = self.normalize(p)

        # =================
        #   .Qualifying
        # =================     
        qual = self.qualify(p)
        qualification, term, term_slice, offsets = qual

        if term is not None:
            if "".join(p[offsets[0]:offsets[1]].split()).lower() != term.lower():
                term = None

        res = (qualification, offsets, term)
        list_to_return.append(res)

        return list_to_return
