from datetime import datetime

from keld.commons.helpers import normalize_name

months = [
    "jan",
    "feb",
    "mar",
    "apr",
    "may",
    "jun",
    "jul",
    "aug",
    "sep",
    "oct",
    "nov",
    "dec"
]

month_max = [00, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]


def _normalize_month(mname):
    return months.index(mname.lower()[:3]) + 1


TAG_MAPPING = {
    "authority": "organization"
}


def resolve_date(match):

    if match.group("date_range"):
        return resolve_date_range(match)

    if match.group("month"):
        month = int(match.group("month").value)
    else:
        month = _normalize_month(match.group("month_name").value)

    iso = datetime(
        int(match.group("year").value),
        month,
        int(match.group("day").value),
    ).date().isoformat()

    return (
        "date",
        (match.start, match.end),
        {"date": iso}
    )


def resolve_date_range(match):
    references = []
    start_group = match.group("date_range_start_date")
    start_s, start_e = start_group.start - match.start, start_group.end - match.start
    end_group = match.group("date_range_end_date")
    end_s, end_e = end_group.start - match.start, end_group.end - match.start

    end_year = end_group.group("year").value
    end_date_group = end_group.group("date")
    start_tag = end_tag = "date"
    implicit_start_month = None
    if end_date_group:
        end_tag, end_offset, end_attr = resolve_date(end_date_group)
        implicit_start_month = end_attr["date"].split("-")[1]
    else:
        end_month_group = end_group.group("month")
        if end_month_group:
            end_month = int(end_month_group.value)
            implicit_start_month = end_month
        else:
            end_month_name_group = end_group.group("month_name")
            if end_month_name_group:
                end_month = _normalize_month(end_month_name_group.value)
                implicit_start_month = end_month
            else:
                end_month = 12
        end_day = month_max[end_month]
        pretty_end_date = f"{end_year}-" + f"{end_month}".rjust(2, "0") + "-" + f"{end_day}".rjust(2, "0")
        end_attr = {"date": pretty_end_date}

    start_date_group = start_group.group("date")
    start_year = end_year
    if start_date_group:
        start_tag, start_offset, start_attr = resolve_date(start_date_group)
    else:
        start_day_group = start_group.group("day")
        if start_day_group:
            start_day = start_day_group.value
        else:
            start_day = 1
        start_month_group = start_group.group("month")
        if start_month_group:
            start_month = start_month_group.value
        else:
            start_month_name_group = start_group.group("month_name")
            if start_month_name_group:
                start_month = _normalize_month(start_month_name_group.value)
            else:
                if start_day_group:
                    start_month = implicit_start_month or 1
                else:
                    start_month = 1
        start_year_group = start_group.group("year")
        if start_year_group:
            start_year = start_year_group.value
        pretty_start_date = f"{start_year}-" + f"{start_month}".rjust(2, "0") + "-" + f"{start_day}".rjust(2, "0")
        start_attr = {"date": pretty_start_date}

    start_attr["refersTo"] = "#startDate"
    end_attr["$reference"] = {
        "tag": "TLCConcept",
        "eId": "startDate",
        "href": f"/akn/ontology/concepts/un/startDate",
        "showAs": "Start Date"
    }
    end_attr["refersTo"] = "#endDate"
    end_attr["$reference"] = {
        "tag": "TLCConcept",
        "eId": "endDate",
        "href": f"/akn/ontology/concepts/un/endDate",
        "showAs": "End Date"
    }
    references.append((start_tag, (start_s, start_e), start_attr))
    references.append((end_tag, (end_s, end_e), end_attr))
    return (
        "event",
        (match.start, match.end),
        {
            "refersTo": "",
            "$subreferences": references
        }
    )


def resolve_location(match):
    if match.group("plenary_meeting"):
        eid = f"plenaryMeeting{match.group('pm_number').value}"
        return (
            "location",
            (match.start, match.end),
            {
                "refersTo": f"#{eid}",
                "$reference": {
                    "tag": "TLCLocation",
                    "eId": eid,
                    "href": f"/akn/ontology/locations/un/{eid}",
                    "showAs": match.value
                }
            }
        )
    eid = normalize_name(match.value)
    return (
        "location",
        (match.start, match.end),
        {
            "refersTo": f"#{eid}",
            "$reference": {
                "tag": "TLCLocation",
                "eId": eid,
                "href": f"/akn/ontology/locations/un/{eid}",
                "showAs": match.value
            }
        }
    )


def resolve_authority(match):
    eid = normalize_name(match.value)
    attributes = {
        "refersTo": f"#{eid}",
        "$reference": {
            "tag": "TLCOrganization",
            "eId": eid,
            "href": f"/akn/ontology/organizations/un/{eid}",
            "showAs": match.value
        }
    }
    return (
        "organization",
        (match.start, match.end),
        attributes
    )


def resolve_docstatus(match):
    return (
        "docStatus",
        (match.start, match.end),
        {
            "refersTo": "#general",
            "$reference": {
                "tag": "TLCConcept",
                "eId": "general",
                "href": "/akn/ontology/concepts/un/general",
                "showAs": "general"
            }
        }
    )


def resolve_reference(match):
    return (
        "ref",
        (match.start, match.end),
        {"href": ""}
    )


def resolve_plain(match):
    return (
        match.type,
        (match.start, match.end),
        {}
    )


RESOLVERS_MAP = {
    "date": resolve_date,
    "location": resolve_location,
    "authority": resolve_authority,
    "reference": resolve_reference,
    "docstatus": resolve_docstatus
}


def resolve(match):
    resolve_func = RESOLVERS_MAP.get(match.type, resolve_plain)
    return resolve_func(match)
