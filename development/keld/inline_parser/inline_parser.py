import os

from replus import Engine

from .resolvers import resolve

here = os.path.dirname(os.path.abspath(__file__))

models = os.path.join(here, "models")

engine = Engine(models, *["i"])


def parse_inlines(text, *filters, exclude=[]):
    results = []
    for m in engine.parse(text, *filters, exclude=exclude):
        results.append(resolve(m))
    return results
