
![alt text](unibo.png)

# Title: SANKOFA – Semantic Annotation of Knowledge Fostering Akoma Ntoso
Monica Palmirani, Fabio Vitali, Aldo Gangemi, Silvio Peroni, Andrea Nuzzolese, Francesco Draicchio, Octavian Bojour, Biagio Distefano, Davide Liga, Francesco Sovrano, Luca Cervone.


![alt text](sankofa.png)
«Sankofa is a word in the Twi language of Ghana that translates to "Go back and get it" (san - to return; ko - to go; fa - to fetch, to seek and take) and also refers to the Asante Adinkra symbol represented either with a stylized heart shape or by a bird with its head turned backwards while its feet face forward carrying a precious egg in its mouth. Sankofa is often associated with the proverb, “Se wo were fi na wosankofa a yenkyi," which translates as: "It is not wrong to go back for that which you have forgotten”.» [Link](https://en.wikipedia.org/wiki/Sankofa)

Title: SANKOFA – Semantic Annotation of Knowledge Fostering Akoma Ntoso
1. [ Description and Objective. ](#one)
2. [ Goals and Tasks. ](#two)
3. [ Methodology of Knowledge Extraction. ](#three)
	3.1 [ Related work ](#three-1)
		3.1.1 [ Use the Word styles ](#three-1-1)
		3.1.2 [ Machine Learning ](#three-1-2)
		3.1.3 [ Frames ](#three-1-3)
		3.1.4 [ Linked open data ](#three-1-4)
	3.2 [ Our Solutions ](#three-2)
		3.2.1 [ NER ](#three-2-1)
		3.2.2 [ Classifier SDG ](#three-2-2)
		3.2.3 [ Qualifier of Preambular and Operationl ](#three-2-3)
4.	[ Technologies Details ](#four)
	4.1	[NER](#four-1)
		4.1.1 [Query phases](#four-1-1)
		4.1.2 [Conclusion and Result](#four-1-2)
	4.2	[Classifier SDG](#four-2)
		4.2.1 [Background Information](#four-2-1)
		4.2.2 [BoW-TFIDF](#four-2-2)
		4.2.3 [Averaged Word Embedding and Doc2Vec](#four-2-3)
		4.2.4 [Our Solution for SDG Classification](#four-2-4)
		4.2.5 [Conclusion and Results](#four-2-5)
	4.3	[Qualifier of Preambular and Operationl](#four-3)
		4.3.1 [A final pattern](#four-3-1)
		4.3.2 [Resolution of Mistakes](#four-3-2)
		4.3.3 [Dynamic Pattern Research](#four-3-3)
		4.2.4 [Conclusion and Results](#four-3-4)
5.	[Akoma Ntoso Marker-Converter](#five)
	5.1	[Process of Conversion](#five-1)
	5.2	[Conclusion and Results](#five-2)
6.	[RDF Generation](#six)
7.	[Milestones](#seven)
8.	[Installation](#eight)
	8.1	[Usage](#eight-1)
	8.1	[troubleshooting](#eight-2)
9.	[References](#nine)
10.	[Resources](#ten)


<a name="one"></a>
## 1. Description and Objective

The SANKOFA project intends to produce a web applications (API) that is capable to mark-up the UN resolutions using Akoma Ntoso XML vocabulary, to qualify the paragraphs according to the role played into the structure (e.g., preambular or operative) and finally to classify the semantic parts of the provision (e.g. references). We intend also to qualify some peculiar textual parts (e.g. person, organization, date, quantity, action, event). We will use a hybrid solution using NLP techniques for detecting structure, references, presentational parts, annexes, inline elements (e.g. symbol, committee, session), etc. We use parsers, vocabularies, NER, regular expressions, patterns, frames, deep learning for detecting the knowledge in the text, and to represent it with semantic annotation. After the qualifications of the textual structures and the semantics of relevant segments, we intend to associate the correct conceptual classes from the given ontologies (e.g., UNDO, SDGIO, UNBIS) to the appropriate text segments, and to create assertions and relationships (e.g. which decision was taken, the timing of the decision, which actors were involved, which results are expected). FRBR and ALLOT Top Level Class are pillars in this solution. Those assertions will be also serialized in RDF following the Linked Open Data principles.


<a name="two"></a>
## 2. Goals and Tasks

To achieve the above mentioned objective the project is divided in tasks:

1.	**Detect NER** like role, event, organization, location, date.
2.	**Classify the sentences** that talk about **SDG** using the definitions of the SDGIO.
3.	**Qualify the sentences** in operational and **preambulary** using linguistic patterns and also to detect the action proposed (e.g., noting, decides).
4.	**Recognise the document structure** in the main parts: coverPage, preface, preamble, mainBody, conclusions, annexes. We use also the information detected in the first three steps in order to distinguish the hierarchical structure of the document and to qualify the preambular sentencens from the operational ones.
5.	**Convert** all the extracted knowledge in **Akoma Ntoso**.
6.	**Interpret** the extracted knowledge and to create semantic assertion using to the existing ontologies (e.g., ALLOT, UNDO, SDGIO, etc.). The idea is to create a RDF repository with those assertions.

Some principles lead our solution:

1.	To produce an **Akoma Ntoso technically** valid but also **semantically sound** following the OASIS LegalDocML specifications level 2, sublevel D \[see para 7 [here](http://docs.oasis-open.org/legaldocml/akn-core/v1.0/os/part1-vocabulary/akn-core-v1.0-os-part1-vocabulary.html#_Toc523925096)\];
2.	To use **authentic** sources and **authoritative** information, using **FRBR** approach for declaring the provenance;
3.	To design the solution following ontology design patterns principles [xx];
4.	To provide a **scalable** method that is customizable also for **other UN agencies**;
5.	To design a **modularized** solution that is adaptable to **other kind of documents** (e.g., report of conference, order of the day, constitution, basic texts, etc.);
6.	To apply the same tools, with a minimal customization, for the other **five languages** of UN using the principle of **portability** and **customization**.

The RESTFul Service for testing all the pipeline is available [here](http://bach.cirsfid.unibo.it/unresolution2akn/)

The repo with all the documentation is available [here](https://gitlab.com/CIRSFID/un-challange-2019)

The license for all the material published in the light of this UN Challenge is: Creative Commons Attribution 4.0 International


<a name="three"></a>
## 3. Methodology of Knowledge Extraction

<a name="three-1"></a>
### 3.1	Related work
There are several methodologies for coping with the above mentioned tasks especially starting from Word files that includes some styling information. 

<a name="three-1-1"></a>
#### 3.1.1	Use the Word styles
One easy method is to use the **word docx information** and the **styles** (e.g., italic, notes, font, size) for detecting the semantic part of the text (e.g., italic could be action, font size. This method apparently produces a very high percentage of success, but it is prone to several side effects: i) it depends to a specific style rules applied to a specific organization/agency of UN, ii) those rules changed over the time and this affects the accuracy of this method; iii) the style rules can change language by language (e.g., Chinese, Arabic, Russian); iv) some mistakes in editing phase are possible; v) **scalability**, **modularization** and **portability** are not guaranteed. For these reasons, we don’t use this method in our solution.

<a name="three-1-2"></a>
#### 3.1.2	Machine Learning 
For applying this method we need a corpus marked up before as gold standard and to have a strong training phase, followed to an evaluation phase made by real expert of domain. This approach need a long term project for producing a robust corpus, training set, super-versioned evaluation. For those reasons we think that this methodology could be used properly on the UN resolution document collections only in a second phase, when a relevant XML AKN corpus is available.  Machine learning in legal domain suffers to three main problems nowadays: i) the machine learning is limited to a fragment of text (e.g., sentence) but in legal domain it is super important the context and the relationships between the sentences; ii) the sentences includes normative citations and references and machine learning usually neglects this aspect that is very significant; iii) legal domain is dynamic over the time and the rules detected in such period of time is not valid in another time. So the regularity detected by the machine learning is based on historical series that change over time. Finally it is not **language independent**. We need a training set for each language of the six managed by UN.


<a name="three-1-3"></a>
#### 3.1.3	Frames
To use frames like FrameNet or FRED [xx] is a good solution, but it needs a modelization of the most important situations in the UN resolution domain in order to avoid excessive fragmentation and to reduce the dimension of the tree. To model situations takes time and effort, we need to involve expert of domain and organize feedback workflow. Finally the situations should be modelled for each language involved in UN and some tool available in this sector are very effective only in English.

<a name="three1-4"></a>
#### 3.1.4	Linked open data
Some techniques use linked open data tools for detecting information in the text and to merge them with Wikipedia information or using Linked Open Cloud ontologies. This method is not accurate and could produce invalid information (not legal valid), not authoritative because they are not checked and validated. Secondly the legal domain change over the time. So we need an ontological level capable to manage the modification over the time like for instance authority competences One of the most important principle of our solution is to track the provenance of the information using FRBR approach.  

<a name="three-2"></a>
### 3.2	Our Solutions

We prefer to use the following technologies for making the results authoritative (not mixed with external not checked sources), legal valid, accurate, scalable, modular, portable, language independent.

<a name="three-2-1"></a>
#### 3.2.1	NER
It is method for detecting inside of the text little and fine information about role, person, organization, date, location, etc. We have used this approach for detecting legal knowledge information inside of the sentences.

We have to recognize the following named entities inside United Nations (UN) documents:

1.	Roles (e.g., Secretary-General);
2.	Organizations (e.g., United-Nations);
3.	Deadlines (e.g., by 2030);
4.	Persons (e.g., Ban Ki-moon);
5.	Geo-political entities (e.g., countries like Nigeria);
6.	Places (e.g., Vienna).

<a name="three-2-2"></a>
#### 3.2.2	Classifier SDG
We have to identify whether a paragraph of a United Nations (UN) document is related to one or more Sustainable Development Goals (SDG). Furthermore, every SDG may have different targets (sub-SDGs) that may change in time (some of the sub-SDGs have a short- or mid-term  deadline: 2020, 2030).
More in details, we need a Natural Language technique that should respect at least the following requirements:

1.	The algorithm should be able to measure how similar is a given paragraph to a SDG and a sub-SDG.
2.	The algorithm should allow us to easily change the SDG (and sub-SDG) definitions, without incurring in significantly slow and error-prone pre-processing processes (eg. a slow model training process).

**Solution:** If we represent a SDG by a document describing it, then the 1st requirement can be met using geometrical encodings (embeddings) of words/documents. In other words, if we are able to associate a numerical vector to every document, then we can easily compute the similarity of two vectors/documents (eg. through cosine similarity).

Many models exist for document embedding, and probably some of the most famous are:

- BoW TF-IDF
- Averaged Word2Vec/GloVe/fastText
- Doc2Vec
- Etc…


The first model (BoW-TFIDF) is probably the fastest to build/train, especially because it does not require huge amount of data for training and it does not require hyper-parameters tuning. While the other models are much slower to train (because they are usually modeled with Artificial Neural Networks) and they perform better when trained with huge datasets, and they usually depend on a lot of hyper-parameters. Fortunately many pre-trained Word2Vec/GloVe/fastText models, trained on very big and generic datasets, are easily available on the web. But, these pre-trained models are not optimized for most of the domain-specific tasks.

In order to perform SDG classification, we have to build a model for every SDG and sub-SDG. We can easily build the aforementioned models by using the SDG and sub-SDG descriptions publicly available at [3]. But this descriptions are not enough for training a Neural-based model from scratch.

For this reason we designed a new ensemble method that effectively combines generic (non domain-specific) Averaged GloVe document similarities with domain-specific BoW-TFIDF document similarities.
This way we do not need any complicated and error-prone learning phase for building the document embedding models, thus allowing us to easily tackle also the 2nd requirement, because we can easily change the index corpus.

<a name="three-2-3"></a>
#### 3.2.3	Qualifier of Preambular and Operationl
The Qualifier module has to perform two tasks:

1.	qualifying a given paragraph as “preambular” or “operational”;
2.	identifying the “Terms” that characterize the qualification the so-called action (e.g. “alsoConsidering”).

Within the pipeline, it is used to process paragraphs one by one, giving as a result:

1.	a qualification (“preambular” or “operational”);
2.	a starting and ending offset which indicates where the terms start and end.


**Methods**

A pharagraph is “preambular” when it belongs to a preamble. Usually it is a verb in –ing form, or an adjective. Some examples are:


**PREAMBOLAR EXAMPLES** 	| **TERMS**
--------------------------	| --------------------
Having also considered…		| havingAlsoConsidered
Being concerned at…		 	| beingConcernedAt
Concerned…	 				| concerned


However sometime we have preambular sentence in the body of the resolution like this example:



![alt text](preambular-operational.png)
Figure 1 - A/RES/68/247 B, N1429631.doc, N1429631.xml




A pharagraph is “operational” when it is not part of a preamble. Usually it is a verb at the present tense. Some examples are:


**OPERATIONAL EXAMPLES** 				| **TERMS**
---------------------------------------	| --------------------
Takes note with appreciation…			| takesNoteWithAppreciation
Reaffirm also…		 					| reaffirmAlso
As already stated in[…], encourages…	| encourages


The module uses a tokenizer to create **a POS-Tag sequence of the first tokens**. In the first tokens, in fact we can found all the information we need.

For this reason, we create a list composed of pair of token and labels. For example, considering just the first seven tokens of the sentence “Having considered in depth the question of Western , we will have a tokenization as follow:

TOKEN 			| 	POS-tag
----------------|----------------
“Having” 		|	“VBG”
“considered”	|	“VBN”
“in” 			| 	“IN”
“depth”			| 	“NN”
“the”	 		| 	“DT”
“question” 		| 	“NN”
“of” 			| 	“IN”


The first token already shows that we are dealing with a “preambular” sentence.

The qualifier is also able to deal with more complex structures, where the first tokens are not enough to fulfil a prediction, like those sentences that start with an introductory part:

> “As a contribution to the preparatory process for this Global Compact, we recognize the … ”

In this case, the qualifier will also check the other parts of the sentence, searching for significant patterns of POS-tags, in the attempt to perform a qualification.


<a name="four"></a>
## 4.	Technologies Details

<a name="four-1"></a>
### 4.1	NER

We used the Spacy [1] Named-Entities Recognizer (NER) for recognizing all the entities expect the 1st one (the roles). The Spacy NER is based on Artificial Neural Networks (ANN). Spacy has several pre-trained models for English:

- Small: English multi-task CNN trained on OntoNotes. Assigns context-specific token vectors, POS tags, dependency parse and named entities. (size: 10 MB)
- Medium: English multi-task CNN trained on OntoNotes, with GloVe vectors trained on Common Crawl. Assigns word vectors, context-specific token vectors, POS tags, dependency parse and named entities. (size: 91 MB)
- Large: English multi-task CNN trained on OntoNotes, with GloVe vectors trained on Common Crawl. Assigns word vectors, context-specific token vectors, POS tags, dependency parse and named entities. (size: 788 MB)
We used the Medium model for 2 reasons:
- The Medium model is better in recognizing Organizations (even if it performs worse than the Large one in recognizing Dates).
- The Medium model size is 1/10 of the Large one, and their quality in modeling Natural Language is almost identical.

The Spacy default NER is able to recognize many more entities [2] than those we need, for example it can also recognize:

- Some legislation related entities.
- Any named language.
- Works of Art: Titles of books, songs, etc.
- Events: Named hurricanes, battles, wars, sports events, etc.
- Products: Objects, vehicles, foods, etc. (Not services.)
- Money: Monetary values, including unit.
- etc..

In Spacy:

- Organizations are tagged with ORG or NORP.
- Deadlines are entities tagged with DATE, and preceded by a token ‘by’ or ‘until’.
- Persons are tagged with PERSON.
- Geo-political entities are tagged with GPE.
- Places are tagged with LOC or FACILITY.

We found that the Spacy NER performs poorly in recognizing roles (accuracy close to 0). For this reason, we decided to use the same technique we used for SDG classification with the following changes:

- we used as corpus a list of roles we manually built (around 50 roles), with absolutely no bias documents;
- during corpus and query pre-processing we have not:

	*	removed stop-words or punctuation
	*	performed any string replacement (eg. “Sustainable Development Goal” with “SDG”)
	*	applied any stemming algorithm;
- we changed the query pre-processing phase;
- we changed the query similarity and classification phases.

<a name="four-1-1"></a>
#### 4.1.1	Query phases
The query phases are three:

1.	pre-processing: in this phase we build a list of composite tokens
2.	similarity phase: in this phase we use the composite tokens list to build a sub-query list, and then we compute the similarity (to the corpus documents) of those sub-queries, by using the same technique used for SDG classification during its query similarity phase.
3.	classification phase: in this phase we properly group together the composite tokens and then we decide whether that group can be classified as a group of words representing a Role Entity.

During the query pre-processing (but not during the document pre-processing), we add a composite-tokenization phase before the lower-case substitution phase. 
A composite token is a list of zero, one or more words. In a composite token there are more than one word only when those words are consecutive words starting by an upper-case character and separated by a whitespace or a ‘/‘ or a ‘-‘.
A composite token with zero words is said to be an empty composite token.
During the composite-tokenization phase, first of all we tokenize the query and then we build a list of composite tokens. Then we remove from every composite token all the (stop-) words having the following Part-of-Speech tags:

-	Particle
-	Pronoun
-	Determiner 
-	Subordinating Conjunction 
-	Adjective

The resulting list of composite tokens might contain empty composite tokens (having no words inside). Thus we remove all the empty composite tokens and we filter the remaining ones removing from them all those words having the following Part-of-Speech tags:

-	Auxiliary Verb 
-	Punctuation 
-	Apposition 
-	Coordinating Conjunction

Now, we do NOT remove further the empty composite tokens.
This is because, during the query similarity phase we use the composite tokens list to build a sub-query list, and then we compute the similarity of those sub-queries. In other words instead of matching the whole query Q against the corpus, we match every composite token in Q by using the same technique used for SDG classification during its query similarity phase.
Thus we do NOT filter further the empty composite tokens because we want to join together in a similarity group G all the consecutive composite tokens having similarity S higher than a pre-defined threshold P = 0.5 (and an empty composite token always has null similarity).

Now, if a similarity group G has more than one composite token, then we have to identify what is its most similar role in the corpus.
In order to do that, we sum the similarities vectors of the composite tokens in G obtaining in such way the similarity vector S of G, then we can sort these similarities in descending order and get a similarity ranking. Finally, every group G having S > T, where T = 1.25 is the group threshold, can be classified as a group of words representing a Role Entity.

<a name="four-1-2"></a>
#### 4.1.2	Conclusion and Result
The Named-Entities Recognizer we used is the same default NER coming with Spacy. But that NER has not been trained to recognize Role Entities, thus we have modified the algorithm used for SDG classification in order to perform Role Entity Recognition (RER). 
One of the advantages of the new RER algorithm is that:

- it is able to find the most similar role in an ontology
- it does not require much hyper-parameters tuning (only the value of P and T have to be tuned)
- it is super fast to train and it allows us to easily change the classes definitions without incurring in significantly slow and error-prone pre-processing processes
- it performs quite well with relatively small training sets.
But the main disadvantage of the new RER algorithm is that its time complexity depends on the size of the corpus. In other words: the bigger is the roles corpus, the linearly slower is the algorithm.

The results are the following.

The NER extraction of geo-political entity in Spacy:

~~~~
<content>
    <p>Recalling the first to fifteenth meetings of the States parties to the <law>Convention</law>, held in <gpe>Maputo</gpe> (1999), <gpe>Geneva</gpe> (2000), <gpe>Managua</gpe> (2001), <gpe>Geneva</gpe> (2002), <gpe>Bangkok</gpe> (2003), <gpe>Zagreb</gpe> (2005), <gpe>Geneva</gpe> (2006), <loc>the Dead Sea</loc> (2007), <gpe>Geneva</gpe> (2008 and 2010), <gpe>Phnom Penh</gpe> (2011), <gpe>Geneva</gpe> (2012, 2013 and 2015) and <person>Santiago</person> (2016), and the First, Second and Third Review Conferences of the States Parties to the Convention, held in <gpe>Nairobi</gpe> (2004), <gpe>Cartagena</gpe>, <gpe>Colombia</gpe> (2009), and <gpe>Maputo</gpe> (2014),</p>
</content>
~~~~

Akoma Ntoso markup made by Marker-Converter:

~~~~
<blockContainer>
    <crossHeading refersTo="#preamble" eId="container_main_body_pg6"><term refersTo="#recalling">Recalling</term> the first to fifteenth meetings of the States parties to the <ref eId="ref_2" refersTo="#convention" href="">Convention</ref>, held in <location eId="location_1" refersTo="#maputo">Maputo</location> (1999), <location refersTo="#geneva">Geneva</location> (2000), <location eId="location_3" refersTo="#managua">Managua</location> (2001), <location refersTo="#geneva">Geneva</location> (2002), <location refersTo="#bangkok">Bangkok</location> (2003), <location eId="location_6" refersTo="#zagreb">Zagreb</location> (2005), <location refersTo="#geneva">Geneva</location> (2006), <location eId="location_8" 
    refersTo="#theDeadSea">the Dead Sea</location> (2007), <location refersTo="#geneva">Geneva</location> (2008 and 2010), <location eId="location_10" refersTo="#phnomPenh">Phnom Penh</location> (2011), <location refersTo="#geneva">Geneva</location> (2012, 2013 and 2015) and <location refersTo="#santiago">Santiago</location> (2016), and the First, Second and Third Review Conferences of the States Parties to the Convention, held in <location refersTo="#nairobi">Nairobi</location> (2004), <location eId="location_13" refersTo="#cartagena">Cartagena</location>, <location eId="location_14" refersTo="#colombia">Colombia</location> (2009), and <location eId="location_15" refersTo="#maputo">Maputo</location> (2014),</crossHeading>
</blockContainer>
~~~~
N1741876.xml


The NER extraction of normative references in Spacy:
~~~~
<content>
    <p>Stresses the importance of the full and effective implementation of and compliance with the <law>Convention</law>, including through the continued implementation of the action plan for the period 2014-2019;</p>
</content>

~~~~


Akoma Ntoso markup made by Marker-Converter:
~~~~
<content>
    <p><term refersTo="#stresses">Stresses</term> the importance of the full and effective implementation of and compliance with the <ref eId="ref_11" refersTo="#convention" href="">Convention</ref>, including through the continued implementation of the action plan for the period 2014-2019;</p>
</content>
~~~~


The NER extraction of organization, event (intervals) in Spacy:
~~~~
<content>
    <p>Reiterates its invitation and encouragement to all interested <gpe>States</gpe>, <org>the United Nations</org>, other relevant international organizations or institutions, regional organizations, <org>the International Committee of the Red Cross</org> and relevant nongovernmental organizations to attend <event>the Sixteenth Meeting of the States Parties to the Convention</event>, to be held in <gpe>Vienna</gpe> from 18 to 21 December 2017, and to participate in the future programme of meetings of the <gpe>States</gpe> parties to the <law>Convention</law>;</p>
</content>
~~~~


Akoma Ntoso markup made by Marker-Converter:
~~~~
<content>
    <p><term refersTo="#reiterates">Reiterates</term> its invitation and encouragement to all interested < organization eId="organization_8" refersTo="#states">States</ organization >, the <organization refersTo="#un">United Nations</organization>, other relevant international organizations or institutions, regional organizations, <organization eId="organization_9" refersTo="#theInternationalCommitteeOfTheRedCross">the International Committee of the Red Cross</organization> and relevant nongovernmental organizations to attend <event eId="event_1" refersTo="#theSixteenthMeetingOfTheStatesPartiesToTheConvention">the Sixteenth Meeting of the States Parties to the Convention</event>, to be held in <location refersTo="#vienna">Vienna</location> <event refersTo="">from <date date="2017-12-18" refersTo="#startDate">18</date> to <date date="2017-12-21" refersTo="#endDate">21 December 2017</date></event>, and to participate in the future programme of meetings of the <location eId="location_23" refersTo="#states">States</location> parties to the <ref eId="ref_16" refersTo="#convention" href="">Convention</ref>;</p>
</content>
~~~~


The NER extraction of role in Spacy:
~~~~
<content>
    <p>Requests the <role>Secretary-General</role>, in accordance with <law>article 11</law>, paragraph 1, of the <law>Convention</law>, to undertake the preparations necessary to convene the Seventeenth Meeting of the States Parties to the Convention and, on behalf of the <gpe>States</gpe> parties and in accordance with <law>article 11</law>, paragraph 4, of the <law>Convention</law>, to invite <gpe>States</gpe> not parties to the <law>Convention</law>, as well as <org>the United Nations</org>, other relevant international organizations or institutions, regional organizations, <org>the International Committee of the Red Cross</org> and relevant non-governmental organizations, to attend <event>the Seventeenth Meeting of the States Parties</event> as observers; </p>
</content>
~~~~


Akoma Ntoso markup made by Marker-Converter:
~~~~
<content>
    <p><term refersTo="#requests">Requests</term> the <role eId="role_2" refersTo="#secretaryGeneral">Secretary-General</role>, in accordance with <ref eId="ref_17" refersTo="#article11" href="">article 11, paragraph 1</ref>, of the <ref eId="ref_18" refersTo="#convention" href="">Convention</ref>, to undertake the preparations necessary to convene the Seventeenth Meeting of the States Parties to the Convention and, on behalf of the <organization eId="organization_24" refersTo="#states">States</ organization > parties and in accordance with <ref eId="ref_19" refersTo="#article11" href="">article 11, paragraph 4</ref>, of the <ref eId="ref_20" refersTo="#convention" href="">Convention</ref>, to invite <location eId="location_25" refersTo="#states">States</location> not parties to the <ref eId="ref_21" refersTo="#convention" href="">Convention</ref>, as well as the <organization refersTo="#un">United Nations</organization>, other relevant international organizations or institutions, regional organizations, <organization eId="organization_11" refersTo="#theInternationalCommitteeOfTheRedCross">the International Committee of the Red Cross</organization> and relevant non-governmental organizations, to attend <event eId="event_2" refersTo="#theSeventeenthMeetingOfTheStatesParties">the Seventeenth Meeting of the States Parties</event> as observers; </p>
</content>
~~~~


<a name="four-2"></a>
### 4.2	Classifier SDG

#### 4.2.1 Background Information
We know that several techniques exist for learning geometrical encodings of words from their co-occurrence information (how frequently they appear together in large text corpora). The goal of word embeddings is to capture the meaning of a word using a low-dimensional vector, and these embeddings are ubiquitous in natural language processing because they allow us to perform arithmetical operations on natural language words.

Document embedding is somehow related to word embedding, but it is a different task because the granularity of the embedder input shifts from words to documents. For this reason, document embedding is said to be harder to get than word embedding and in many cases it is built upon word embeddings.

Some famous document embedding techniques are:

- BoW-TFIDF;
- Singular Value Decomposition (used by Latent Semantic Analysis);
- Averaged Word2Vec / GloVe / fastText;
- Doc2Vec.

#### 4.2.2 BoW-TFIDF
Bow-TFIDF stands for: **Bag of Words based Term Frequency - Inverse Document Frequency**. In BoW, documents are described by word occurrences while completely ignoring the relative position information of the words in the document. BoW tokenizes the documents and counts the occurrences of token and return them as a sparse matrix.

The BoW model can reasonably convert raw text to numbers. However, if our purpose is to **identify signature (important) words in a document**, there is a better transformation that we can apply. Here, by “signature words in a document” we mean all those words in the document that are important to resume the meaning of a document.

The TF-IDF is the product of two statistics: term frequency and inverse document frequency. **Term frequency** (TF) is basically the output of the BoW model. For a specific document, it determines how important a word is by looking at how frequently it appears in the document. Term frequency measures the local importance of the word. [2]

The second component of TF-IDF is **Inverse Document Frequency (IDF)**. For a word to be considered a signature word of a document, it shouldn’t appear that often in the other documents. Thus, the frequency among different documents of a signature word must be low, in other words the inverse document frequency must be high. For example the word “and” might reasonably have an high TF for a specific document, but this does not mean that “and” is an important word for that document, in fact probably it has also a high IDF.

The TF-IDF is the product of these two frequencies. For a word to have high TF-IDF in a document, it must appear a lot of times in said document and must be absent in the other documents. It must be a signature word of the document. [2]

The similarity between TF-IDF embeddings is said to be **syntagmatic (topic related)** and it is usually measured through cosine similarity.


#### 4.2.3 Averaged Word Embedding and Doc2Vec
Word2Vec [9], GloVe [5] and fastText [10] are unsupervised learning algorithms for word embedding, based on **artificial neural networks**. Word embedding is a type of mapping that allows words with similar meaning to have similar representation. The basic idea behind word embedding (and distributional semantics) can be summed up in the so-called **distributional hypothesis** [6]: linguistic items with similar distributions have similar meanings; words that are used and occur in the same contexts tend to purport similar meanings.

All the aforementioned word embedding algorithms consist in a shallow or a deep Artificial Neural Network usually trained by mean of Stocastic Gradient Descent, intuitively with the goal of optimally predicting a word given its context or viceversa.

For example, the **GloVe** model is trained on the non-zero entries of a global word-word co-occurrence matrix, which tabulates how frequently words co-occur with one another in a given corpus. GloVe is essentially a log-bilinear model with a weighted least-squares objective. The main intuition underlying the model is the simple observation that ratios of word-word co-occurrence probabilities have the potential for encoding some form of meaning. The training objective of GloVe is to learn word vectors such that their dot product equals the logarithm of the words' probability of co-occurrence. [5]

It is worthy to mention that both Word2Vec and GloVe are not able to handle new words (words not used during the training phase), while fastText can produce **word embedding also for new words**.

A naive approach to build document embeddings through word embedding might be averaging the word embeddings of a document. This naive approach is called **Averaged Word Embedding (AWE)**. One of the disadvantages of this document embedding technique is that it is not sensible to words ordering, for example the sentences “This is a meaningful sentence” and “This a meaningful sentence is” have the same AWE but only the first one is properly formed and it has a meaning. Another disadvantage of ANN-based word embedding algorithms is that they usually require a significantly large amount of training data.

A more sophisticated approach for document embedding might be **Doc2Vec** [8]. Doc2Vec is very powerful when combined with ANNs and it represents the state-of-the-art for document embedding, but, like AWE, it might require a huge amount of data in order to express its real potential. Thus Doc2Vec is probably the best choice if we can dispose of a big-enough dataset of documents regarding a specific linguistic domain, but this is usually unlikely to be when working with **uncommon languages or very specific domains**.

Anyway, an important aspect of these representations is the ability to solve word analogies in the form “A is to B what C is to D”, by using simple arithmetic. For example, in Word2Vec, we might see that the following word embeddings equations are valid [1]:

- “Paris  -  France + Germany = Berlin”
- “King - Man + Woman = Queen”

Thus, the similarity between these embeddings is said to be a paradigmatic and it is usually measured through cosine similarity.


#### 4.2.4 Our Solution for SDG Classification
Assuming that we represent SDGs (and sub-SDGs) by descriptive documents taken from [3], then we want to produce good-enough document embeddings of these SDGs descriptions in order to easily perform document similarity through arithmetic. 
We might produce document embeddings by training from scratch a Doc2Vec algorithm or some other ANN-based algorithm, but our dataset is made of SDG descriptions taken from [3] and it is too small for any ANN-based approach, because of the SDGs are just 17 and even the sub-SDGs are always less than 20. In other words, under these conditions it seems that we cannot train an ad hoc ANN-based model without data augmentation, because we definitely have not enough data.

For this reason we designed a new ensemble method that effectively combines **generic (non domain-specific) Averaged GloVe** document similarities with **domain-specific BoW TF-IDF** document similarities. 

Our solution tries to exploit the best from both the aforementioned techniques:

- The GloVe model is much slower to train and it requires a huge amount of data for proper training, but there exist many models pre-trained on very generic datasets that can be easily exploited. We use a GloVe model pre-trained on data coming from Common Crawl, this data is not domain-specific, thus the resulting word embeddings tend to lose information when used in specific domains such as UN Documents. A solution to the domain-specificity problem can be transfer learning, but even when adopting the classical transfer learning approach we would need a significantly bigger dataset than the one we dispose. Thus, we use AWE based on GloVe to model only generic (non domain-specific) information (eg. semantic relationship among non domain-specific words).
- The BoW-TFIDF model is fast to train and it does not require a huge dataset and hyper-parameters tuning, but it is a shallow learning technique and it lacks of semantic expressivity when compared to techniques such as GloVe. But TFIDF has been specifically designed to extract document signatures, topic information. Thus, we can use BoW-TFIDF to model domain-specific information.

Let A (the query) and B (a corpus document) be two distinct documents, we want to compute the similarity between A and B. 
In order to do that, we combine:

- The cosine similarity of the TFIDF embeddings of A and B: that is a **topical/syntagmatic** similarity extracted by populating the vectors with information on which text regions the linguistic items occur in [4]. On the semantic level Syntagmatic associations indicate compatible combinations of words.

- The cosine similarity of the average of the GloVe word embeddings of A and B: that is a **paradigmatic** similarity extracted by populating the vectors with information on which other linguistic items the items co-occur with \[4\]. On the semantic level Paradigmatic substitutions allow items from a semantic set (synonyms, antonyms, etc..) to be grouped together.

In other words, the idea behind this ensemble is to combine the unique and different properties of the aforementioned similarities, in order to get a new paradigmatic similarity potentially able to express topic similarity on a domain on which the GloVe model has not been trained on.

Our way of combining TFIDF with Word2Vec/GloVe differs from the one adopted for example in \[7\] or in \[8\]. In \[7,8\] the document embedding is obtained by averaging the Word2Vec/GloVe word embeddings weighted by their TFIDF word embedding. But, in our approach we combine document similarities instead of word embeddings. In principle, the technique adopted in [7] might be used to improve the paradigmatic similarity used in our technique.

The **pipeline** of our **algorithm** is as follows:

1.	**Corpus pre-processing**: for building the TF-IDF model we need properly formatted data.
2.	**TF-IDF model building**: only once, before performing any query.
3.	**Query pre-processing**: same as corpus pre-processing.
4.	**Query similarity computation**.
5.	**Query classification**: classify the query according to its similarity.

As backbone Python library we decided to use Spacy [11], for pre-processing and for word embedding. Furthermore we used the Snowball algorithm implementation coming with NLTK [12], for stemming.

The default Spacy Language (Pre-)Processing Pipeline is the following:

- Tokenizer: Segment text into tokens.
- Tagger: Assign part-of-speech tags.
- Parser: Assign dependency labels.
- NER: Detect and label named entities.

During the TF-IDF pre-processing phase (for both corpus documents and queries) we perform the following steps:

1.	Replace upper-cases with lower-cases.
2.	Replace every occurrence of "sustainable development goal” with “sdg”, and set as word embedding of “sdg” the sum of the embeddings of "sustainable”, “development” and “goal”.
3.	Replace every occurrence of: 
	- “sdg 1”, “sdg 2”, …, “sdg 17”;
	- “1st sdg”, “2nd sdg”, …, “17th sdg”
	- “first sdg”, “second sdg”, …, “seventeenth sdg”
with “sdg1”, “sdg2”, …, “sdg17”, and set as their word embeddings the sum of the embedding of “sdg” with the embedding of the respective number.

1.	Perform: tokenization and lemmatization 
2.	Perform **stemming** on lemmas.
3.	Remove stop-words (as defined by Spacy) and punctuation.

We have empirically observed that **stemming helps TF-IDF in achieving greater generalization** and better results in SDG classification.

We decided to consider the words _“Sustainable Development Goal”_ as a unique token and furthermore to give a unique token to every SDG (SDG1 stands for the first SDG, and so on), this is the reason behind the occurrences replacements described at point 2 and 3. We took this decision in order to better classify all those SDGs explicitly mentioned through their unique identifier (e.g. SDG 3 for the third SDG). 
 

The corpus is usually defined by N different documents:

- In the SDG classification problem we have 17 different SDGs, thus we use their definitions as corpus. Furthermore we add to every SDG document the SDG unique identifier in order to be able to classify queries containing these unique IDs. The unique identifier of a SDG is an important marker for SDG classification, but even by applying the aforementioned tokenization tricks the TFIDF model cannot understand it because these unique identifiers are a single unique token (thus with very low term frequency). For this reason, in order to add bias toward these IDs we build 17 new different documents (one per SDG) containing only the SDG ID. These new documents are called  bias documents, while the other ones are called class documents. Thus our corpus for the SDG classification is made of N=34 documents. We will call this technique the Important Keyword Bias trick (IKB trick). If the set of bias documents is not none, then there should be exactly the same number of bias documents for every class document.
- For the sub-SDG classification problem we build the corpus similarly to the SDG classification one, but instead of using the definitions of the SDGs as corpus we use the definitions of the sub-SDGs of a SDG. Thus we have a separate sub-SDG classifier for every SDG.

We have empirically observed that the bigger is the SDG (or sub-SDG) definition, the better is the resulting TFIDF model in computing syntagmatic similarities. 

In other words, if we use as SDG definitions only the SDG titles (a few tokens per SDG), the resulting model in practice performs poorly. But if we use as SDG definitions the whole text used in [3] for describing every SDG (hundreds of tokens per SDG), then the resulting TFIDF model performs much better.

We **build the TF-IDF model only once**, using the corpus and performing the following steps:

1.	Build a fixed Dictionary of all possible words in the corpus.
2.	Using the aforementioned Dictionary, get the Bag-of-Word of every document in the corpus.
3.	Build the TF-IDF model using the aforementioned BoWs.

After we have built the TF-IDF model we can compute a query similarity as follows:

1.	Get the BoW of a query Q using the fixed corpus Dictionary and compute its TF-IDF vector.
2.	Compute the TF-IDF cosine similarity (T) between the query vector and the vector of every document in the original corpus. The result should be a vector T of N real numbers in [0,1], each one representing the similarity of the query and a document in the corpus.
3.	Compute the GloVe AWE cosine similarity (G) between the whole corpus and the query, using the predefined Spacy method for document similarity. The result should be a vector G of N real numbers in [0,1], each one representing the similarity of the query and a document in the corpus.
4.	Compute R: the average of G. R should be a measure of how much Q is relevant to the SDG (or sub-SDG) topic (the topic of Sustainable Development Goals).
5.	Compute C = (T + G)\*R. Where C here is called combined similarity. Furthermore in this formula G is said to be the semantic shift, while R is said to be the paradigmatic topic weight.

The intuitive idea behind using the semantic shift G and the  paradigmatic topic weight R is that the TF-IDF similarity T is high for a query Q and a document D when the query words and the document words are similar, but T is a syntagmatic similarity and thus may be lower (or even 0) when Q contains words in the synsets of D. Thus, in order to address the aforementioned synset-words problem we sum T with a paradigmatic similarity G before scaling it by R. We scale (T + G) by R in order to give significantly more similarity to the queries paradigmatically more inherent to the corpus topics.

Now that we have the combined similarity, we can use it in order to perform SDG (or sub-SDG) classification. A query can be classified as related to a specific SDG or not. Thus, we have to understand when a query is not related to any SDG. In order to do this, we have to choose a similarity threshold T.

Empirically we observe that the bigger is the query, the smaller tends to be the value of C. Thus we hypothesize that T is a function of the size of the query, for this reason in order to perform SDG classification using C we perform the following steps:

1.	We compute W = C \* (1 + L), where L is the base-2 logarithm of the number of tokens in the query. This is called log-length scaling.
2.	We sum the weighted similarity of all the bias documents to the weighted similarity of the corresponding class document, thus obtaining the biased similarity B. In other words, the bias documents add bias to the corresponding class document only.
3.	Let M be the average of B, we compute B = B - M in order to center the biased similarity vector B. Please note that we do NOT normalize B by its standard deviation. The goal of centering B is to give more focus on the variance of the query similarity to the corpus.
4.	We sort the class documents D ordered by descending biased similarity B.
5.	We set T = 0.75, and we get the index of all the class documents V having B > T.
6.	If the set of V is empty, then the query Q is said to be not related to any class (document). Otherwise we have the ranking of the most related classes to Q (one or even more).

The intuitive idea behind the scaling of C by L is that the bigger is the query Q, the (smoothly) lower is C. We sum 1 to L before scaling because otherwise queries having length 1 would have W equal to 0. Queries having length 1 might be reasonable, for example a query containing only the token “SDG1” should be classified as SDG 1.

We set T = 0.75 because we empirically find that 0.75 is a good threshold. But a more robust approach would be to apply an automatic regression technique on a labelled dataset in order to find the optimal value of T.

<a name="four-2-1"></a>
#### 4.2.5	Conclusion and Results

We designed a new ensemble method that effectively combines generic (non domain-specific) Averaged GloVe document similarities with domain-specific TF-IDF document similarities, for achieving SDG classification of UN documents paragraphs. Furthermore we have also shown how to improve this algorithm by using the Universal Sentence Encoder [13]. The algorithm we described is quite versatile and powerful. In fact it is able to perform multi-class classification, it does not require much hyper-parameters tuning (practically only the value of T has to be tuned), it is super fast to train, it allows us to easily change the classes definitions without incurring in significantly slow and error-prone pre-processing processes, and it performs quite well with relatively small training sets. But, it is very important to mention that we do not have properly evaluated our results due to the lack of a gold standard or even a big-enough test-set annotated by domain experts.

We separately annotated two different test-sets (annotated by different people):

- Test-Set A: made of 128 annotated paragraphs (by F. Sovrano)
- Test-Set B: made of 112 annotated paragraphs (by F. Draicchio)

Those 2 sets share around 50 paragraphs.

We performed several experiments in order to understand how good is our classifier in recognizing concepts related to SDG goals (we have not tested SDG targets yet).

We remember that every paragraph can be classified in one or more of 18 classes:

- Class 0: no SDG found
- Class 1-17: one per SDG

The classifier output is a prediction set, every time the classifier predicts more than one class, we check whether there is at least one predicted class that appears also in the annotation set:

- If the intersection set between the annotation and the prediction is empty, then we use as Truth the first class in the annotations ranking and as Guess the first class in the predictions ranking.
- If the intersection set is not empty, then we use as Truth and Guess the first class in the intersection set. Please note that a set in Python is “randomly” ordered.

We use Truth and Guess to compute all the following statistics, by using the functions provided by sklearn.metrics [18].

For the algorithm describe until now, we get the following statistics:

Test-Set A

**statistical measure** 	| **result**
--------------------------	| --------------------
Accuracy 				| 	0.671875
Balanced Accuracy 		|	0.5539898989898989
Matthews Correlation Coefficient (MCC)	|	0.6107615935632802
Precision 				| 	[0.52702703, 1., 0.5, 1., 1., 1., 0., 0.94736842, 0.5, 1., 1., 1., 0.81818182, 1., 1.]
Recall					| 	[0.975, 0.5, 0.2, 0.33333333, 0.66666667, 0.66666667,  0., 0.54545455, 0.27272727, 1., 0.5, 0.5, 0.9, 0.25, 1.]
Support	 				| 	[40, 2, 5, 3, 3, 3, 1, 33, 11, 4, 6, 2, 10, 4, 1]



Test-Set B

**statistical measure** 	| **result**
--------------------------	| --------------------
Accuracy 				| 	0.7767857142857143
Balanced Accuracy 		|	0.5532670454545454
Matthews Correlation Coefficient (MCC)	|	0.7013871705307468
Precision 				| 	[0.75409836, 1., 0.5, 1., 1., 1.,  0., 0.88888889, 0.16666667, 1., 1., 0.,  0.81818182, 0., 1., 1.]
Recall					| 	[0.95833333, 0.5, 0.2, 1., 0.66666667, 0.66666667,  0., 0.72727273, 0.5, 0.8, 0.5, 0., 1., 0., 1., 0.33333333]
Support	 				| 	[48, 2, 5, 1, 3, 3, 1, 22, 2, 5, 4, 2, 9, 1, 1, 3]



If we change the value of the threshold T from 0.75 to 0.6 we get the following statistics:


Test-Set A

**statistical measure** 	| **result**
--------------------------	| --------------------
Accuracy 				| 	0.7109375
Balanced Accuracy 		|	0.547878787878788
Matthews Correlation Coefficient (MCC)	|	0.6505348761221265
Precision 				| 	[0.6031746, 0.5, 0.5, 1., 0., 0.66666667,  1., 0., 0.95238095, 0.54545455, 1., 1., 0., 0.84615385, 1., 1.]
Recall					| 	[0.95, 0.5, 0.2, 0.33333333, 0., 0.66666667,  0.66666667, 0., 0.60606061, 0.54545455, 1., 0.5,  0., 1., 0.25, 1.]
Support	 				| 	[40, 2, 5, 3, 0, 3, 3, 1, 33, 11, 4, 6, 1, 11, 4, 1]



Test-Set B


**statistical measure** 	| **result**
--------------------------	| --------------------
Accuracy 				| 	0.7946428571428571
Balanced Accuracy 		|	0.602938988095238
Matthews Correlation Coefficient (MCC)	|	0.7282123263905372
Precision 				| 	[0.81818182, 1., 0.5, 1., 1., 1.,  0., 0.85, 0.25, 1., 1., 0., 0.8, 0., 1., 1.]
Recall					| 	[0.9375, 0.5, 0.33333333, 1., 0.66666667, 0.66666667,  0., 0.80952381, 1., 0.8, 0.6, 0., 1., 0., 1., 0.33333333]
Support	 				| 	[48, 2, 6, 1, 3, 3, 1, 21, 2, 5, 5, 2, 8, 1, 1, 3]



As we can see, setting the threshold T = 0.6 practically improves all the statistics, MCC included.
The proposed solution for SDG classification has to be considered as a first prototype, we believe we can improve it by properly using pre-trained models of state-of-the-art sentence embedders.

The conversion in Akoma Ntoso is made using \<keyword\> plus a \<proprietary\> block in order to classify the paragraph or the sentence where the classification occurs and also to annotate the grade of confidence:

~~~~
<classification source="#cirsfidUnibo">
    <keyword eId="keyword_1" dictionary="SDGIO" value="goal_11" href="/akn/ontology/concepts/un/sdg_11" showAs="SDG 11" refersTo="#concept_sdg_11"/>
    <keyword eId="keyword_2" dictionary="SDGIO" value="goal_11" href="/akn/ontology/concepts/un/sdg_11" showAs="SDG 11" refersTo="#concept_sdg_11"/>
    <keyword eId="keyword_3" dictionary="SDGIO" value="goal_11" href="/akn/ontology/concepts/un/sdg_11" showAs="SDG 11" refersTo="#concept_sdg_11"/>
    <keyword eId="keyword_3_11.7.2" value="goal_11_11.7.2" href="/akn/ontology/concepts/un/sdg_11_11.7.2" showAs="SDG 11_11.7.2" refersTo="#concept_sdg_11_11.7.2" dictionary="SDGIO"/>
</classification>
~~~~

The keyword is associate to a concept for the ontology mapping.

~~~~
<TLCConcept eId="concept_sdg_11" href="/akn/ontology/concepts/un/sdg_11" showAs="SDG 11"/>
<TLCConcept eId="concept_sdg_11_11.7.2" href="/akn/ontology/concepts/un/sdg_11_11.7.2" showAs="SDG 11_11.7.2"/>
~~~~

In the proprietary block we annotate the source where the classification occurs and the confidence grade.

~~~~
<proprietary source="#cirsfidUnibo">
	<akn4un:source href="#container_preamble_pg9">
	  <akn4un:sdgGoal value="goal_11" confidence="1.219282865524292" name="SDGIO"/>
	</akn4un:source>
	<akn4un:source href="#container_preamble_pg11">
	  <akn4un:sdgGoal value="goal_11" confidence="1.1596317291259766" name="SDGIO"/>
	</akn4un:source>
	<akn4un:source href="#para_4">
	  <akn4un:sdgGoal value="goal_11" confidence="0.9397022724151611" name="SDGIO"/>
	</akn4un:source>
</proprietary>
~~~~
N1740682.xml file


<a name="four-3"></a>
### 4.3	Qualifier of Preambular and Operationl 
Let first_word, second_word. third_word, etc, be the first words of a paragraph, and let first_label, second_label, etc. The qualifier will search for patterns **following strict linguistic patterns** and exploiting **the power of the SpaCy POS-tagger**.
In particular, the algorithm search for patterns in the following straightforward way: it starts checking the paragraph from the first token-tag pair, choosing what to do after, going down little by little towards the following words.
Just in limited cases it searches for specific tokens (in rare verbal phrases such as “bearing in mind” or “pay tribute to”). 
The first token can have different POS-tags. The following table illustrates the process of the analysis which starts from the token-tag pair of the very first word of the paragraph.



POS-tag of the **first word** 	| 	Action for preambolar/operational task 	| Action for term detection
---------------	|----------------|----------------------------|
VBG (ing-form) |Qualification = **Preambolar** <br><br> *e.g. “having also considered”* | Searching for the following patterns: <br> \<VBG\>\<IN\>\* <br> ing (+ preposition) <br><br> \<VBG\>\<VBN\/VBD\>\<IN\>\* <br> ing + past participle/past tense (+ preposition) <br><br> \<VBG\>\<RB\>\<VBN\/VBD\>\<IN\>\* <br> ing + adverb + p.p./p.t. (+ preposition) <br><br> \<VBG\>\<TO\>\<V\*\>\<IN\>\* <br> ing + to + verb (+ preposition) |
PRP (personal pronoun) <br> \[e.g. we\] | Searching for suitable operational patterns: <br> ADVERB + VERB (not ing-form) -> operational <br> e.g. “we therefore consider” | Not considering the  pronoun, searching for the patterns: <br> \<V\*\>\<IN\>\* <br> verb (+ preposition) <br> \<RB\>\<V\*\>\<IN\>\* <br> adverb + verb (+ prep.) |
VBZ (3rd person verb)<br>VBP (1st 2nd persons)<br>VB (base verb)<br> |Operational<br>e.g. “decide”, “decides”, “are”|\<V\*\>\<IN\>\*<br>verb (+ prep.)<br>
RB (Adverb)| If VBG follows -> **preambular**<br>e.g. *“also calling upon”*<br><br>If other V\* follows -> operational<br>*e.g. “also recognizes”*<br><br>If adjective follow -> preambular<br>*e.g. “deeply concerned”*|Searching for patterns:<br>\<RB\>\<V\*\>\<IN\>\*<br>adverb + verb (+ prep.)<br><br>\<RB\>\<JJ/VBN/VBD\>\<IN\>\*\*<br>adv. + adjective/p.p./p.t. (+prep.)|
JJ (Adjective)<br>VBD (past participle)<br>VBN (past tense)|Preambular<br><br>e.g. “aware further”|\< JJ/ VBN/VBD \>\<IN\>\*\*<br>adj/p.p./p.t. (+prep.)<br><br>\< JJ/ VBN/VBD \>\<RB\>\<IN\>\*\*<br>adj/p.p./p.t. + adv (+prep.)|
CC (Coniugation)|If VBG follows -\> preambular<br>e.g. “also calling upon”<br><br>If other V\* follows -\> operational<br>e.g. “also recognizes”<br><br>If adjective follow -\> preambular<br>e.g. “deeply concerned”|Searching for patterns:<br><br>\<CC\>\<V\*\>\<IN\>\*<br>Con. + verb(+ prep.)<br><br>\<CC\>\<JJ/ VBN/VBD\>\<IN\>\*\*<br>Con. + adj./p.p./p.t. (+prep.)|
IN<br>(Preposition)|Searching for the pattern<br>-COMA + VERB<br>-COMA + PRONOUN<br>e.g. *“As stated by the President, (we) reaffirm”*||
OTHER CASES|Searching for the pattern<br>-COMA + VERB<br>-COMA + PRONOUN<br> e.g. *“Given the result of the last facts, (we) reaffirm”*||

*only for getting phrasal verbs\**

*only for getting phrasal verbs\*\**

<a name="four-3-1"></a>
#### 4.3.1 A final pattern

After having detected the main term (e.g. “alsoRequest”), the qualifier also search for a final pattern “With”+JJ+NN (e.g. with grave concern), in order to detect expand the term into “alsoRequestWithGraveConcern”.

<a name="four-3-2"></a>
#### 4.3.2 Resolution of Mistakes

Interestingly, we also prepared the algorithm to prevent POS-tag mistakes.
Sometimes, for example, a paragraph starting with a verb is considered a noun. For example, if the first word is “requests”, the POS-tagger could interpret it as a NNS (plural noun). For this reason, we created a function that add a virtual subject before any NN and NNS found at the beginning of a paragraph. Because, they are very likely to be verbs. With this adjustment, the POS-tag correctly detect a verb instead of a noun.

<a name="four-3-3"></a>
#### 4.3.3 Dynamic Pattern Research

In some cases, a paragraph can start with informative components, that cannot describe the qualification of the paragraph.
For example, in the sentence “Given the recent facts, urges…” the part before the coma is automatically excluded by the qualifier. In this way, only “Urges” will be considered as an element for discriminating between the ‘preambolar’ or ‘operational’ categories, and for creating the right term “urge”.

**The ability to generalize and the need for some ad hoc pattern**
For some special case, some ad-hoc rules have been created. For example: “Bearing in mind”, “Keeping in mind”, “Pay tribute”. However, their occurrence is not frequent. Having chosen very general POS-tag patterns, the qualifier’s ability to generalize is relatively high.

**Exceptions**

We have detected two rare exceptions like the following:

1. Sentences starting with a 3rd person verb that begins with "re-", for example:

>	*Re-emphasizes the need to …*

![alt text](re-emphasizes.png)
Figure 6 -N0448880.doc

2. Inside of the **declarations**, that are **annex of resolutions** (e.g., A/RES/66/2) we find sentences starting with a subject pronoun (usually “we”) separated from its own predicate and having other verbal forms included in such separation:

>	*we, heads of state and government, assembled at the united nations on 27 september 2018 to undertake a comprehensive …*

![alt text](we-sentence.png)
Figure 7 - N1247866.pdf

<a name="four-3-1"></a>
#### 4.3.4	Conclusions and Results
After having created a dataset of 1000 sentences, manually annotated as "preambolar" or "operational", we have elaborated a preliminary testing/validation that is the following:

![alt text](confmatrix_qual.png)

- F1: **0.998**
- Precision: **0.999**
- Recall: **0.998**

The Marker-Converter convert this information in \<term\> with the related \<TLCTerm\> in the \<references\> block for permitting the connection with the ontological level.


~~~~
<container name="underlining" eId="container_preamble_pg13">
	<p><term refersTo="#underlining">Underlining</term> the fact that mutual understanding, dialogue, cooperation, transparency and confidence-building are important elements in all activities for the promotion and protection of human rights,</p>
</container>
~~~~
Figure 8- N1642803.doc, N1642803.xml


~~~~
<container name="reaffirmingAlso" eId="container_preamble_pg14">
    <p><term refersTo="#reaffirmingAlso">Reaffirming also</term> the hope that, in appointing highly qualified lecturers for the seminars to be held within the framework of the fellowship programmes in international law, account would be taken of the need to secure the representation of major legal systems and balance among various geographical regions,</p>
</container>
~~~~
Figure 9 - N1248556.doc, N1248556.xml


<a name="five"></a>
## 5.	Akoma Ntoso Marker-Converter

This module produce the Akoma Ntoso XML using Regular expressions and heuristics for detecting coverPage, preface, preamble, body, conclusions, annex, table.
This module reuse all the previous step knowledge for marking correctly the semantic part of the text.
**Even if the AKN4UN define the resolutions as \<documentCollection\> composed by different parts, we have preferred in this challenge to simplify the structure using \<statement\> only. It will not a big deal to wrap the result in a \<documentCollection\> later on.**

<a name="five-1"></a>
### 5.1	Process of Conversion

The first step of the conversion consists in loading the provided word document and converting it (or rather: its parts) into txt.
The second step is parsing the text top to bottom and using pattern matching to identify structural elements such as the document title, number, the paragraphs, sections, annexes and so on.

The pattern matching process uses replus, which provides a method to write modular, template-based extensible regular expression.

Depending on the result of the pattern matching on the text, the text itself is mapped as-is into Objects that work as a proxy for Akoma Ntoso xml generation.

Before the objects are appended, the text is qualified via a paragraph_qualifier which has the job to determine if the text represents a preambular or an operational element; then it is appended accordingly.

Once the objects are all appended, a downward recursive algorithm is used to ensure that all the elements are placed accordingly to their hierarchical value (eg. if a section and a paragraph happen to be siblings, the latter will be set as a child of the first).

The next step is to correctly generate the eIds of the objects (which may have prefixes depending on their parent(s)).

After that the structure is in place, it is possible to run pattern matching and machine learning algorithms to identify all inline elements, such as dates, references, roles, organizations and so on.

The inline pattern matching also uses replus; the match objects are passed through a series of resolvers which will extract the metadata, build the attributes and the corresponding Top-Level Concept to be added to the AKN references.

Other than regexes, inline elements are recognized using spaCy with some customized NER.

Once the structure and the inlines are done, another spaCy-powered custom algorithm identifies SDG with their targets and respective indicators. The results are mapped into AKN keywords, references and custom name-spaced (akn4un) elements that will link the results to their corresponding elements.

The last step simply consists in writing the AKN to an xml file and validate it.


**Cover**

![alt text](cover.png)

~~~~
</meta>
	<coverPage>
		<container name="publicationCoverPart">
			<p class="upper">
				<docNumber refersTo="#symbol">A/RES/71/4</docNumber>
			</p>
			<p>
				<span refersTo="#publisher" class="upper">
					<docAuthority refersTo="#un">United Nations</docAuthority>
				</span>
			</p>
			<p>
				<img src="logo-un.jpg"/>
				<docAuthority refersTo="#gaun" class="rightBig">General Assembly </docAuthority>
			</p>
			<p>
				<docStatus refersTo="#general" class="leftSmall">Distr.: General</docStatus>
			</p>
			<p>
				<date date="2016-10-17" class="leftSmall" refersTo="#publicationEvent">17 October 2016</date>
			</p>
		</container>
		<container name="contextCoverPart">
			<p>
				<session value="70" refersTo="#generalAssemblySession">Seventy-first session</session>
			</p>
			<p>
				<ref eId="ref_1" href="/akn/un/debateReport/agenda/ga/xxx/xxx/#item_113" refersTo="#origin">Agenda item 113</ref>
			</p>
			<p>
				<docTitle>
					<docType>Resolution</docType> adopted by the <docAuthority refersTo="#gaun">General Assembly</docAuthority> on <docDate date="2016-10-13" refersTo="#adoptionEvent">13 October 2016</docDate>
				</docTitle>
			[without reference to a <docProponent refersTo="#mc">Main Committee</docProponent> (<ref href="/akn/un/statement/draftResolution/ga/2016-10-17/A-RES-71-4/">A/71/L.4</ref>)]
		</p>
		</container>
	</coverPage>
~~~~


**Footer/Presentation**

![alt text](footer.png)

~~~~
</references>
<presentation source="#palmirani">
	<fao:firstPageHeading>
		<fao:left class="two-third">
			<docketNumber refersTo="#symbol">A/RES/71/4</docketNumber>
		</fao:left>
		<fao:right class="one-third bold little">
~~~~

Appointment of the Secretary-General of the United Nations
~~~~
</fao:right>
	</fao:firstPageHeading>
	<fao:firstPageFooter>
		<fao:left class="two-third">
			<docketNumber refersTo="#publicationJobNumber">16-17779 (E)</docketNumber>
		</fao:left>
		<img alt="bar code" class="barCode" src="/resolution_16-17779E_barCode.jpg"/>
		<fao:right class="one-third bold bigger">
			<img alt="Please recycle" class="image" src="./recylcleLogo.jpg"/>
			<img alt="embedded QR code" class="qrCode" src="./resolution_16-17779E_qcCode.jpg"/>
		</fao:right>
	</fao:firstPageFooter>
</presentation>
~~~~


**Heading/Presentation in case of multiple page we have this one:**

![alt text](heading.png)

~~~~
<fao:firstPageHeading>
	<fao:left class="two-third">
		<docketNumber refersTo="#symbol">A/RES/73/183<docketNumber>
	</fao:left>
	<fao:right class="one-third bold little">
	Enhancing the role of the Commission on Crime Prevention and Criminal Justice in contributing to the implementation of the 2030 Agenda for Sustainable Development
	</fao:right>
</fao:firstPageHeading>
~~~~


**Preface**

![alt text](preface.png)

~~~~
<preface>
	<p>
		<docNumber>71/4</docNumber>.<docTitle>
			<docPurpose refersTo="#appointment">Appointment</docPurpose> of the Secretary-General of the United Nations</docTitle>
	</p>
</preface>
~~~~

**Preamble**

![alt text](preamble.png)

~~~~
<preamble>
	<formula name="enactingFormula">
		<p>The General Assembly,</p>
	</formula>
	<block name="pramble" eId="block_1" refersTo="#preamble">
		<term eId="term_1" refersTo="#recalling">Recalling </term>
		<ref href="">XV of the Charter of the United Nations</ref>, and 	<term eId="term_2" refersTo="#reaffirming">reaffirming</term>
the role of the General Assembly under <ref href="">Article 97</ref>,
</block>
	<block name="pramble" eId="block_2">
		<term eId="term_3" refersTo="#welcoming">Welcoming </term>the process of selection and appointment of the Secretary -General and its timely conclusion, guided by the principles of transpa rency and inclusivity as set out in its <ref href="">resolutions 69/321 of 11 September 2015</ref> and <ref href="">70/305 of 13 September 2016</ref>, including the organization of informal dialogues with all candidates for the position of Secretary-General,</block>
	<block name="preamble" eId="block_3" refersTo="#preamble">
		<term eId="term_4" refersTo="#havingConsidered">Having considered </term> the recommendation contained in <ref href="xxxxxx">Security Council resolution 2311 (2016) of 6 October 2016</ref>,</block>
</preamble>
~~~~

**Body**

![alt text](body.png)

~~~~
	<mainBody>
			<block name="body" refersTo="#body">
				<term eId="term_5" refersTo="#appoints">Appoints</term>
				<person refersTo="#antonioGuterres" as="#sgun">Mr. António Guterres </person>
				<role eId="role_1" refersTo="#sgun">Secretary-General of the United Nations </role>for a
<concept refersTo="#termOfOffice">term of office</concept> beginning on <event refersTo="#intervalOfcommitment">
					<date date="2017-01-01" refersTo="#startDate">1 January 2017</date> and ending on <date date="2021-12-31" refersTo="#endDate">31 December 2021</date>
				</event>.
						</block>
		</mainBody>

**Conclusions**

![alt text](conclusions.png)

		<conclusions>
			<p>
				<event refersTo="#meeting">
					<location refersTo="#plenaryMeeting27Location">
						<quantity refersTo="#meetingNumber" eId="quantity_1">27th</quantity> plenary meeting</location>
				</event>
				<docDate date="2016-10-13" refersTo="#adoptionEvent">13 October 2016</docDate>
			</p>
		</conclusions>
~~~~


**Annex.**

![alt text](annex.png)

~~~~
<attachments>
	<attachment>
		<doc name="annex">
			<meta>
				<identification source="#cirsfid">
					<FRBRWork>
						<FRBRthis value="/akn/un/statement/resolution/ga/2011-12-06/66-106/!main/annex"/>
						<FRBRuri value="/akn/un/statement/resolution/ga/2011-12-06/66-106/"/>
						<FRBRdate date="2011-12-06" name="generation"/>
						<FRBRauthor href="#comp_1__ga" as="#comp_1__author"/>
						<FRBRcountry value="un"/>
						<FRBRsubtype value="resolution"/>
						<FRBRnumber value="68-247B"/>
						<FRBRprescriptive value="false"/>
						<FRBRauthoritative value="true"/>
					</FRBRWork>
					<FRBRExpression>
						<FRBRthis value="/akn/un/statement/resolution/ga/2011-12-06/66-106/eng@/!main/annex"/>
						<FRBRuri value="/akn/un/statement/resolution/ga/2011-12-06/66-106/eng@"/>
						<FRBRdate date="2011-12-06" name="markup"/>
						<FRBRauthor href="#comp_1__ga" as="#comp_1__editor"/>
						<FRBRlanguage language="eng"/>
					</FRBRExpression>
					<FRBRManifestation>
						<FRBRthis value="/akn/un/statement/resolution/ga/2011-12-06/666-106/eng@/!main/annex.xml"/>
						<FRBRuri value="/akn/un/statement/resolution/ga/2011-12-06/66-106/eng@.akn"/>
						<FRBRdate date="2019-04-18" name="publication"/>
						<FRBRauthor href="#comp_1__palmirani" as="#comp_1__editor"/>
					</FRBRManifestation>
				</identification>
			</meta>
			<preface>
				<p>
					<docType>Annex</docType>
					<docTitle>Code of conduct for the judges of the United Nations Dispute
Tribunal and the United Nations Appeals Tribunal</docTitle>
				</p>
			</preface>
			<preamble>
				<recitals>
					<heading>Preamble</heading>
					<recital>
						<block name="whereas">Whereas the Charter of the United Nations affirms, inter alia, the
determination of Member States to establish conditions under which justice can be
maintained to achieve international cooperation in promoting and encouraging
respect for human rights and fundamental freedoms without any discrimination,</block>
					</recital>
				</recitals>
			</preamble>
			<mainBody>
				<componentRef showAs="" src=""/>
			</mainBody>
		</doc>
	</attachment>
</attachments>
~~~~

**Exceptional Case**

![alt text](exceptional.png)

~~~~
<section eId="sec_VII">
	<num>VII</num>
	<heading>Capital master plan</heading>
	<crossHeading eId="sec_VII__crossHeading_1" refersTo="#preamble">Recalling its resolutions 54/249 of 23 December 1999, 55/238 of 23 December 2000,
56/234 and 56/236 of 24 December 2001, 56/286 of 27 June 2002, section II of its
resolution 57/292 of 20 December 2002, its resolution 59/295 of 22 June 2005, section II
of its resolution 60/248 of 23 December 2005, its resolutions 60/256 of 8 May 2006,
60/282 of 30 June 2006, 61/251 of 22 December 2006, 62/87 of 10 December 2007,
63/270 of 7 April 2009, 64/228 of 22 December 2009, 65/269 of 4 April 2011, section III
of its resolution 66/258 of 9 April 2012, section V of its resolution 67/246 and section IV
of its resolution 68/247 A and its decisions 58/566 of 8 April 2004, 65/543 of 24 December
2010 and 66/555 of 24 December 2011,</crossHeading>
	<crossHeading eId="sec_VII__crossHeading_2" refersTo="#preamble">Having considered the reports of the Secretary-General on the update of the eleventh
annual progress report on the implementation of the capital master plan18 and on updated
information on final expenditure for associated costs for the period from 2008 to 201319
and the related report of the Advisory Committee,20</crossHeading>
	<paragraph eId="sec_VII__para_1">
		<num>1.</num>
		<content>
			<p>Takes note of the reports of the Secretary-General;18,19</p>
		</content>
	</paragraph>
	<paragraph eId="sec_VII__para_2">
		<num>2.</num>
		<content>
			<p> Endorses the conclusions and recommendations contained in the report of the
Advisory Committee,20 subject to the provisions of the present resolution;</p>
		</content>
	</paragraph>
	<chapter eId="sec_VII__chp_A">
		<num>A.</num>
		<heading> Update of the eleventh annual progress report</heading>
		<paragraph eId="sec_VII__chp_A__para_3">
			<num>3.</num>
			<content>
				<p>Welcomes with appreciation the donations from Member States that are
contributing to the capital master plan project;</p>
			</content>
		</paragraph>
		<paragraph eId="sec_VII__chp_A__para_4">
			<num>4.</num>
			<content>
				<p>Notes that as at 28 March 2014 an amount of 678,214 dollars in assessments
remained unpaid to the capital master plan for 2013 and prior periods, and urges the
Member States concerned to arrange for the disbursement of those contributions
expeditiously;</p>
			</content>
		</paragraph>
	</chapter>
</section>
~~~~

<a name="five-2"></a>
### 5.2	Conclusion and Results

We have processed the UN documents with the following results:

-	Totali processati: 591
-	Validi: 556
-	Non validi: 35
-	Percentuale di validità: 94.1 %
-	Tempo impiegato: 3h 51m (Mbp 2015, i5 16gb RAM).

The invalidity is mostly due to:

1.	the annexes and to a trivial problem in the eId generation. We can fix this issue easily in one week of work;
2.	The presence of components, embedded documents (e.g., charter), tables. We can fix this issue in two weeks of work;
We can improve the marker-converter with a better precision of the references recognition, of the semantic annotation, the presentation markup part. We need two weeks for this task.

<a name="six"></a>
## 6.	RDF Generation

The information used inside of the Akoma Ntoso could be serialized in RDF using UNDO ontology. The idea is to connect them in a RDF assertion connecting the agent with the role, the action and the duration of the event. An example is showed below:

~~~~
<paragraph>
	<content>
		<p>
			<term eId="term_5" refersTo="#appoints">Appoints</term>
			<person refersTo="#antonioGuterres" as="#sgun">Mr. António Guterres </person>
			<role eId="role_1" refersTo="#sgun">Secretary-General of the United Nations </role>for a
<concept refersTo="#termOfOffice">term of office</concept> beginning on <event refersTo="#intervalOfcommitment">
				<date date="2017-01-01" refersTo="#startDate">1 January 2017</date> and ending on <date date="2021-12-31" refersTo="#endDate">31 December 2021</date>
			</event>.</p>
	</content>
</paragraph>
~~~~

~~~~
:a-res-71-4-2016-10-13 a undo:Resolution .
:generalAssembly a allot:Organization .
:sgun a allot:Role .
:un a lkif:Jurisdiction .

:antonioGuterres a allot:Person ;
tvc:hasValue
: antonioGuterres-sgun-2017-2021 .
:antonioGuterres-sgun-2017-2021 a undo:ValueInTimeAndContext ;
tvc:withValue :sgun ;
tvc:atTime : intervalOfCommitment;
tvc:withinContext :un .

:intervalOfCommitment a undo:Interval ;
ti:hasIntervalStartDate "2017-01-01T00:00:00"^^xsd:dateTime ;
ti:hasIntervalEndDate "2021-12-31T00:00:00"^^xsd:dateTime .

:appoints a allot:Term ;
undo:hasRelatedConcept :termOfOffice .

:termOfOffice a allot:Concept .

:annotation-a-res-71-4-2016-10-13 a undo:Annotation ;
oa:hasBody :appoints ;
oa:hasTarget :a-res-50-100-1996-02-02-para-1 .

:relation-1 a undo:Relation ;
undo:hasProponent :general-assembly ;
undo:hasSemantics :appoints ;
undo:hasReceiver : antonioGuterres .
~~~~
N1632409.docx

We will do this task in two months.


<a name="seven"></a>
## 7.	Milestones

We have completed the 60% of the work respect the tasks expected. We need more time for implementing the following tasks (6 months in term of schedule, 12 man/months in term of effort):

1.	NER: refine the reference, location – duration: two weeks; effort: two weeks;
2.	CLASSIFIER: better recognition of SDG – duration: one month; effort: one month;
3.	QUALIFIER: adjectives, etc. in the preambular sentences – duration: one month; effort: one month;
4.	MARKER-CONVERTER: 1) eId of Annex – one week; 2) table, component – two weeks; 3) semantic parts and references – two weeks. Duration: one month; effort: one month;
5.	ONTOLOGY/RDF: Duration: two month; effort: two month;
6.	All the tasks need a scientific evaluation by third parties. We would like to ask to external people (e.g., student of Summer School LEX or expert of UN) – Duration: three month; effort: three month;
7.	Refinement of the software – three months.


![alt text](tabellona.png)


<a name="eight"></a>
## 8.	Installation

- requires python3.7+
- clone this repo
- cd to the repo/development
- create a virtual environment: _python3 -m venv venv_
- load the virtual environment: _source venv/bin/activate_
- install the dependencies: _pip install -r requirements.txt_
- install the spaCy model: _python -m spacy download en_core_web_md_


<a name="eight-1"></a>
### 8.1 Usage

- download all the documents: python run.py --download
- to parse one document: python run.py --parse <filepath>
- to parse all the documents: python run.py --parseall
- to use with a GUI: python run.py --gui \[--port: port_no\] (it will return \*.akn zip archives, also saved locally in keld/server/converted/)


**All the converted files will be written in the directory _data/output_**

<a name="eight-2"></a>
### 8.2 Troubleshooting

If you are experiencing problems with import errors, export the PYTHONPATH as follows:

~~~~
export PYTHONPATH:${PYTHONPATH}:<full/path/to/the/repo/>
~~~~

<a name="nine"></a>
## 9. References

1.	[Arthmetic properties of word embeddings](https://medium.com/data-from-the-trenches/arithmetic-properties-of-word-embeddings-e918e3fda2ac)
2.	[Bag-of-Words TFIDF explained](http://datameetsmedia.com/bag-of-words-tf-idf-explained/)
3.	[Sustainable Development Goals](https://www.un.org/sustainabledevelopment/sustainable-development-goals/)
4.	[The distributional Hypotesis](https://hyunyoung2.github.io/2018/03/08/The_Distribuational_Hypothesis/)
5.	Pennington, Jeffrey, Richard Socher, and Christopher Manning. "Glove: Global vectors for word representation." Proceedings of the 2014 conference on empirical methods in natural language processing (EMNLP). 2014.
6.	Sahlgren, Magnus. "The distributional hypothesis." Italian Journal of Disability Studies 20 (2008): 33-53.
7.	Zhu, Wei, et al. "A study of damp-heat syndrome classification using Word2vec and TF-IDF." 2016 IEEE International Conference on Bioinformatics and Biomedicine (BIBM). IEEE, 2016.
8.	Le, Quoc, and Tomas Mikolov. "Distributed representations of sentences and documents." International conference on machine learning. 2014.
9.	Mikolov, Tomas, et al. "Efficient estimation of word representations in vector space." arXiv preprint arXiv:1301.3781 (2013).
10.	Bojanowski, Piotr, et al. "Enriching word vectors with subword information." Transactions of the Association for Computational Linguistics 5 (2017): 135-146.
11.	[SpaCy](https://spacy.io)
12.	https://blog.ouseful.info/2017/09/page/3/
13.	http://wit.istc.cnr.it/stlab-tools/fred/
14.	http://docs.oasis-open.org/legaldocml/akn-core/v1.0/os/part1-vocabulary/akn-core-v1.0-os-part1-vocabulary.html#_Toc523925096
15.	http://docs.oasis-open.org/legaldocml/akn-nc/v1.0/cs01/akn-nc-v1.0-cs01.html
16.	http://ontologydesignpatterns.org/wiki/Main_Page
17.	https://iswc2017.semanticweb.org/wp-content/uploads/papers/MainProceedings/179.pdf

<a name="ten"></a>
## 10. Resources

- Documents: http://undocs.org/; https://digitallibrary.un.org/
- UNDO ontology:https://unsceb-hlcm.github.io/onto-undo/index.html
- https://github.com/unsceb-hlcm/undo
- SDGIO:http://purl.unep.org/sdg/sdgio.owl
- The PURLs for individual SDGIO terms should be resolved via environment live. For example:
- http://purl.unep.org/sdg/SDGIO_00020001
- http://uneplive.unep.org/ontology/index/SDGIO_00020001
- If you need a more descriptive resolver, OntoBee can help, e.g.:
- http://www.ontobee.org/ontology/SDGIO?iri=http://purl.unep.org/sdg/SDGIO_00000000
- UNBIS:http://metadata.un.org/?lang=en
- https://s3.amazonaws.com/unbis-thesaurus/unbist-20181105.ttl
